import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';
import { App } from '/imports/ui/App';
// import 'bootstrap/dist/css/bootstrap.min.css';
import '@fortawesome/fontawesome-free'
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'

import 'react-checkbox-tree/lib/react-checkbox-tree.css';

Meteor.startup(() => {
  render(<App/>, document.getElementById('react-target'));
});

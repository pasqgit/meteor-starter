import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { LlistesCollection } from '/imports/api/llistes';
import { DatesCollection } from '/imports/api/dates';
import { EntradesTextCollection } from '/imports/api/entradesText';
// import { HabitsCollection } from '/imports/api/habits';
import { TimeSessionsCollection } from '/imports/api/timeSessions';
import { LaGerraCollection } from '/imports/api/laGerra';

import { FilesCol } from '/imports/api/files.js';

import fs from 'fs';
// function insertLink({ title, url }) {
//   LlistesCollection.insert({title, url, createdAt: new Date()});
// }

import { EntitatsCollection } from '/imports/api/entitats';
import { EventsCollection } from '/imports/api/events';
import { AreesCollection } from '/imports/api/arees';
import { LogresCollection } from '/imports/api/logres';
import { CampsDeContribucioCollection } from '/imports/api/campsDeContribucio';
import { TasquetesCollection } from '/imports/api/tasquetes';
import { AreesPersonalsCollection } from '/imports/api/areesPersonals';
//import { Dates } from '/imports/api/dates.js';
import { Roles } from 'meteor/alanning:roles';


const SEED_USERNAME = 'meteorite';
const SEED_PASSWORD = 'password';

Meteor.startup(() => {
  // If the Links collection is empty, add some data.
  // if (LlistesCollection.find().count() === 0) {
  //   insertLink({
  //     title: 'Do the Tutorial',
  //     url: 'https://www.meteor.com/tutorials/react/creating-an-app'
  //   });

  //   insertLink({
  //     title: 'Follow the Guide',
  //     url: 'http://guide.meteor.com'
  //   });

  //   insertLink({
  //     title: 'Read the Docs',
  //     url: 'https://docs.meteor.com'
  //   });

  //   insertLink({
  //     title: 'Discussions',
  //     url: 'https://forums.meteor.com'
  //   });
  // }

  
  ///ROLS////////////////////////////////////////////////////////////////////////////////////////////////
  Roles.createRole('user', {unlessExists: true});
  Roles.createRole('admin', {unlessExists: true});

  Meteor.publish(null, function () {
    //console.log("id: ", this.userId);
    if (this.userId) {
      return Meteor.roleAssignment.find({ 'user._id': this.userId });
    } else {
      this.ready();
    }
  });
  /////////////////////////////////////////////////////////////////////////////////////////////////////

  
  if (!Accounts.findUserByUsername(SEED_USERNAME)) {
    Accounts.createUser({
      username: SEED_USERNAME,
      password: SEED_PASSWORD,
    });
  }

});

Meteor.publish('llistesUsuari', function (uid) {
  return LlistesCollection.find({user: uid}, 
    {
      sort: {fecha: -1},
      // skip: npp * (np - 1),
      // limit: 9 //Math.min(limit + (np * npp), 9)
    }
  );
});

Meteor.publish('entradesTextUsuari', function (uid) {
  return EntradesTextCollection.find({user: uid}, 
    {
      sort: {fecha: -1},
      // skip: npp * (np - 1),
      // limit: 9 //Math.min(limit + (np * npp), 9)
    }
  );
});

////////////
//--HÀBITS--

Meteor.publish('habitsUsuari', function (uid) {
  return HabitsCollection.find({user: uid}, 
    {
      sort: {fecha: -1},
      // skip: npp * (np - 1),
      // limit: 9 //Math.min(limit + (np * npp), 9)
    }
  );
});


Meteor.publish('datesUsuari', function (uid) {
  return DatesCollection.find({user: uid}, 
    {
      sort: {date: -1},
      // skip: npp * (np - 1),
      // limit: 9 //Math.min(limit + (np * npp), 9)
    }
  );
});

////////////////////////////////////////////////////////////////

Meteor.publish('entitatsUsuari', function (uid) {
  return EntitatsCollection.find({user: uid}, 
    {
      sort: {date: -1},
      // skip: npp * (np - 1),
      // limit: 9 //Math.min(limit + (np * npp), 9)
    }
  );
});

Meteor.publish('events', function (uid) {
  return EventsCollection.find({user: uid}, 
    {
      sort: {date: -1},
      // skip: npp * (np - 1),
      // limit: 9 //Math.min(limit + (np * npp), 9)
    }
  );
});
Meteor.publish('arees', function (uid) {
  return AreesCollection.find({user: uid}, 
    {
      sort: {date: -1},
      // skip: npp * (np - 1),
      // limit: 9 //Math.min(limit + (np * npp), 9)
    }
  );
});
Meteor.publish('logres', function (uid) {
  return LogresCollection.find({user: uid}, 
    {
      sort: {date: -1},
      // skip: npp * (np - 1),  
      // limit: 9 //Math.min(limit + (np * npp), 9)
    }
  );
});
Meteor.publish('campsDeContribucio', function (uid) {
  return CampsDeContribucioCollection.find({user: uid}, 
    {
      sort: {date: -1},
      // skip: npp * (np - 1),
      // limit: 9 //Math.min(limit + (np * npp), 9)
    }
  );
});

// Meteor.publish('habitsUsuariDia', function (uid, dia) {
//   return HabitsCollection.find({user: uid}, 
//     {
//       sort: {fecha: -1},
//       // skip: npp * (np - 1),
//       // limit: 9 //Math.min(limit + (np * npp), 9)
//     }
//   );
// });


Meteor.publish('timeSessions', function (uid) {
  return TimeSessionsCollection.find({user: uid});
});

Meteor.publish('laGerra', function (uid) {
  return LaGerraCollection.find({user: uid});
});


Meteor.publish('usuaris', function (uid) {
  if (uid) {
    return Meteor.users.find({_id: uid},{fields: {username: 1, avatarId: 1, avatarLink: 1}});
  }
  return Meteor.users.find({},{fields: {username: 1, avatarId: 1, avatarLink: 1}});
});

Meteor.publish('tasquetesUsuari', function (uid) {
  return TasquetesCollection.find({uid: uid});
});

Meteor.publish('areesPersonalsUsuari', function (uid) {
  return AreesPersonalsCollection.find({uid: uid});
});


Accounts.onCreateUser((options, user) => {

  // const customizedUser = Object.assign({
  //   dexterity: _.random(1, 6) + _.random(1, 6) + _.random(1, 6),
  // }, user);

  const customizedUser = {...user, ...options.opts};

  // We still want the default hook's 'profile' behavior.
  if (options.profile) {
    customizedUser.profile = options.profile;
  }

  return customizedUser;
});


Meteor.methods({

  'creaUsuariAmbRols'(usr, roleScope) {
    let id;

    id = Accounts.createUser(usr);

    Roles.addUsersToRoles(id, usr?.opts?.rols, roleScope);

    return id;
  },


  'users.update'() {

    Meteor.users.update(userId, {
      $set: {
        mailingAddress: newMailingAddress
      }
    });

  },

  'fs.scan.dir'(path) {
    let __dirname; 
    let arrDirs = [];

    __dirname = path;

    if(Meteor.isServer) {
      //fs = Meteor.require('fs');

      fs.readdir(__dirname, 
        { withFileTypes: true },
        (err, files) => {
          console.log("\nCurrent directory files:");
          if (err)
            console.log(err);
          else {
            files.forEach(file => {
              if (fs.existsSync(__dirname+"/"+file.name) && fs.lstatSync(__dirname+"/"+file.name).isDirectory()){
                arrDirs = [...arrDirs, file];
                console.log(file.name); // JSON.stringify()
                // fs.lstat(__dirname+"/"+file.name, (err, stats) => {

                //      if(err)
                //          return console.log(err); //Handle error
                  
                //      console.log(`Is file: ${stats.isFile()}`);
                //      console.log(`Is directory: ${stats.isDirectory()}`);
                //      console.log(`Is symbolic link: ${stats.isSymbolicLink()}`);
                //      console.log(`Is FIFO: ${stats.isFIFO()}`);
                //      console.log(`Is socket: ${stats.isSocket()}`);
                //      console.log(`Is character device: ${stats.isCharacterDevice()}`);
                //      console.log(`Is block device: ${stats.isBlockDevice()}\n`);
                //  });
              }
            // console.log(`Ext: ${path.extname(file.name)}`);
            // console.log(`EsDir: ${fs.existsSync(__dirname+"/"+file.name) && fs.lstatSync(__dirname+"/"+file.name).isDirectory()}\n`);
          })
        }
      })
    }

    return arrDirs;
  },

  'avatarUserSet'(avatarId) {
    FilesCol.update(avatarId, {
      $set: {
        "meta.userId": Meteor.userId()
      }
    })
  },

  'userAvatarUpdate'(avatarId, avatarLink) {
    Meteor.users.update(Meteor.userId(), {
      $set: {
        "profile.avatarId": avatarId,
        "profile.avatarLink": avatarLink
      }
    })
  },

  'ent.fesPublica'(entId) {
    const ent = EntitatsCollection.findOne(entId);

    if (ent.user === Meteor.userId()) {
      EntitatsCollection.update(entId, {
        $set: {...ent, privacy: []}
      });
    } 
  },

  'ent.fesPrivada'(entId) {
    const ent = EntitatsCollection.findOne(entId);
   // console.log(ent);

    if (ent.user === Meteor.userId()) {
      EntitatsCollection.update(entId, {
        $set: {...ent, privacy: [Meteor.userId()]}
      });
    } 
  },
});


//Meteor.publish('files.images.all', () => Images.collection.find({}));

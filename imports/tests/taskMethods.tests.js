import { Meteor } from 'meteor/meteor';
import { Random } from 'meteor/random';
import { mockMethodCall } from 'meteor/quave:testing';
//import assert from "assert";
import { assert } from 'chai';
import { DatesCollection } from '/imports/api/dates.js';

if (Meteor.isServer) {
  describe('Tasks', () => {
    describe('methods', () => {
      it('can delete owned task', () => {});
    });
  });
}

if (Meteor.isClient) {
  describe('Masks', () => {
    describe('methods', () => {
      it('can delete owned mask', () => {});
    });
  });
}


// if (Meteor.isServer) {
//   describe('Tasks', () => {
//     describe('methods', () => {
//       const userId = Random.id();
//       let taskId;

//       beforeEach(() => {
//         DatesCollection.remove({});
//         taskId = DatesCollection.insert({
//           text: 'Test Task',
//           createdAt: new Date(),
//           userId,
//         });
//       });
//     });
//   });
// }

if (Meteor.isServer) {
  describe('Dates', () => {
    describe('methods', () => {
      const userId = Random.id();
      let dataId;
      
      beforeEach(() => {
        DatesCollection.remove({});
        dataId = DatesCollection.insert({
          text: 'Test Task',
          createdAt: new Date(),
          userId,
        });
      });

      it('can delete owned date', () => {
        mockMethodCall('dates.remove', dataId, { context: { userId } });

        assert.equal(DatesCollection.find().count(), 0);
      });
    });
  });
}
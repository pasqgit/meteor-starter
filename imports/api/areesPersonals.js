import { Mongo } from 'meteor/mongo';

export const AreesPersonalsCollection = new Mongo.Collection('areesPersonals');

Meteor.methods({
    'areesPersonals.insert'(areaPersonal){
        // if (!Meteor.userId()){
        // throw new Meteor.Error('not-authorized');
        // }
        AreesPersonalsCollection.insert({
            ...areaPersonal,
            createdAt: new Date(),
            user: Meteor.userId()
        });
    },

    'areesPersonals.update'(areaPersonal){
        // if (Meteor.userId() !== allcod.user){
        // throw new Meteor.Error('not-authorized');
        // }
        AreesPersonalsCollection.update(areaPersonal._id, {
            $set: {
                ...areaPersonal
            }
        });
    },

    'areesPersonals.delete'(areaPersonal){
        // if (Meteor.userId() !== allcod.user){
        // throw new Meteor.Error('not-authorized');
        // }
        AreesPersonalsCollection.remove(areaPersonal._id);
    },

    'areesPersonals.remove'(id, context) {
        AreesPersonalsCollection.remove(id);
    }

});
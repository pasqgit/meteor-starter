import { Mongo } from 'meteor/mongo';

export const LlistesCollection = new Mongo.Collection('llistes');

Meteor.methods({
    'llistes.insert'(llista){
        // if (!Meteor.userId()){
        // throw new Meteor.Error('not-authorized');
        // }
        LlistesCollection.insert({
            ...llista,
            createdAt: new Date(),
            user: Meteor.userId()
        });
    },

    'llistes.update'(llista){
        // if (Meteor.userId() !== allcod.user){
        // throw new Meteor.Error('not-authorized');
        // }
        LlistesCollection.update(llista._id, {
            $set: {
                ...llista
            }
        });
    },

    'llistes.delete'(llista){
        // if (Meteor.userId() !== allcod.user){
        // throw new Meteor.Error('not-authorized');
        // }
        LlistesCollection.remove(llista._id);
    }
});
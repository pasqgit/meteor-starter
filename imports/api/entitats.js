import { Mongo } from 'meteor/mongo';

export const EntitatsCollection = new Mongo.Collection('entitats');

Meteor.methods({
    'entitats.insert'(entitat){
        // if (!Meteor.userId()){
        // throw new Meteor.Error('not-authorized');
        // }
        return EntitatsCollection.insert({
            ...entitat,
            createdAt: new Date(),
            user: Meteor.userId(),
            privacy: [Meteor.userId()]
        });
    },

    'entitats.update'(entitat){
        // if (Meteor.userId() !== allcod.user){
        // throw new Meteor.Error('not-authorized');
        // }
        EntitatsCollection.update(entitat._id, {
            $set: {
                ...entitat
            }
        });
    },

    'entitats.delete'(entitat){
        // if (Meteor.userId() !== allcod.user){
        // throw new Meteor.Error('not-authorized');
        // }
        EntitatsCollection.remove(entitat._id);
    },

    'entitats.remove'(id, context) {
        EntitatsCollection.remove(id);
    },

    'entitats.afegirText'(id, text) {
        const entTexts = EntitatsCollection.findOne(id).texts || [];

        EntitatsCollection.update(id, {
            $set: {
                texts: [
                    ...entTexts,
                    {
                        text, 
                        createdAt: new Date(), 
                        createdBy: Meteor.userId()
                    }
                ]
            }
        });
    },

    'entitat.novaSub'(ent, subId){
        EntitatsCollection.update(ent._id, {
            $set: {
                ...ent,
                descendents: ent.descendents?.length > 0 ? Array.from(new Set([...ent.descendents, subId])) : [subId]
            }
        });
    },

    'entitat.novaSuperior'(ent, superiorId){
        EntitatsCollection.update(ent._id, {
            $set: {
                ...ent,
                superiors: ent.superiors?.length > 0 ? Array.from(new Set([...ent.superiors, superiorId])) : [superiorId]
            }
        });
    },

    'entitat.matxacaDescendents'(entId, descendents){
        let ent = EntitatsCollection.findOne(entId);

        EntitatsCollection.update(entId, {
            $set: {
                ...ent,
                descendents
            }
        });
    },

    'entitat.matxacaSuperiors'(entId, superiors){
        let ent = EntitatsCollection.findOne(entId);

        EntitatsCollection.update(entId, {
            $set: {
                ...ent,
                superiors
            }
        });
    }

});
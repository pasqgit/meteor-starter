import { Mongo } from 'meteor/mongo';

export const LaGerraCollection = new Mongo.Collection('laGerra');

Meteor.methods({
    'laGerra.insert'(lg){
        // if (!Meteor.userId()){
        // throw new Meteor.Error('not-authorized');
        // }
        LaGerraCollection.insert({
            ...lg,
            createdAt: new Date(),
            user: Meteor.userId()
        });
    },

    'laGerra.upsert'(lg){
        // if (Meteor.userId() !== allcod.user){
        // throw new Meteor.Error('not-authorized');
        // }
        LaGerraCollection.upsert(lg._id, {
            $set: {
                ...lg,
               upsertedAt: new Date(),
               user: Meteor.userId()
            }
        });
    },

    'laGerra.delete'(lg){
        // if (Meteor.userId() !== allcod.user){
        // throw new Meteor.Error('not-authorized');
        // }
        LaGerraCollection.remove(lg._id);
    }
});
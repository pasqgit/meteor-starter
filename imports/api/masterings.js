import { Mongo } from 'meteor/mongo';

export const MasteringsCollection = new Mongo.Collection('masterings');

Meteor.methods({
    'masterings.insert'(mastering){
        // if (!Meteor.userId()){
        // throw new Meteor.Error('not-authorized');
        // }
        MasteringsCollection.insert({
            ...mastering,
            createdAt: new Date(),
            user: Meteor.userId()
        });
    },

    'masterings.update'(mastering){
        // if (Meteor.userId() !== allcod.user){
        // throw new Meteor.Error('not-authorized');
        // }
        MasteringsCollection.update(mastering._id, {
            $set: {
                ...mastering
            }
        });
    },

    'masterings.delete'(mastering){
        // if (Meteor.userId() !== allcod.user){
        // throw new Meteor.Error('not-authorized');
        // }
        MasteringsCollection.remove(mastering._id);
    },

    'masterings.remove'(id, context) {
        MasteringsCollection.remove(id);
    }

});
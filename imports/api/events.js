import { Mongo } from 'meteor/mongo';

export const EventsCollection = new Mongo.Collection('events');

Meteor.methods({
    'events.insert'(event){
        // if (!Meteor.userId()){
        // throw new Meteor.Error('not-authorized');
        // }
        EventsCollection.insert({
            ...event,
            createdAt: new Date(),
            user: Meteor.userId()
        });
    },

    'events.update'(event){
        // if (Meteor.userId() !== allcod.user){
        // throw new Meteor.Error('not-authorized');
        // }
        EventsCollection.update(event._id, {
            $set: {
                ...event
            }
        });
    },

    'events.delete'(event){
        // if (Meteor.userId() !== allcod.user){
        // throw new Meteor.Error('not-authorized');
        // }
        EventsCollection.remove(event._id);
    },

    'events.remove'(id, context) {
        EventsCollection.remove(id);
    }

});
import { Mongo } from 'meteor/mongo';

export const TimeSessionsCollection = new Mongo.Collection('timeSessions');

Meteor.methods({
    'timeSessions.insert'(ts){
        // if (!Meteor.userId()){
        // throw new Meteor.Error('not-authorized');
        // }
        TimeSessionsCollection.insert({
            ...ts,
            createdAt: new Date(),
            user: Meteor.userId()
        });
    },

    'timeSessions.upsert'(ts){
        // if (Meteor.userId() !== allcod.user){
        // throw new Meteor.Error('not-authorized');
        // }
        TimeSessionsCollection.upsert(ts._id, {
            $set: {
                ...ts,
               upsertedAt: new Date(),
               user: Meteor.userId()
            }
        });
    },

    'timeSessions.delete'(ts){
        // if (Meteor.userId() !== allcod.user){
        // throw new Meteor.Error('not-authorized');
        // }
        TimeSessionsCollection.remove(ts._id);
    }
});
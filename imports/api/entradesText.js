import { Mongo } from 'meteor/mongo';

export const EntradesTextCollection = new Mongo.Collection('entradesText');

Meteor.methods({
    'entradesText.insert'(entradaText){
        // if (!Meteor.userId()){
        // throw new Meteor.Error('not-authorized');
        // }
        EntradesTextCollection.insert({
            ...entradaText,
            createdAt: new Date(),
            user: Meteor.userId()
        });
    },

    'entradesText.update'(entradaText){
        // if (Meteor.userId() !== allcod.user){
        // throw new Meteor.Error('not-authorized');
        // }
        EntradesTextCollection.update(entradaText._id, {
            $set: {
                ...entradaText
            }
        });
    },

    'entradesText.delete'(entradaText){
        // if (Meteor.userId() !== allcod.user){
        // throw new Meteor.Error('not-authorized');
        // }
        EntradesTextCollection.remove(entradaText._id);
    }
});
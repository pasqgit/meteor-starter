import { Mongo } from 'meteor/mongo';

export const DatesCollection = new Mongo.Collection('dates');

Meteor.methods({
    'dates.insert'(data){
        // if (!Meteor.userId()){
        // throw new Meteor.Error('not-authorized');
        // }
        DatesCollection.insert({
            ...data,
            createdAt: new Date(),
            user: Meteor.userId()
        });
    },

    'dates.update'(data){
        // if (Meteor.userId() !== allcod.user){
        // throw new Meteor.Error('not-authorized');
        // }
        DatesCollection.update(data._id, {
            $set: {
                ...data
            }
        });
    },

    'dates.delete'(data){
        // if (Meteor.userId() !== allcod.user){
        // throw new Meteor.Error('not-authorized');
        // }
        DatesCollection.remove(data._id);
    },

    'dates.remove'(id, context) {
        DatesCollection.remove(id);
    }

});
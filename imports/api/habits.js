import { Mongo } from 'meteor/mongo';

export const HabitsCollection = new Mongo.Collection('habits');

Meteor.methods({
    'habits.insert'(habit){
        // if (!Meteor.userId()){
        // throw new Meteor.Error('not-authorized');
        // }
        HabitsCollection.insert({
            ...habit,
            createdAt: new Date(),
            user: Meteor.userId()
        });
    },

    'habits.update'(habit){
        // if (Meteor.userId() !== allcod.user){
        // throw new Meteor.Error('not-authorized');
        // }
        HabitsCollection.update(habit._id, {
            $set: {
                ...habit
            }
        });
    },

    'habits.delete'(habit){
        // if (Meteor.userId() !== allcod.user){
        // throw new Meteor.Error('not-authorized');
        // }
        HabitsCollection.remove(habit._id);
    }
});
import { Mongo } from 'meteor/mongo';

export const CampsDeContribucioCollection = new Mongo.Collection('campsdecontribucio');

Meteor.methods({
    'campsDeContribucio.insert'(campDeContribucio){
        // if (!Meteor.userId()){
        // throw new Meteor.Error('not-authorized');
        // }
        CampsDeContribucioCollection.insert({
            ...campDeContribucio,
            createdAt: new Date(),
            user: Meteor.userId()
        });
    },

    'campsDeContribucio.update'(campDeContribucio){
        // if (Meteor.userId() !== allcod.user){
        // throw new Meteor.Error('not-authorized');
        // }
        CampsDeContribucioCollection.update(campDeContribucio._id, {
            $set: {
                ...campDeContribucio
            }
        });
    },

    'campsDeContribucio.delete'(campDeContribucio){
        // if (Meteor.userId() !== allcod.user){
        // throw new Meteor.Error('not-authorized');
        // }
        CampsDeContribucioCollection.remove(campDeContribucio._id);
    },

    'campsDeContribucio.remove'(id, context) {
        CampsDeContribucioCollection.remove(id);
    }

});
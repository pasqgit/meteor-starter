import { Mongo } from 'meteor/mongo';

export const TasquetesCollection = new Mongo.Collection('tasquetes');

Meteor.methods({
    'tasquetes.insert'(tasqueta){
        // if (!Meteor.userId()){
        // throw new Meteor.Error('not-authorized');
        // }
        TasquetesCollection.insert({
            ...tasqueta,
            createdAt: new Date(),
            user: Meteor.userId()
        });
    },

    'tasquetes.update'(tasqueta){
        // if (Meteor.userId() !== allcod.user){
        // throw new Meteor.Error('not-authorized');
        // }
        TasquetesCollection.update(tasqueta._id, {
            $set: {
                ...tasqueta
            }
        });
    },

    'tasquetes.delete'(tasqueta){
        // if (Meteor.userId() !== allcod.user){
        // throw new Meteor.Error('not-authorized');
        // }
        TasquetesCollection.remove(tasqueta._id);
    },

    'tasquetes.remove'(id, context) {
        TasquetesCollection.remove(id);
    }

});
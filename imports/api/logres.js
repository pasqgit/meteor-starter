import { Mongo } from 'meteor/mongo';

export const LogresCollection = new Mongo.Collection('logres');

Meteor.methods({
    'logres.insert'(logre){
        // if (!Meteor.userId()){
        // throw new Meteor.Error('not-authorized');
        // }
        LogresCollection.insert({
            ...logre,
            createdAt: new Date(),
            user: Meteor.userId()
        });
    },

    'logres.update'(logre){
        // if (Meteor.userId() !== allcod.user){
        // throw new Meteor.Error('not-authorized');
        // }
        LogresCollection.update(logre._id, {
            $set: {
                ...logre
            }
        });
    },

    'logres.delete'(logre){
        // if (Meteor.userId() !== allcod.user){
        // throw new Meteor.Error('not-authorized');
        // }
        LogresCollection.remove(logre._id);
    },

    'logres.remove'(id, context) {
        LogresCollection.remove(id);
    }

});
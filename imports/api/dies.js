import { Mongo } from 'meteor/mongo';

export const DiesCollection = new Mongo.Collection('dies');

Meteor.methods({
    'dies.insert'(dia){
        // if (!Meteor.userId()){
        // throw new Meteor.Error('not-authorized');
        // }
        DiesCollection.insert({
            ...dia,
            createdAt: new Date(),
            user: Meteor.userId()
        });
    },

    'dies.update'(dia){
        // if (Meteor.userId() !== allcod.user){
        // throw new Meteor.Error('not-authorized');
        // }
        DiesCollection.update(dia._id, {
            $set: {
                ...dia
            }
        });
    },

    'dies.delete'(dia){
        // if (Meteor.userId() !== allcod.user){
        // throw new Meteor.Error('not-authorized');
        // }
        DiesCollection.remove(dia._id);
    }
});
import { Mongo } from 'meteor/mongo';

export const AreesCollection = new Mongo.Collection('arees');

Meteor.methods({
    'arees.insert'(area){
        // if (!Meteor.userId()){
        // throw new Meteor.Error('not-authorized');
        // }
        AreesCollection.insert({
            ...area,
            createdAt: new Date(),
            user: Meteor.userId()
        });
    },

    'arees.update'(area){
        // if (Meteor.userId() !== allcod.user){
        // throw new Meteor.Error('not-authorized');
        // }
        AreesCollection.update(area._id, {
            $set: {
                ...area
            }
        });
    },

    'arees.delete'(area){
        // if (Meteor.userId() !== allcod.user){
        // throw new Meteor.Error('not-authorized');
        // }
        AreesCollection.remove(area._id);
    },

    'arees.remove'(id, context) {
        AreesCollection.remove(id);
    }

});
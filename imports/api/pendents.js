import { Mongo } from 'meteor/mongo';

export const PendentsCollection = new Mongo.Collection('pendents');

Meteor.methods({
    'pendents.insert'(pendent){
        // if (!Meteor.userId()){
        // throw new Meteor.Error('not-authorized');
        // }
        PendentsCollection.insert({
            ...pendent,
            createdAt: new Date(),
            user: Meteor.userId()
        });
    },

    'pendents.update'(pendent){
        // if (Meteor.userId() !== allcod.user){
        // throw new Meteor.Error('not-authorized');
        // }
        PendentsCollection.update(pendent._id, {
            $set: {
                ...pendent
            }
        });
    },

    'pendents.delete'(pendent){
        // if (Meteor.userId() !== allcod.user){
        // throw new Meteor.Error('not-authorized');
        // }
        PendentsCollection.remove(pendent._id);
    },

    'pendents.remove'(id, context) {
        PendentsCollection.remove(id);
    }

});
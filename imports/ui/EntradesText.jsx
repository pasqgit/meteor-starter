import React, {
  useRef,
  useState
} from 'react';
import { useTracker } from 'meteor/react-meteor-data';
import { EntradesTextCollection } from '../api/entradesText';
import Radium from 'radium';


const EntradesText = Radium(() => {
  
  const [entradaSeleccionada, setEntradaSeleccionada] = useState(null);
  const [creaVisible, setCreaVisible] = useState(false);

  const { 
    subs_entradesTextUsuari,
    entradesText,
    user
  } = useTracker(() => {
    const subs_entradesTextUsuari = Meteor.subscribe('entradesTextUsuari', Meteor.userId());
    const entradesText = EntradesTextCollection.find().fetch();
    const user = Meteor.user();
    return { 
      subs_entradesTextUsuari,
      entradesText,
      user
    };
  });


  const CreaEntradaText = () => {

    //const [arrElems, setArrElems] = useState([]);
  
    const inTitol = useRef();
    const taTextEntrada = useRef();
    const btCreaEntrada = useRef();
  
    return <div
      style={{
        border: `2px solid yellow`,
        background: `grey`,
        padding: `.5em 1em`
      }}>
      <input ref={inTitol} type="text" placeholder="Títol de l'entrada..." />
      
      <ul>
      {
      //  arrElems.map(e => <li>{e}</li>)
      }
      </ul>
      
      {/* Omplir la llista amb elements */}
      <textarea ref={taTextEntrada} placeholder="Text de l'entrada..." 
        style={{
          marginLeft: `2em`
        }}
        autoFocus
        // onKeyDown={ ev => {
        //   ev.key === 'Enter' &&  btCreaElement.current.click();
        // }}
      />
      {/* <button 
        ref={btCreaEntrada}
        onClick={ ev => {
       //   setArrElems([...arrElems, inElement.current.value]);
          inTitol.current.value = "";
        }}>Afegeix element</button> */}
      
      <br />
      <br />
      
      <button onClick={ev => {
        Meteor.call('entradesText.insert', {
          titol: inTitol.current.value,
          cos: taTextEntrada.current.value,
          user: Meteor.userId()
        }, (err, res) => {
          inTitol.current.value = "";
          taTextEntrada.current.value = "";
          //setArrElems([]);
        });
      //  setCreaVisible(false);
      }}>Guarda l'entrada</button>
    </div>;
  };

  const MostraEntrades = () => {
    return <>
      {
        entradesText.map((entrada, ind) => {
          return <BotóEntrada entrada={entrada} />;
        })
      }
    </>;
  };
  
  const MostraEntrada = ({titol, cos}) => {
    return <div style={{
      background: `white`,
      border: `2px solid black`,
      borderRadius: `1em`,
      padding: `1em`,
      margin: `1px auto`
    }}>
      <h3>{titol}</h3>
      <div>{cos}</div>
    </div>;
  };
  
  const BotóEntrada = ({entrada}) => {
    const [liHover, setLiHover] = useState(false);
    
    return <>
      <li style={{
        padding: `1em`,
        border: `1px solid black`,
        margin: `2px`,
        background: `grey`,
        color: `white`,
        ':hover': {
          background: `crimson`
        }
      }} 
      key={entrada?._id}
      onClick={ ev => {  
          setEntradaSeleccionada(entradaSeleccionada ? null : entrada);
      }}
      onMouseEnter={ ev => {
        setLiHover(!liHover);
      }}
      onMouseLeave={ ev => {
        setLiHover(false);
      }}
      >
        <div style={{
          display: `grid`
        }}>
          {/* { liHover && <button onClick={ ev => {
            if (confirm(`Eliminar la entrada "${entrada.titol}"?`)) {
              Meteor.call('entrada.delete', entrada);
            }
          }}>x</button> } */}
          {entrada?.titol}
        </div>
      </li>
    </>;
  }

  return <>
    { user && <>
      <div style={{
        border: `2px solid navy`,
        borderRadius: `1em`,
        background: `beige`
      }}>
        <h2>Entrades de text: </h2>
        <ul style={{
          display: `flex`,
          listStyle: `none`
         }}>{//entrada.map( llista => <BotóLlista llista={llista} />)}
         }
          <MostraEntrades />

          <li><button style={{
            padding: `1em`,
            border: `1px solid black`,
            margin: `2px`,
            background: `grey`,
            color: `white`,
            ':hover': {
              background: `crimson`
            }
          }}
          onClick={ev => {
            setCreaVisible(!creaVisible);
          }}>+</button></li>
        </ul>
      </div>

      <MostraEntrada {...entradaSeleccionada} />
      
      <CreaEntradaText />
      
      {/* { creaVisible && <CreaLlista /> } */}
    </>}
  </>;

});

export { EntradesText, CreaEntradaText };
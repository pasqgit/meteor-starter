import { Meteor } from 'meteor/meteor';
import React, {useState} from 'react';
import { useTracker } from 'meteor/react-meteor-data';

import { FilesCol } from '/imports/api/files.js';
import { EntitatsCollection } from '/imports/api/entitats';
import AvatarFileUpload from './files/AvatarFileUpload';

const Perfil = ({u, setEditaPerfil}) => {

  const [novaImg, setNovaImg] = useState(false);

  const files = useTracker(() => {
    const filesHandle = Meteor.subscribe('files.all');
   // const docsReadyYet = filesHandle.ready();
    const files = FilesCol?.find({userId: Meteor.userId()}, {sort: {name: 1}}).fetch(); // Meteor.userId() ?? "nop"
  
    return files;
  });

  const { 
     entitats
     //, areesPersonals 
     } = useTracker(() => {
          Meteor.subscribe('entitatsUsuari', Meteor.userId());
          const entitats = EntitatsCollection.find({}).fetch();
          // Meteor.subscribe('areesPersonalsUsuari', Meteor.userId());
          // const areesPersonals = AreesPersonalsCollection.find({}).fetch();
          return {
               entitats
               // ,
               // areesPersonals
          };
     });

  const [avatarId, setAvatarId] = useState();
  const [avatarLink, setAvatarLink] = useState();
  
//   const u = Meteor.user;
  //const avatarLink = u.avatarLink;

  // alert("avLnk: ", u.profile.avatarLink);


  function descendentsTrobades(e) {
     return Array.from(new Set(entitats.find({
          $expr: {
               $in: [e._id, "$superiors"]
          }
     })));
  }

  
  return <>
    <h1>Config</h1>
    <div
      style={{
        background: `#dddd`,
        border: `lightblue 3px solid`,
        padding: `.5em 2em`,
        borderRadius: `.7em`,
        fontWeight: `bold`,
        margin: `2em 1em 1em 1em`
      }}
    >
     <h2>Perfil</h2>
     <div style={{
          paddingLeft: `3em`
     }}>
          <h3>Avatar:</h3>
          <div style={{
               paddingLeft: `3em`
          }}>
               {console.log(Meteor.user())}

               <img 
                    style={{
                         width: `6em`,
                         height: `6em`,
                         borderRadius: `50%`,
                         overflow: `hidden`,
                         verticalAlign: `middle`,
                         margin: `.3em`,
                         border: `solid 4px whitesmoke`
                    }}
                    src={u?.profile.avatarLink} 
               />
               <AvatarFileUpload setAvatarId={setAvatarId} setAvatarLink={setAvatarLink} novaImg={novaImg} setNovaImg={setNovaImg} />
               <button onClick={ev => {
                    ev.preventDefault();
                    ev.stopPropagation();
                    
                    Meteor.call('userAvatarUpdate', avatarId, avatarLink, () => {
                         setNovaImg(false);
                    });
                    
                    // alert(`
                    // avatarId: ${avatarId}
                    // avatarLink: ${avatarLink}`);
                    
               }}>Estableix</button>
          </div>

          <h3>Nom d'usuari:</h3>
          <div style={{
               paddingLeft: `3em`
          }}>
               <span style={{
                    verticalAlign: `middle`,
                    margin: `0.3em 0.3em 0.3em auto`,
                    fontWeight: `normal`
               }}>{u?.username}</span>
               <button>Canvia</button>
          </div>
     </div>
    </div>

    <div
      style={{
        background: `#dddd`,
        border: `lightblue 3px solid`,
        padding: `.5em 2em`,
        borderRadius: `.7em`,
        fontWeight: `bold`,
        margin: `2em 1em 1em 1em`
      }}
    >
     <h2>Entitats</h2>
     <div style={{
          paddingLeft: `3em`
     }}>
          <h3>Arbre:</h3>
          <div style={{
               paddingLeft: `3em`
          }}>
              <button onClick={ev => {}}>Repassar estructura de l'arbre</button>
          </div>

          <div title="PRIVILEGIS ELEVATS"
               style={{
                    background: `#f00a`
               }}
          >
          <    h3>OPERACIONS ELEVADES:</h3>

               <button
                    onClick={ev => {
                         let mapDescendentsTrobades = new Map();

                         entitats.forEach(e => {
                              let setTrobades = new Set(
                                   entitats.filter(f => f.superiors?.includes(e._id))
                              );

                              mapDescendentsTrobades.set(e._id, setTrobades);
                         });

                         if (confirm("Vas a alterar l'estructura de l'arbre d'Entitats. Continua sols si saps el que estàs fent.")) {
                              mapDescendentsTrobades.forEach((val, key) => {
                               //    Meteor.call('entitat.matxacaDescendents', key, Array.from(val));
                               //console.log(Array.from(val).map(v => v._id));
                                   Meteor.call('entitat.matxacaDescendents', key, Array.from(val).map(v => v._id)), () => {
                                        alert("Arbre actualitzat");
                                   };
                              });
                         }
                       
                        // console.table(mapDescendentsTrobades);

                    }}
               >
                    Restaura DESCENDENTS a partir de SUPERIORS
               </button>
          
               {/* <button
                    onClick={ev => {

                    }}
               >
                    Restaura SUPERIORS a partir de DESCENDENTS
               </button> */}
          </div>
          
     </div>
    </div>
    
  </>;
};
  
export { Perfil };
import React, { useRef, useState, useContext, useEffect } from "react";
import{ Meteor } from 'meteor/meteor';
import { useTracker } from 'meteor/react-meteor-data';
import { TasquetesCollection } from '/imports/api/tasquetes';
import Radium from 'radium'; 

// Aquesta mini aplicació permet la creació de tasques a una col·lecció de la BD.
// Són vinculades a l'usuari que les crea
// Com a mínim tindrà aquests camps:
// { titol, uid, createdAt, dates_feta }
// El camp 'dates_feta' és un arrai dels moments en que la tasca ha sigut realitzada
//

export const Cos = ({fetes, setFetes}) => {
     const inTitolTasqueta = useRef();
     const { tasquetes } = useTracker(() => {
          Meteor.subscribe('tasquetesUsuari', Meteor.userId());
          const tasquetes = TasquetesCollection.find({}).fetch();
          return {
               tasquetes
          };
     });
     const [triades, setTriades] = useState([]);

     //const 

     return <div style={{display: `flex`}}>
          <fieldset>
               <legend>Crea una tasqueta nova: </legend>
               <input type="text" ref={inTitolTasqueta} autoFocus />               
               <button onClick={ev => {
                    Meteor.call('tasquetes.insert', {titol: inTitolTasqueta.current.value, uid: Meteor.userId(), dates_feta: [], dates_triada: []});
                    inTitolTasqueta.current.value = "";
                    inTitolTasqueta.current.focus();
               }}>Guarda</button>
          </fieldset>
          <fieldset>
               <legend>Tasquetes:</legend>
               <button onClick={ev => {
                    let triada = tasquetes[Math.floor(Math.random()*tasquetes.length)].titol;

                    if (triades.length < tasquetes.length) {

                         if (triada && triades.includes(triada)) {
                              while (triades.includes(triada)) {
                                   triada = tasquetes[Math.floor(Math.random()*tasquetes.length)].titol;
                              }
                         } 
                              
                         setTriades([triada, ...triades]);

                    }
                    
               }}>Extreu una tasqueta</button>
               <ul style={{
                    color: `magenta`,
                    textShadow: `0 0 .2em lime`
                    // ,
                    // "&:first-child": {
                    //      fontSize: `2em`
                    // }
               }}>
                    {
                         triades?.map((tqt,i,a) => <li key={`trd_${i}`} 
                              onClick={ev => {
                                   ev.stopPropagation();

                                   confirm(`Vas a donar la tasqueta "${tqt}" com a feta?`)
                                   && setFetes([tqt, ...fetes])
                              }}
                         >{tqt}</li>)
                    }
               </ul>
               <ul style={{
                    color: `grey`
               }}>
                    {
                         tasquetes?.map((tqt,i,a) => <li key={`tqt_${i}`} 
                              style={{
                                   textDecoration: triades.includes(tqt.titol) ? `line-through` : `none`,
                                   display: triades.includes(tqt.titol) ? `none` : `block`,
                                   fontWeight: triades.includes(tqt.titol) ? `bold` : `none`
                              }}
                              data-tasqueta-index={i}
                              onClick={ev => {
                                   ev.stopPropagation();

                                   confirm(`Vas a triar la tasqueta "${tqt.titol}"?`)
                                   && setTriades([tqt.titol, ...triades])
                              }}
                         >{tqt.titol}</li>)
                    }
               </ul>
          </fieldset>
     </div>;
};
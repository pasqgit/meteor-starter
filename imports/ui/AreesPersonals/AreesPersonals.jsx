import React, { useRef, useState, useContext, useEffect } from "react";
import{ Meteor } from 'meteor/meteor';
import { useTracker } from 'meteor/react-meteor-data';
import { AreesPersonalsCollection } from '/imports/api/areesPersonals';
import Radium from 'radium'; 

// Aquesta mini aplicació permet la creació de tasques a una col·lecció de la BD.
// Són vinculades a l'usuari que les crea
// Com a mínim tindrà aquests camps:
// { titol, uid, createdAt, dates_feta }
// El camp 'dates_feta' és un arrai dels moments en que la tasca ha sigut realitzada
//

function weighted_random(items, weights) {
     var i;
 
     for (i = 0; i < weights.length; i++)
         weights[i] += weights[i - 1] || 0;
     
     var random = Math.random() * weights[weights.length - 1];
     
     for (i = 0; i < weights.length; i++)
         if (weights[i] > random)
             break;
     
     return items[i];
}

export const AreesPersonals = ({fetes, setFetes}) => {
     const inTitolAreaPersonal = useRef();
     const { areesPersonals } = useTracker(() => {
          Meteor.subscribe('areesPersonalsUsuari', Meteor.userId());
          const areesPersonals = AreesPersonalsCollection.find({}).fetch();
          return {
               areesPersonals
          };
     });
     const [triades, setTriades] = useState([]);

     //const 

     return <div style={{display: `flex`}}>
          <fieldset>
               <legend>Crea una Àrea Personal nova: </legend>
               <input type="text" ref={inTitolAreaPersonal} autoFocus />               
               <button onClick={ev => {
                    Meteor.call('areesPersonals.insert', {titol: inTitolAreaPersonal.current.value, uid: Meteor.userId(), dates_feta: [], dates_triada: []});
                    inTitolAreaPersonal.current.value = "";
                    inTitolAreaPersonal.current.focus();
               }}>Guarda</button>
          </fieldset>
          <fieldset>
               <legend>Àrees Personals:</legend>
               <button onClick={ev => {
                    let triada = areesPersonals[Math.floor(Math.random()*areesPersonals.length)].titol;

                    if (triades.length < areesPersonals.length) {

                         if (triada && triades.includes(triada)) {
                              while (triades.includes(triada)) {
                                   triada = areesPersonals[Math.floor(Math.random()*areesPersonals.length)].titol;
                              }
                         } 
                              
                         setTriades([triada, ...triades]);

                    }
                    
               }}>Extreu una Àrea Personal</button>
               <ul style={{
                    color: `magenta`,
                    textShadow: `0 0 .2em lime`
                    // ,
                    // "&:first-child": {
                    //      fontSize: `2em`
                    // }
               }}>
                    {
                         triades?.map((ap,i,a) => <li key={`trd_${i}`}
                              data-ap={ap}
                              onClick={ev => {
                                   ev.stopPropagation();

                                   confirm(`Vas a donar l'Àrea Personal "${ap}" com a feta?`)
                                   && setFetes([ap, ...fetes])
                              }}
                         >{ap}</li>)
                    }
               </ul>
               <ul style={{
                    color: `grey`
               }}>
                    {
                         areesPersonals?.map((ap,i,a) => <li key={`ap_${i}`} 
                              style={{
                                   textDecoration: triades.includes(ap.titol) ? `line-through` : `none`,
                                   display: triades.includes(ap.titol) ? `none` : `block`,
                                   fontWeight: triades.includes(ap.titol) ? `bold` : `none`
                              }}
                              data-ap-index={i}
                              onClick={ev => {
                                   ev.stopPropagation();

                                   confirm(`Vas a triar l'Àrea Personal "${ap.titol}"?`)
                                   && setTriades([ap.titol, ...triades])
                              }}
                         >{ap.titol}</li>)
                    }
               </ul>
          </fieldset>
     </div>;
};
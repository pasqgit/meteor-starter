import React, {
  useRef,
  useState
} from 'react';
import { useTracker } from 'meteor/react-meteor-data';
import { LlistesCollection } from '../api/llistes';
import Radium from 'radium';


const Llistes = Radium(() => {
  
  const [llistaSeleccionada, setLlistaSeleccionada] = useState(null);
  const [creaVisible, setCreaVisible] = useState(false);

  const { 
    subs_llistesUsuari,
    llistes,
    user
  } = useTracker(() => {
    const subs_llistesUsuari = Meteor.subscribe('llistesUsuari', Meteor.userId());
    const llistes = LlistesCollection.find().fetch();
    const user = Meteor.user();
    return { 
      subs_llistesUsuari,
      llistes,
      user
    };
  });


  const CreaLlista = () => {

    const [arrElems, setArrElems] = useState([]);
  
    const inTitol = useRef();
    const inElement = useRef();
    const btCreaElement = useRef();
  
    return <fieldset 
      style={{
        margin: `1em`,
      }}
      onClick={ev => {
        ev.stopPropagation();
      }}
    >
      <legend>Nova llista:</legend>
      <input ref={inTitol} type="text" placeholder="Títol de la llista..." />
      
      <ul>
      {
        arrElems.map(e => <li>{e.caption}</li>)
      }
      </ul>
      
      {/* Omplir la llista amb elements */}
      <input ref={inElement} type="text" placeholder="Text de l'element..." 
        style={{
          marginLeft: `2em`
        }}
        onKeyDown={ ev => {
          ev.key === 'Enter' &&  btCreaElement.current.click();
        }}
      />
      <button 
        ref={btCreaElement}
        onClick={ ev => {
          ev.stopPropagation();

          const elVal = {
            caption: inElement.current.value,
            createdAt: new Date(),
            done: false,
            classes: ""
          };

          setArrElems([...arrElems, elVal]);
          inElement.current.value = "";
        }}
      >Afegeix element</button>
      
      <br />
      <br />
      
      <button onClick={ev => {
        Meteor.call('llistes.insert', {
          titol: inTitol.current.value,
          elements: arrElems
        }, (err, res) => {
          inTitol.current.value = "";
          setArrElems([]);
        });
        setCreaVisible(false);
      }}>Guarda la llista</button>
    </fieldset>;
  };

  const MostraLlista = ({llista}) => {

    const [editMode, setEditMode] = useState(false); // view | edit | completed
    const [arrElements, setArrElements] = useState(llista?.elements);
    const chkDoneRef = useRef();


    const ElementLlista = ({done, caption, classes, createdAt, index}) => {

      const [chkDone, setChkDone] = useState(!!done);


      return <li 
        
        className={classes}
        onClick={ ev => {
          ev.stopPropagation();
         // chkDoneRef.current.checked = !chkDoneRef.current.checked;
         // setChkDone(!chkDone);
        }}
      >
        <input 
          type="checkbox" 
          name="chkDone" 
          ref={chkDoneRef}
          checked={chkDone}
          onChange={ ev => {
            const copiaArrElem = [...llista?.elements];
            copiaArrElem[index].done = !copiaArrElem[index].done;
            
            setArrElements(copiaArrElem);
            
            console.log("arrElems: ", copiaArrElem);
            Meteor.call('llistes.update', {
              ...llista,
              elements: copiaArrElem
            }, () => {
           //   setChkDone(!chkDone);
             // alert(`Llista ${llista.titol} actualitzada`);
            });
          }}
        />
        <label 
          htmlFor="chkDone"
          style={{
            textDecoration: chkDone ? `line-through`: `auto`
          }}
        >{caption}</label>
      </li>;
    };

    return llista && <fieldset 
      style={{
        border: editMode ? `2px dashed yellow` : `1px solid navy`,
        background: editMode ? `red` : `deeppink`,
        padding: `0 2em`,
        borderRadius: `1em`,
        margin: `.5em 1em`
        //width: `30%` // Volem que s'adapte al contingut però que, seguint sent 
      }}
      onClick={ev => {
        ev.stopPropagation();
        setEditMode(!editMode);
      }}
    >
      <legend style={{
        fontSize: `1.5em`,
        fontWeight: `bolder`,
        background: `aqua`,
        border: `2px solid black`
      }}>{llista?.titol}</legend>
      <ul>
        {
          arrElements.map((el, index) => <ElementLlista index={index} {...el} />)
        }
      </ul>
    </fieldset>
  };
  
  const BotóLlista = Radium(({llista}) => {
    const [liHover, setLiHover] = useState(false);
    
    return <>
      <li style={{
        padding: `1em`,
        border: `1px solid black`,
        margin: `2px`,
        background: llista?._id == llistaSeleccionada?._id && `yellow` || `grey`,
        color: llista?._id == llistaSeleccionada?._id && `black` || `white`,
        ':hover': {
          background: `crimson`
        }
      }} 
      key={llista?._id}
      onMouseEnter={ ev => {  
          // setLlistaSeleccionada(llistaSeleccionada ? null : llista);
          setLlistaSeleccionada(llista);
        }}
      onClick={ ev => {
        ev.stopPropagation();
          setLiHover(!liHover);
      }}
      // onMouseLeave={ ev => {
      // //  setLiHover(false);
      // }}
       // onClick={ ev => ev.stopPropagation() }
      >
        <div 
          style={{
            display: `grid`
          }}
          onMouseOver={ ev => {
            ev.stopPropagation();
          }}
        >
          { liHover && <button 
            onClick={ ev => {
              if (confirm(`Eliminar la llista "${llista.titol}"?`)) {
                Meteor.call('llistes.delete', llista);
              }
            }}
            style={{
              position: `absolute`
            }}
          >x</button> }
          {llista.titol}
        </div>
      </li>
    </>;
  });

  return <div 
    style={{
      background: `pink`,
      padding: `1em 0`,
      border: `2px solid white`,
      borderRadius: `2em`
    }}
    onClick={ev => {
      ev.stopPropagation();
      setLlistaSeleccionada(null);
      setCreaVisible(false);
    }}
  >
    { user && <>
      <fieldset 
        style={{
          border: `1px solid navy`,
          borderRadius: `1em`,
          display: `inline-block`,
          background: `navajowhite`,
          padding: `0 1em`
        }}
        onClick={ev => {
          ev.stopPropagation();
          setLlistaSeleccionada(null);
        }}
      >
        <legend style={{
          border: `solid 1px navy`,
          display: `inline-block`,
          borderRadius: `.4em`,
          padding: `0 3px`,
          marginBottom: `0`,
          background: `white`
        }}>Llistes: </legend>
        <ul style={{
          display: `flex`,
          listStyle: `none`
        }}>{llistes.map( llista => <BotóLlista llista={llista} />)}
          <li style={{
            position: `relative`
          }}><button style={{
            padding: `1em`,
            border: `1px solid black`,
            margin: `2px`,
            background: `grey`,
            color: `white`,
            ':hover': {
              background: `crimson`
            }
          }}
          onClick={ev => {
            ev.stopPropagation();
            setCreaVisible(!creaVisible);
          }}>+</button></li>
        </ul>
      </fieldset>
      
      <MostraLlista llista={llistaSeleccionada} />
      
      { creaVisible && <CreaLlista /> }
    </>}
  </div>;

});

export { Llistes };
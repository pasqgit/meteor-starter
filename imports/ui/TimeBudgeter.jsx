import React, { useState, useEffect, useRef } from 'react';
import { useTracker } from 'meteor/react-meteor-data';
import { TimeSessionsCollection } from '../api/timeSessions.js';
import differenceInDays from 'date-fns/differenceInDays'


export function getMsSinceMidnight(d) {
     var e = new Date(d);
     return d - e.setHours(0, 0, 0, 0);
   }
   
export const TimeBudgeter = () => {
   
     const {
       subs_timeSessions,
       timeSessions
     } = useTracker(() => {
       const subs_timeSessions = Meteor.subscribe('timeSessions', Meteor.userId());
       const timeSessions = TimeSessionsCollection.find().fetch();
   
       return {
         subs_timeSessions,
         timeSessions
       };
     });
   
     const [ara, setAra] = useState(new Date());
     // const [acts, setActs] = useState([]);
     const [act, setAct] = useState(null);
   
     const [dia, setDia] = useState({
       init: null,
       totalSpent: 0,
       acts: []
     });
   
     function msToday(t) {
       const horaZero = new Date().setHours(0, 0, 0, 0);
       return t - horaZero;
     }
   
     // if (subs_timeSessions.ready()) {
     let sessio = (ara - timeSessions[timeSessions?.length - 1]?.acts[timeSessions[timeSessions?.length - 1]?.acts?.length - 1] > msToday(ara)) ? {
       init: new Date().setHours(0, 0, 0, 0),
       acts: [],
       user: Meteor.userId()
     } : timeSessions[timeSessions?.length - 1];
     // } else {
     // const [sessio, setSessio] = useState({
     //   init: null,
     //   totalSpent: 0,
     //   stop: null,
     //   acts: [],
     //   user: Meteor.userId()
     // });
     // }
   
     const [configActs, setConfigActs] = useState([
       {
         nomAct: "Dormir",
         horesNominals: 8,
         msRemain: 8 * 60 * 60 * 1000,
         msDue: null,
         init: null,
         maxDue: null,
         doneMsSum: 0,
         color: `black`
       },
       {
         nomAct: "Prod",
         horesNominals: 8,
         msRemain: 8 * 60 * 60 * 1000,
         msDue: null,
         init: null,
         maxDue: null,
         doneMsSum: 0,
         color: `blue`
       },
       {
         nomAct: "Neces",
         horesNominals: 2,
         msRemain: 2 * 60 * 60 * 1000,
         msDue: null,
         init: null,
         maxDue: null,
         doneMsSum: 0,
         color: `red`
       },
       {
         nomAct: "Altres",
         horesNominals: 6,
         msRemain: 6 * 60 * 60 * 1000,
         msDue: null,
         init: null,
         maxDue: null,
         doneMsSum: 0,
         color: `magenta`
       }
     ]);
   
     const refPunter = useRef();
   
     const [anteriorsVisibles, setAnteriorsVisibles] = useState(false);

     useEffect(() => {
       const stTemps = Meteor.setTimeout(() => {
         setAra(new Date());
       }, 1000);
       return () => Meteor.clearTimeout(stTemps);
     });
   
     useEffect(() => {
       const stTemps2 = Meteor.setTimeout(() => {
         const horaZero = new Date().setHours(0, 0, 0, 0);
   
         if (sessio.init && !dia.init) {
           setDia({ ...dia, init: new Date() });
         }
         //setAra(new Date());
         if (ara.getHours() == 0 && ara.getMinutes() == 0) {
           setDia({
             ...dia,
             date: horaZero,
             used: (act && acts.reduce((acc, a, i) => acc + (ara - a.start))),
             budget,
             acts: sessio.acts.map((sa, ind) => sa.start < ara && sa.start.setHours(0, 0, 0, 0))
           })
         }
         //setPD()
       }, 59000);
       return () => Meteor.clearTimeout(stTemps2);
     });
   
   
    const [comentantActivitat, setComentantActivitat] = useState({
        mostrant: false,
        actSeleccionada: null
    });

    const refActCom = useRef();

     function afegeixAct(act) {
       const instant = new Date();
       //let sessio;
       //let nouActs = [...acts];
   
       setAct(act);
   
       //nouActs[nouActs.length]?.end = instant;
   
       //setActs([...nouActs, act])
       if (!dia.init) {
         setDia({
           ...dia,
           init: instant
         })
       }
   
       sessio = ((sessioAnt) => {
         let nouSessActs = [...sessioAnt.acts.slice(0, sessioAnt.acts.length - 1)];
   
         if (sessioAnt.acts.length) {
           nouSessActs = [
             ...nouSessActs,
             {
               ...sessioAnt.acts[sessioAnt.acts.length - 1],
               stop: instant
             }
           ];
         }
   
         return {
           ...sessioAnt,
           init: sessioAnt.init ? sessioAnt.init : instant,
           acts: [
             ...nouSessActs,
             act
           ]
         };
       })(sessio || {
         init: null,
         totalSpent: 0,
         end: null,
         acts: [],
         user: Meteor.userId()
       });
   
       Meteor.call('timeSessions.upsert', sessio);
   
       // setActs([...acts, act]);
       // acts[acts.length].end = instant;
     }
   
     function msSessioInit(sessio) {
          const 
               init = sessio?.init,
               hZeroData = new Date(init).setHours(0,0,0,0)
          ;

          return init - hZeroData;
     }
   
     const Segments = ({sessio, anterior}) => {
       const refSegments = useRef();
   
       return <><div
         style={{
           position: `relative`,
           //left: `calc(${getMsSinceMidnight(new Date()) - getMsSinceMidnight(sessio?.acts[0]?.start) / (24*60*60*1000) * 100}% - 1px)px)`
           left: !anterior && `calc(${getMsSinceMidnight(ara) / (24 * 60 * 60 * 1000) * 100}% - 1px)` || `calc(${getMsSinceMidnight(sessio.init) / (24 * 60 * 60 * 1000) * 100}% - 1px)`,
           whiteSpace: `nowrap`
         }}
       >
         {sessio?.acts?.map((activ, idx) => <div
           ref={refSegments}
           key={`act_${idx}`}
           style={{
             background: activ.color,
             display: `inline-block`,
             minWidth: `1px`,
             width: `${!activ.stop ? ((ara - activ.start) * refPunter.current.clientWidth / (24 * 60 * 60 * 1000)) : ((activ.stop - activ.start) * refPunter.current.clientWidth / (24 * 60 * 60 * 1000))}px`,
             height: `10px`,
             left: !anterior && `-${((ara - sessio?.acts[0].start) * refPunter.current.clientWidth / (24 * 60 * 60 * 1000))}px` || "auto",
             position: `relative`,
             whiteSpace: `nowrap`,
             verticalAlign: `top`
           }}
           title={`${activ.nomAct}: ${msToTime(activ.stop?activ.stop - activ.start:ara - activ.start)}`}
           data-activ={JSON.stringify(activ)}
          //  onMouseEnter={e => {
          //    console.log(JSON.parse(e.target.dataset.activ));
          //  }}
           onClick={e => {
             setComentantActivitat({
               mostrant: !comentantActivitat.mostrant,
              actSeleccionada: {
                ...activ,
                comentaris: []
              }
             });
           }}
         ></div>)}
         <>
          <div style={{
               position: `absolute`,
               background: `cyan`, 
               color: `cyan`,
               zIndex: `2000`,
               width: `1px`, 
               display: `inline-block`, 
               height: `10px`,
               lineHeight: `10px`,
               left: !anterior && 
                   `calc(${((new Date(sessio?.init) - new Date(sessio?.init).setHours(0,0,0,0)) / (24 * 60 * 60 * 1000)) * 100}% - ${getMsSinceMidnight(ara) / (24 * 60 * 60 * 1000) * 100}%)`
                || `calc(${((new Date(sessio?.init) - new Date(sessio?.init).setHours(0,0,0,0)) / (24 * 60 * 60 * 1000)) * 100}% - ${getMsSinceMidnight(sessio?.init) / (24 * 60 * 60 * 1000) * 100}%)`
               }}
          >init</div>
          <div style={{
               position: `absolute`,
               background: `lime`, 
               width: `1px`, 
               display: `inline-block`, 
               height: `10px`,
               lineHeight: `10px`,
               //left: `calc(${((new Date(sessio?.init) - new Date(sessio?.init).setHours(0,0,0,0)) / (24 * 60 * 60 * 1000)) * 100}% - ${getMsSinceMidnight(ara) / (24 * 60 * 60 * 1000) * 100}%)`
               //left: `calc(${(((new Date(new Date(sessio?.init).getTime() + (24*3600000))) - sessio?.init) / (24*3600000)) * 100}%)` // FLIPES AMB LA TONTÀ! QUE NO ESTAVA CLAR QUE HAVIA DE SER 100%??????
               left: !anterior && `calc(${((new Date(sessio?.init) - new Date(sessio?.init).setHours(0,0,0,0)) / (24 * 60 * 60 * 1000)) * 100}% - ${getMsSinceMidnight(ara) / (24 * 60 * 60 * 1000) * 100}% + 100%)`
                || `calc(${((new Date(sessio?.init) - new Date(sessio?.init).setHours(0,0,0,0)) / (24 * 60 * 60 * 1000)) * 100}% - ${getMsSinceMidnight(sessio.init) / (24 * 60 * 60 * 1000) * 100}% + 100%)`
               }}
          >24h</div>
          <div style={{
               position: `absolute`,
               background: `lime`, 
               width: `1px`, 
               display: `inline-block`, 
               height: `10px`,
               lineHeight: `10px`,
               //left: `calc(${((new Date(sessio?.init) - new Date(sessio?.init).setHours(0,0,0,0)) / (24 * 60 * 60 * 1000)) * 100}% - ${getMsSinceMidnight(ara) / (24 * 60 * 60 * 1000) * 100}%)`
               //left: `calc(${(((new Date(new Date(sessio?.init).getTime() + (24*3600000))) - sessio?.init) / (24*3600000)) * 100}%)` // FLIPES AMB LA TONTÀ! QUE NO ESTAVA CLAR QUE HAVIA DE SER 100%??????
               left: !anterior && `calc(${((new Date(sessio?.init) - new Date(sessio?.init).setHours(0,0,0,0)) / (24 * 60 * 60 * 1000)) * 100}% - ${getMsSinceMidnight(ara) / (24 * 60 * 60 * 1000) * 100}% + 200%)`
                || `calc(${((new Date(sessio?.init) - new Date(sessio?.init).setHours(0,0,0,0)) / (24 * 60 * 60 * 1000)) * 100}% - ${getMsSinceMidnight(sessio.init) / (24 * 60 * 60 * 1000) * 100}% + 200%)`
               }}
          >48h</div>
          <div style={{
               position: `absolute`,
               background: `lime`, 
               width: `1px`, 
               display: `inline-block`, 
               height: `10px`,
               lineHeight: `10px`,
               //left: `calc(${((new Date(sessio?.init) - new Date(sessio?.init).setHours(0,0,0,0)) / (24 * 60 * 60 * 1000)) * 100}% - ${getMsSinceMidnight(ara) / (24 * 60 * 60 * 1000) * 100}%)`
               //left: `calc(${(((new Date(new Date(sessio?.init).getTime() + (24*3600000))) - sessio?.init) / (24*3600000)) * 100}%)` // FLIPES AMB LA TONTÀ! QUE NO ESTAVA CLAR QUE HAVIA DE SER 100%??????
               left: !anterior && `calc(${((new Date(sessio?.init) - new Date(sessio?.init).setHours(0,0,0,0)) / (24 * 60 * 60 * 1000)) * 100}% - ${getMsSinceMidnight(ara) / (24 * 60 * 60 * 1000) * 100}% + 300%)`
                || `calc(${((new Date(sessio?.init) - new Date(sessio?.init).setHours(0,0,0,0)) / (24 * 60 * 60 * 1000)) * 100}% - ${getMsSinceMidnight(sessio.init) / (24 * 60 * 60 * 1000) * 100}% + 300%)`
               }}
          >72h</div>
          <div style={{
               position: `absolute`,
               background: `lime`, 
               width: `1px`, 
               display: `inline-block`, 
               height: `10px`,
               lineHeight: `10px`,
               //left: `calc(${((new Date(sessio?.init) - new Date(sessio?.init).setHours(0,0,0,0)) / (24 * 60 * 60 * 1000)) * 100}% - ${getMsSinceMidnight(ara) / (24 * 60 * 60 * 1000) * 100}%)`
               //left: `calc(${(((new Date(new Date(sessio?.init).getTime() + (24*3600000))) - sessio?.init) / (24*3600000)) * 100}%)` // FLIPES AMB LA TONTÀ! QUE NO ESTAVA CLAR QUE HAVIA DE SER 100%??????
               left: !anterior && `calc(${((new Date(sessio?.init) - new Date(sessio?.init).setHours(0,0,0,0)) / (24 * 60 * 60 * 1000)) * 100}% - ${getMsSinceMidnight(ara) / (24 * 60 * 60 * 1000) * 100}% + 400%)`
                || `calc(${((new Date(sessio?.init) - new Date(sessio?.init).setHours(0,0,0,0)) / (24 * 60 * 60 * 1000)) * 100}% - ${getMsSinceMidnight(sessio.init) / (24 * 60 * 60 * 1000) * 100}% + 400%)`
               }}
          >96h</div>
          <div style={{
               position: `absolute`,
               background: `lime`, 
               width: `1px`, 
               display: `inline-block`, 
               height: `10px`,
               lineHeight: `10px`,
               //left: `calc(${((new Date(sessio?.init) - new Date(sessio?.init).setHours(0,0,0,0)) / (24 * 60 * 60 * 1000)) * 100}% - ${getMsSinceMidnight(ara) / (24 * 60 * 60 * 1000) * 100}%)`
               //left: `calc(${(((new Date(new Date(sessio?.init).getTime() + (24*3600000))) - sessio?.init) / (24*3600000)) * 100}%)` // FLIPES AMB LA TONTÀ! QUE NO ESTAVA CLAR QUE HAVIA DE SER 100%??????
               left: !anterior && `calc(${((new Date(sessio?.init) - new Date(sessio?.init).setHours(0,0,0,0)) / (24 * 60 * 60 * 1000)) * 100}% - ${getMsSinceMidnight(ara) / (24 * 60 * 60 * 1000) * 100}% + 500%)`
                || `calc(${((new Date(sessio?.init) - new Date(sessio?.init).setHours(0,0,0,0)) / (24 * 60 * 60 * 1000)) * 100}% - ${getMsSinceMidnight(sessio.init) / (24 * 60 * 60 * 1000) * 100}% + 500%)`
               }}
          >120h</div>
          <div style={{
               position: `absolute`,
               background: `lime`, 
               width: `1px`, 
               display: `inline-block`, 
               height: `10px`,
               lineHeight: `10px`,
               //left: `calc(${((new Date(sessio?.init) - new Date(sessio?.init).setHours(0,0,0,0)) / (24 * 60 * 60 * 1000)) * 100}% - ${getMsSinceMidnight(ara) / (24 * 60 * 60 * 1000) * 100}%)`
               //left: `calc(${(((new Date(new Date(sessio?.init).getTime() + (24*3600000))) - sessio?.init) / (24*3600000)) * 100}%)` // FLIPES AMB LA TONTÀ! QUE NO ESTAVA CLAR QUE HAVIA DE SER 100%??????
               left: !anterior && `calc(${((new Date(sessio?.init) - new Date(sessio?.init).setHours(0,0,0,0)) / (24 * 60 * 60 * 1000)) * 100}% - ${getMsSinceMidnight(ara) / (24 * 60 * 60 * 1000) * 100}% + 600%)`
                || `calc(${((new Date(sessio?.init) - new Date(sessio?.init).setHours(0,0,0,0)) / (24 * 60 * 60 * 1000)) * 100}% - ${getMsSinceMidnight(sessio.init) / (24 * 60 * 60 * 1000) * 100}% + 600%)`
               }}
          >144h</div>
          <div style={{
               position: `absolute`,
               background: `lime`, 
               width: `1px`, 
               display: `inline-block`, 
               height: `10px`,
               lineHeight: `10px`,
               //left: `calc(${((new Date(sessio?.init) - new Date(sessio?.init).setHours(0,0,0,0)) / (24 * 60 * 60 * 1000)) * 100}% - ${getMsSinceMidnight(ara) / (24 * 60 * 60 * 1000) * 100}%)`
               //left: `calc(${(((new Date(new Date(sessio?.init).getTime() + (24*3600000))) - sessio?.init) / (24*3600000)) * 100}%)` // FLIPES AMB LA TONTÀ! QUE NO ESTAVA CLAR QUE HAVIA DE SER 100%??????
               left: !anterior && `calc(${((new Date(sessio?.init) - new Date(sessio?.init).setHours(0,0,0,0)) / (24 * 60 * 60 * 1000)) * 100}% - ${getMsSinceMidnight(ara) / (24 * 60 * 60 * 1000) * 100}% + 700%)`
                || `calc(${((new Date(sessio?.init) - new Date(sessio?.init).setHours(0,0,0,0)) / (24 * 60 * 60 * 1000)) * 100}% - ${getMsSinceMidnight(sessio.init) / (24 * 60 * 60 * 1000) * 100}% + 700%)`
               }}
          >168h</div>
          <div style={{
               position: `absolute`,
               background: `lime`, 
               width: `1px`, 
               display: `inline-block`, 
               height: `10px`,
               lineHeight: `10px`,
               //left: `calc(${((new Date(sessio?.init) - new Date(sessio?.init).setHours(0,0,0,0)) / (24 * 60 * 60 * 1000)) * 100}% - ${getMsSinceMidnight(ara) / (24 * 60 * 60 * 1000) * 100}%)`
               //left: `calc(${(((new Date(new Date(sessio?.init).getTime() + (24*3600000))) - sessio?.init) / (24*3600000)) * 100}%)` // FLIPES AMB LA TONTÀ! QUE NO ESTAVA CLAR QUE HAVIA DE SER 100%??????
               left: !anterior && `calc(${((new Date(sessio?.init) - new Date(sessio?.init).setHours(0,0,0,0)) / (24 * 60 * 60 * 1000)) * 100}% - ${getMsSinceMidnight(ara) / (24 * 60 * 60 * 1000) * 100}% + 800%)`
                || `calc(${((new Date(sessio?.init) - new Date(sessio?.init).setHours(0,0,0,0)) / (24 * 60 * 60 * 1000)) * 100}% - ${getMsSinceMidnight(sessio.init) / (24 * 60 * 60 * 1000) * 100}% + 800%)`
               }}
          >192h</div>
        </>
       </div>
       </>;
     };
   
     function msToTime(msp, clock) {
      const ms = Math.abs(msp);
      
      const
         h = Math.floor(ms / (60 * 60 * 1000)).toString().padStart(2, '0'),
         m = Math.floor((ms / 1000) % 3600 / 60).toString().padStart(2, '0'),
         s = Math.floor((ms / 1000) % 60).toString().padStart(2, '0')
        ;

        if (clock) {
          return `${msp<0?"+":""}${h}:${m}`;
        } else {
          return `${msp<0?"+":""}${h}h${m}'${s}"`;
        }
     }
   
     const Estat = () => {
   
       function msAct(nom) {
         return `${sessio?.acts?.reduce((acc, a, i) => {
           if (a.nomAct == nom) {
             if (a.hasOwnProperty("stop")) {
               return acc + (a.stop - a.start);
             } else {
               return acc + (ara - a.start);
             }
           } else {
             return acc;
           }
         }, 0)}`
       }

       const Comptador = ({actNom}) => <div style={{display: `flex`}}>
          <div
            style={{
              justifyContent: `space-around`
            }}
          > 
              <div style={{
                    color: `${configActs.find((v, i) => v.nomAct == actNom)?.color}`
                  }}>{
                    `${actNom}: ${
                      msToTime(msAct(actNom))
                    }`}
                </div>
                <div
                  style={{
                    color: `${configActs.find((v, i) => v.nomAct == actNom)?.color}`,
                    textDecoration: `${(configActs.find((v, i) => v.nomAct == actNom)?.horesNominals * 3600000 - msAct(actNom) > 0) ? "none" : "underline overline"}`
                  }}
                >{
                  `(${configActs.find((v, i) => v.nomAct == actNom)?.horesNominals * 3600000 - msAct(actNom) > 0 
                      ? msToTime(configActs.find((v, i) => v.nomAct == actNom)?.horesNominals * 3600000 - msAct(actNom)) 
                      : "+"+msToTime((-1)*(configActs.find((v, i) => v.nomAct == actNom)?.horesNominals * 3600000 - msAct(actNom)))
                    })`
                  }
                </div>
            </div>

          <div style={{width: `10em`, height: `1em`, border: `1px solid black`, display: `flex`}}>
            <div style={{
              width: `${100 - ((msAct(actNom) / (configActs.find((v, i) => v.nomAct == actNom)?.horesNominals * 3600000)) * 100)}%`,
              background: `${configActs.find((v, i) => v.nomAct == actNom)?.color}`
            }}></div>
            <div></div>
          </div> 
        </div>
      ;
   
       return <div
         style={{
           border: `3px ridge hsl(0,100%,25%)`,
           margin: `2em auto 0`,
           width: `20em`,
           minHeight: `6em`
         }}
       >
         <div style={{ color: act?.color }}>Activitat actual: {act?.nomAct}</div>
         <hr />
         
         <Comptador actNom="Dormir" />
         <Comptador actNom="Prod" />
         <Comptador actNom="Neces" />
         <Comptador actNom="Altres" />

         <hr />
         <div>Inici de sessió: {sessio?.init?.toString()}</div>
         <div>Sessió: {`${msToTime(ara - sessio?.acts[0]?.start)} (${msToTime(24 * 3600000 - (ara - sessio?.acts[0]?.start))})`}</div>
       </div>;
     };
   
     const Tren = () => {
       return <div
         style={{
           border: `3px ridge hsl(0,100%,25%)`,
           margin: `2em auto 0`,
           width: `20em`,
           minHeight: `6em`
         }}
       >
         {sessio?.acts?.map((a, i) => <div key={`div_${i}`} style={{ display: `inline-block`, margin: `0 1em`, color: a.color }}>
           {a.nomAct}
         </div>)}
       </div>;
     };


    const HistorialDies = () => {
      // let arrDies = Array(differenceInDays(ara, timeSessions[0].init));
      // arrDies.fill(ara);
      let arrDies = [];
      let accDies = new Map();
      let punterData = timeSessions[0].init;
      
      do {
        arrDies.push(punterData.toDateString());
        accDies.set(punterData.toDateString(), []);

        punterData = new Date(punterData.getTime() + 24*60*60*1000);
      } while (punterData.setHours(0,0,0,0) != ara.setHours(0,0,0,0))
      
      // return arrDies.map((d, i) => <div style={{
      //     background: `#bbb`,
      //     width: `90%`,
      //     height: `2em`,
      //     border: `1px solid black`,
      //     margin: `2em auto`,
      //     position: `relative`,
      //     overflow: `visible`,
      //     display: `block`
      //   }}>{String(d)}</div>);

      // let accDiesActs = timeSessions.reduce((acc, v, i) => {
      //   v.acts.forEach((vact, idvact) => {
      //     if ((vact.start.getTime() > v.init.getTime()) && (vact.start.getTime() <= (v.init.getTime() + (24*60*60*1000))) ) {
      //       return acc?.set(vact.start.toDateString(), [...acc.get(vact.start.toDateString()), vact]);
      //     }

      //     console.log("vact: ", vact);
      //     console.log("vact.start: ", vact.start.getTime());

      //     console.log("vInit: ", v.init.getTime());
      //     console.log("acc: ", acc);
      //     console.dir("accDies: ", accDies);

      //   })
      // }, accDies);







      let arrActs = timeSessions.reduce((acc, v, i) => {
        return [...acc, ...v.acts];
      }, []);

      

      //   return [...acc, ...v.acts];
      // }, accDies);

      //console.log("arrActs: ", arrActs);
      // console.log("accDies: ", accDies);
  //    console.log("accDiesActs: ", accDiesActs);

      return <>
        <div>{String(ara)}</div>
        {arrDies
          .reverse()
          .map((ad,i) => <div 
            key={`ad_${i}`}
            title={ad}
            style={{
              background: `#bbb`,
              width: `90%`,
              height: `5px`,
              border: `1px solid black`,
              margin: `.3em auto`,
              position: `relative`,
              overflow: `visible`,
              display: `block`
            }}>
              {/* <Segments
                sessio={{
                  acts: arrActs
                  .filter((ac, acix) => (ac.start.getTime() > new Date(ad).getTime()) && (ac.start.getTime() <= new Date(ad).getTime() + (24*60*60*1000)))
                  //.map((ac, acix) => <div></div>)
                }}
              /> */}
            </div>
          )
        }
        <div>{String(timeSessions[0].init)}</div>
      </>;
    };


     const SessionsAnteriors = () => {
          return timeSessions.sort((a, b) => b.init - a.init).map((ts, i) => {
               return <div 
                  key={`sa_${i}`}
                  style={{
                      background: `#bbb`,
                      width: `90%`,
                      height: `2em`,
                      border: `1px solid black`,
                      margin: `2em auto`,
                      position: `relative`,
                      overflow: `visible`,
                      display: `block`
                }}>
                    <Segments sessio={ts} anterior />
               </div>;
          });
     };   
   
     return <>
       <div
         style={{
           display: `inline-block`,
           position: `absolute`,
           textAlign: `center`,
           fontSize: `3em`,
           zIndex: `10`,
           margin: `0 auto`,
           //width: `100%`,
           // margin-left: auto;
           // margin-right: auto;
           left: `0`,
           right: `0`,
           top: `.5em`,
           // left: "0",
           // color: `white`,
           textShadow: `0 0 .1em black`,
           background: `hsla(0, 100%, 100%, .87)`,
           width: `max-content`,
           padding: `.1em .5em`,
           borderRadius: `.5em`
         }}
       >{(() => {
         let dema = new Date(ara);
         dema.setDate(dema.getDate() + 1);
         let msPerDema = (-1) * (ara - new Date(dema.setHours(0, 0, 0, 0)));
   
         return `${ara.getHours()}:${String(ara.getMinutes()).padStart(2, "0")} (${msToTime(msPerDema, true)})`;
       })()}
       </div>
       
       <br /><br />
       
       <div
         ref={refPunter}
         style={{
           background: `#bbb`,
           width: `90%`,
           height: `10px`,
           border: `1px solid black`,
           margin: `2em auto`,
           position: `relative`,
           overflow: `hidden`
         }}
       >
         <Segments sessio={sessio} />
       </div>

       <div style={{
         display: `flex`
       }}>
         <Estat />
         <Tren />
       </div>
       
       <br /><br />

       

       <div style={{
         display:`flex`,
         justifyContent: `space-around`
       }}>
         <button
           style={{
             background: "blue",
             flexGrow: `1`,
             padding: `2em`
           }}
           onClick={e => {
             afegeixAct({
               nomAct: "Prod",
               start: new Date(),
               overDue: false,
               color: "blue"
             });
           }}
         >Prod</button>
         <button
           style={{
             background: "red",
             flexGrow: `1`,
             padding: `2em`          
           }}
           onClick={e => {
             afegeixAct({
               nomAct: "Neces",
               start: new Date(),
               overDue: false,
               color: "red"
             });
           }}
         >Neces</button>
         <button
           style={{
             background: "magenta",
             flexGrow: `1`,
             padding: `2em`          
           }}
           onClick={e => {
             afegeixAct({
               nomAct: "Altres",
               start: new Date(),
               overDue: false,
               color: "magenta"
             });
           }}
         >Altres</button>
         <button
           style={{
             background: "#ddd",
             flexGrow: `1`,
             padding: `2em`          
           }}
           onClick={e => {
             afegeixAct({
               nomAct: "Dormir",
               start: new Date(),
               overDue: false,
               color: "#000"
             })
           }}
         >Dormir</button>
       </div>
       
       {
         comentantActivitat.mostrant &&
         <>
        <div>
          <div
            style={{
              width: `90%`,
              margin: `1em .5em`
            }}
          >
            {
              comentantActivitat?.actSeleccionada?.comentaris?.map((c,i) => <div 
                style={{
                  background: `grey`,
                  color: `white`,
                  maxWidth: `60%`,
                  border: `1px solid white`,
                  borderRadius: `.5em`,
                  margin: `0 auto`,
                  padding: `.5em 1em`
                }}
                title={c.createdAt}
              >
                {c.text}
              </div>)
            }
          </div>
        </div>
        <div
          style={{
            display: `flex`,
            width: `90%`,
            margin: `1em`
          }}
        >
          <textarea
            ref={refActCom}
            style={{
              display: `block`,
              width: `80%`,
              minHeight: `2em`,
              margin: `1em auto`
            }}
            placeholder="Comenta l'activitat..."
          ></textarea>
          <button
            onClick={e => {
              setComentantActivitat({
                ...comentantActivitat,
                actSeleccionada: {
                  ...comentantActivitat?.actSeleccionada,
                  comentaris: [
                    ...comentantActivitat?.actSeleccionada?.comentaris,
                    { createdAt: ara,
                      text: refActCom.current.value
                    }
                  ]
                }
              });
              refActCom.current.value = "";
              refActCom.current.focus();
            }}
          >
            Guarda el comentari
          </button>
        </div>
        </>
       }

       <div style={{
         display:`flex`
       }}>

        <button
          style={{
            padding: `2em`,
            flexGrow: `1`
          }}
          // onClick={e => setComentantActivitat(
          //   {
          //     mostrant: !comentantActivitat.mostrant
          //   }
          // )}
        >COMENTAR L'ACTIVITAT
        </button>

         <button
           style={{
             padding: `2em`,
             flexGrow: `1`
           }}
           onClick={e => {
             const instant = new Date();
   
             if (confirm("Acabar la sessió actual?") && confirm("Vas a tancar la sessió actual i posaràs tots els valors A ZERO!!")) {
               setAct({
                 nomAct: null,
                 start: instant
               });
   
               Meteor.call('timeSessions.upsert', {...sessio, end: instant});
               Meteor.call('timeSessions.upsert', {
                 init: instant,
                 acts: [],
                 user: Meteor.userId()
               });
             }
           }}
         >REINICIAR LA SESSIÓ
         </button>

       </div>
       <button
        onClick={e => setAnteriorsVisibles(!anteriorsVisibles)}
       >SESSIONS PASSADES
       </button>
       {
        //anteriorsVisibles && <SessionsAnteriors />
          anteriorsVisibles && <HistorialDies />
       }
     </>;
   };
import { Meteor } from 'meteor/meteor';
import React, {useState} from 'react';
import { useTracker } from 'meteor/react-meteor-data';

import { FilesCol } from '/imports/api/files.js';
import { useHistory } from 'react-router-dom';

const UserStat = ({u, setEditaPerfil}) => {

  const history = useHistory();

  const files = useTracker(() => {
    const filesHandle = Meteor.subscribe('files.all');
   // const docsReadyYet = filesHandle.ready();
    const files = FilesCol?.find({"meta.userId": Meteor.userId()}, {sort: {name: 1}}).fetch(); // Meteor.userId() ?? "nop"
  
    return files;
  });
  
  const [mostraMenu, setMostraMenu] = useState(false);

  //const avatarLink = u.avatarLink;

  // alert("avLnk: ", u.profile.avatarLink);
  
  return <>
    <div
      style={{
        // color: `lightblue`,
        position: `absolute`,
        top: `1em`,
        right: `1em`,
        cursor: `pointer`,
        background: `yellow`,
        border: `lightblue 3px solid`,
        //padding: `.15em`,
        borderRadius: `.7em`,
        fontWeight: `bold`,
      }}
      onMouseEnter={ev => {
        setMostraMenu(true);
      }}
     
      // title="Logout"

      onClick={ev => {
        ev.stopPropagation();
        ev.preventDefault();

        history.push(`/config`);
      }}
    >
      <img 
        style={{
          width: `3em`,
          height: `3em`,
          borderRadius: `50%`,
          overflow: `hidden`,
          verticalAlign: `middle`,
          margin: `.3em`,
          border: `solid 4px whitesmoke`
        }}
        src={u?.profile?.avatarLink} 
      />
      <span style={{
        verticalAlign: `middle`,
        margin: `0.3em 0.3em 0.3em auto`
      }}>{u?.username}</span>
    </div>
    {
      mostraMenu &&
      <div style={{
          position: `absolute`,
          background: `white`,
          right: `0`,
          top: `5em`,
          padding: `.4em .5em`,
          border: `1px   #aaa`,
          cursor: `pointer`,
          zIndex: `200`
        }}
        onMouseLeave={ev => {
          setMostraMenu(!mostraMenu);
        }}
      >
        <div style={{
            padding: `.2em`
          }}
          onClick={ev => {
            //setEditaPerfil(true);
            ev.stopPropagation();
            ev.preventDefault();
            
            history.push(`/config`);
          }}
        >Configuració</div>
        <div style={{
          padding: `.2em`
        }}
        onClick={() => Meteor.logout()}
        >Tanca la sessió</div>
      </div>
    }
  </>;
};
  
export { UserStat };
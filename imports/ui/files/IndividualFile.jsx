import React from 'react';
// import PropTypes from 'prop-types';

const IndividualFile = props => {
 
//   propTypes: {
//     fileName: PropTypes.string.isRequired,
//     fileSize: PropTypes.number.isRequired,
//     fileUrl: PropTypes.string,
//     fileId: PropTypes.string.isRequired
//   }

  function removeFile(){
    let conf = confirm('Are you sure you want to delete the file?') || false;
    if (conf == true) {
      Meteor.call('RemoveFile', props.fileId, function (err, res) {
        if (err)
          console.log(err);
      })
    }
  }

  function renameFile(){

    let validName = /[^a-zA-Z0-9 \.:\+()\-_%!&]/gi;
    let prompt    = window.prompt('New file name?', props.fileName);

    // Replace any non valid characters, also do this on the server
    if (prompt) {
      prompt = prompt.replace(validName, '-');
      prompt.trim();
    }

    if (!_.isEmpty(prompt)) {
      Meteor.call('RenameFile', props.fileId, prompt, function (err, res) {
        if (err)
          console.log(err);
      })
    }
  }

  return <div className="m-t-sm">
      <img src={props.fileUrl} alt={props.fileName} style={{maxWidth: `200px`, maxHeight: `200px`}}/>

      <div className="row">
        <div className="col-md-12">
          <strong>{props.fileName}</strong>
          <div className="m-b-sm">
          </div>
        </div>
      </div>

      <div className="row">
        <div className="col-md-3">
          <button onClick={renameFile} className="btn btn-outline btn-primary btn-sm">
            Rename
          </button>
        </div>


        <div className="col-md-3">
          <a href={props.fileUrl} className="btn btn-outline btn-primary btn-sm"
             target="_blank">View</a>
        </div>

        <div className="col-md-2">
          <button onClick={removeFile} className="btn btn-outline btn-danger btn-sm">
            Delete
          </button>
        </div>

        <div className="col-md-4">
          Size: {props.fileSize}
        </div>
      </div>
    </div>
  }
export default IndividualFile;
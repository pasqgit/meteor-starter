import { useTracker } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import React, { useState, useRef } from 'react';
// import PropTypes from 'prop-types';
import { FilesCol } from '/imports/api/files.js';

import IndividualFileEnt from '/imports/ui/files/IndividualFileEnt.jsx';

import _ from 'lodash';



const LlistaArxiusEnt = (props) => {
  
  const files = useTracker(() => {
    const filesHandle = Meteor.subscribe('files.all');
   // const docsReadyYet = filesHandle.ready();
    const files = FilesCol?.find({meta:{userId: props.uidProvisional || Meteor.userId(), entId: props.entId}}, {sort: {name: 1}}).fetch(); // Meteor.userId() ?? "nop"
  
    return files;
  });

  // This is our progress bar, bootstrap styled
  // Remove this function if not needed
  // function showUploads() {
  //   //console.log('**********************************', uploading);

  //   if (!_.isEmpty(uploading)) {
  //     return <div>
  //       {uploading.file.name}

  //       <div className="progress progress-bar-default">
  //         <div style={{width: progress + '%'}} aria-valuemax="100"
  //            aria-valuemin="0"
  //            aria-valuenow={progress || 0} role="progressbar"
  //            className="progress-bar">
  //           <span className="sr-only">{progress}% Complete (success)</span>
  //           <span>{progress}%</span>
  //         </div>
  //       </div>
  //     </div>
  //   }
  // }

  {
  //  debug("Rendering FileUpload",docsReadyYet);
    if (files /* && docsReadyYet*/) {

      let fileCursors = files;

      // Run through each file that the user has stored
      // (make sure the subscription only sends files owned by this user)
      let display = fileCursors.map((aFile, key) => {
        // console.log('A file: ', aFile.link(), aFile.get('name'))
        let link = FilesCol.findOne({_id: aFile._id}).link();  //The "view/download" link

        // Send out components that show details of each file
        return <div 
          key={'file' + key}
        >
          <IndividualFileEnt
            fileName={aFile.name}
            fileUrl={link}
            fileId={aFile._id}
            fileSize={aFile.size}
          />
        </div>
      })

      return <div
        style={{
          display: `flex`,
          justifyContent: `space-evenly`,
          alignItems: `center`,
          flexWrap: `wrap`
        }}
      >

        {display}

      </div>
    }
    else return <div>Loading file list</div>;
  }
};

export default LlistaArxiusEnt;
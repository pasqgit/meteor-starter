import { Meteor } from 'meteor/meteor';
import React from 'react';
import { useTracker } from 'meteor/react-meteor-data';

import { FilesCollection } from '/imports/api/files.js';
import FileUpload from '/imports/ui/files/FileUpload.jsx';


const Files = () => {

    const {
        files,
        docsReadyYet
    } = useTracker(() => {

        const docsReadyYet = Meteor.subscribe('files.all');
        const files = FilesCollection?.find().fetch();

        return {
            files,
            docsReadyYet
        };
    });

    return <>
        <h1 style={{
            fontFamily: `cyber`,
            padding: `.7rem`,
            color: `#ff0c`,
            textShadow: `.02em .02em .1em black`
        }}>Files</h1>
        {   docsReadyYet.ready()
            ?   <FileUpload files={files} docsReadyYet={true} />
            :   <FileUpload files={files} docsReadyYet={false} />
        }
    </>;
};

export { Files };
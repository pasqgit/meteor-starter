import { useTracker } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import React, { useState, useRef } from 'react';
// import PropTypes from 'prop-types';
import { FilesCol } from '/imports/api/files.js';

import IndividualFile from '/imports/ui/files/IndividualFile.jsx';

import _ from 'lodash';

const debug = require('debug')('demo:file');

const FileUploadComponent = (props) => {
  
  const [uploading, setUploading] = useState([]);
  const [progress, setProgress] = useState(0);
  const [inProgress, setInProgress] = useState(false);

  const fileinput = useRef();

  const files = useTracker(() => {
    const filesHandle = Meteor.subscribe('files.all');
   // const docsReadyYet = filesHandle.ready();
    const files = FilesCol?.find({meta:{userId: props.uidProvisional || Meteor.userId()}}, {sort: {name: 1}}).fetch(); // Meteor.userId() ?? "nop"
  
    return files;
  });

    

  function uploadIt(e) {
    e.preventDefault();

    //let self = this;

    if (e.currentTarget.files && e.currentTarget.files[0]) {
      // We upload only one file, in case
      // there was multiple files selected
      var file = e.currentTarget.files[0];

      if (file) {
        let uploadInstance = FilesCol.insert({
          file,
          meta: {
            locator: props.fileLocator,
            userId: props.uidProvisional || Meteor.userId() // Optional, used to check on server for file tampering
          },
          chunkSize: 'dynamic',
          allowWebWorkers: true // If you see issues with uploads, change this to false
        }, false)

        self.setState({
          uploading: uploadInstance, // Keep track of this instance to use below
          inProgress: true // Show the progress bar now
        });

        // These are the event functions, don't need most of them, it shows where we are in the process
        uploadInstance.on('start', function () {
          console.log('Starting');
        })

        uploadInstance.on('end', function (error, fileObj) {
          console.log('On end File Object: ', fileObj);
        })

        uploadInstance.on('uploaded', function (error, fileObj) {
          console.log('uploaded: ', fileObj);

          props.setUpId(fileObj._id);

          // Remove the filename from the upload box
          fileinput.current.value = '';

          // Reset our state for the next file
          setUploading([]);
          setProgress(0);
          setInProgress(false);
        })

        uploadInstance.on('error', function (error, fileObj) {
          console.log('Error during upload: ' + error)
        });

        uploadInstance.on('progress', function (progress, fileObj) {
          console.log('Upload Percentage: ' + progress)
          // Update our progress bar
          setProgress(progress);
        });

        uploadInstance.start(); // Must manually start the upload
      }
    }
  }

  // This is our progress bar, bootstrap styled
  // Remove this function if not needed
  function showUploads() {
    console.log('**********************************', uploading);

    if (!_.isEmpty(uploading)) {
      return <div>
        {uploading.file.name}

        <div className="progress progress-bar-default">
          <div style={{width: progress + '%'}} aria-valuemax="100"
             aria-valuemin="0"
             aria-valuenow={progress || 0} role="progressbar"
             className="progress-bar">
            <span className="sr-only">{progress}% Complete (success)</span>
            <span>{progress}%</span>
          </div>
        </div>
      </div>
    }
  }


  {
  //  debug("Rendering FileUpload",docsReadyYet);
    if (files /* && docsReadyYet*/) {

      let fileCursors = files;

      // Run through each file that the user has stored
      // (make sure the subscription only sends files owned by this user)
      let display = fileCursors.map((aFile, key) => {
        // console.log('A file: ', aFile.link(), aFile.get('name'))
        let link = FilesCol.findOne({_id: aFile._id}).link();  //The "view/download" link

        // Send out components that show details of each file
        return <div key={'file' + key}>
          <IndividualFile
            fileName={aFile.name}
            fileUrl={link}
            fileId={aFile._id}
            fileSize={aFile.size}
          />
        </div>
      })

      return <div>

        <div className="row">
          <div className="col-md-12">
            <p>Upload New File:</p>
            <input type="file" 
              id="fileinput" 
              disabled={inProgress} 
              ref={fileinput}
              onChange={uploadIt} 
            />
          </div>
        </div>

        <div className="row m-t-sm m-b-sm">
          <div className="col-md-6">

            {showUploads()}

          </div>
          <div className="col-md-6">
          </div>
        </div>

        {display}

      </div>
    }
    else return <div>Loading file list</div>;
  }
};

//
// This is the HOC - included in this file just for convenience, but usually kept
// in a separate file to provide separation of concerns.
//
// export default withTracker( ( props ) => {

//   const filesHandle = Meteor.subscribe('files.all');
//   const docsReadyYet = filesHandle.ready();
//   const files = FilesCol.find({meta:{userId: props.uidProvisional || Meteor.userId()}}, {sort: {name: 1}}).fetch(); // Meteor.userId() ?? "nop"

//   return {
//     docsReadyYet,
//     files,
//   };

// })(FileUploadComponent);

export default FileUploadComponent;
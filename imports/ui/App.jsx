import React, { useState, useEffect, useRef, useMemo } from 'react';
import { Meteor } from 'meteor/meteor';
import { useTracker } from 'meteor/react-meteor-data';
//import { Hello } from './Hello.jsx';

import { UserStat } from './UserStat.jsx';
import { EntradaSessio } from './EntradaSessio.jsx';
import { Dia } from '/imports/ui/Dia.jsx';
import { BrowserRouter as Router, Route, Link, useHistory, Redirect, Switch } from 'react-router-dom';
import { DnD } from '/imports/ui/dnd/DnD.jsx';
import { Files } from '/imports/ui/files/Files.jsx';
import { TimeBudgeter } from './TimeBudgeter.jsx';
import { LaGerra } from './LaGerra.jsx';

import { NavBar } from '/imports/ui/NavBar/NavBar.jsx';
import { Dashboard } from './Dashboard/Dashboard.jsx';
import { AppContext } from '../api/Contexts/AppContext.js';
import { Cicles } from './Cicles/Cicles.jsx';
import { Tasquetes } from './Tasquetes/Tasquetes.jsx';
import { Entitats, Ent } from '/imports/ui/Entitats/Entitats.jsx';
import { AreesPersonals } from './AreesPersonals/AreesPersonals.jsx';
import { DashboardDelDia } from './Dashboard/DashboardDelDia.jsx';
import { FolderTM } from '/imports/ui/FolderTM/FolderTM.jsx';

import { EntitatsCollection } from '/imports/api/entitats';
import { Perfil } from './Perfil.jsx';


import { Roles } from 'meteor/alanning:roles';

export const App = () => {
  const [mostraData, setMostraData] = useState();
  const [editaPerfil, setEditaPerfil] = useState();
  
  const user = useTracker(() => Meteor.user());

  // const history = useHistory

  const { 
    entitats
    //, areesPersonals 
  } = useTracker(() => {
      Meteor.subscribe('entitatsUsuari', Meteor.userId());
      const entitats = EntitatsCollection.find().fetch();
      // Meteor.subscribe('areesPersonalsUsuari', Meteor.userId());
      // const areesPersonals = AreesPersonalsCollection.find({}).fetch();
      return {
          entitats
          // ,
          // areesPersonals
      };
  });

  const data_dnd = [
    {
      title: 'Group 0',
      items: ['0', '1', '2', '3']
    },
    {
      title: 'Group 1',
      items: ['4', '5']
    }
  ];

  function colorDeMes(nMes, offset, s, l, a) {
    return `hsla(${nMes+8 * (30 + offset)}, ${s}%, ${l}%, ${a})`;
  }

  return <Router>
    <AppContext.Provider value={{mostraData, setMostraData}}>
      <div
        style={{
          background: colorDeMes(new Date().getMonth(), 0, 100, 50, .5)
        }}
      >
        {
          Meteor.userId() && <NavBar>
            <nav>
              <div><Link to="/" >Inici</Link></div>
              <Link to="/entitats" >Entitats</Link>
              <Link to="/dashboardDelDia" >Dashboard Diari</Link>
              {/* <Link to="/areesPersonals" >Àrees Personals</Link>
              <Link to="/tasquetes" >Tasquetes</Link> */}
              <Link to="/temps" >Temps</Link>
              <Link to="/dnd">DnD</Link>
              <Link to="/files">Files</Link>
              <Link to="/dia">Dia</Link>
              <Link to="/time-budgeter">TimeBudgeter</Link>
              <Link to="/la-gerra">LaGERRAAA!</Link>
              <Link to="/cicles">Cicles</Link>
              <Link to="/foldertm">FolderTM</Link>
            </nav>
            <UserStat u={user} setEditaPerfil={setEditaPerfil} />
          </NavBar>
        }
        <div style={{
          padding: `.5em`
          // height: `100%`
        }}>
          {
            Meteor.userId() ? <>
              <Route exact path="/">
                {/* <Tasquetes /> */}
                {
                  Roles.userIsInRole(Meteor.userId(), 'admin', 'starter') 
                    && <h2>Admin confirmat</h2>
                }
              </Route>

              <Route exact path="/entitats">
                <Entitats />
                {/* <Tree treeData={treeData} /> */}
              </Route>

              <Route exact path="/ent/:id">
                <Ent entitats={entitats} />
              </Route>

              <Route exact path="/dashboardDelDia">
                <DashboardDelDia />
              </Route>
              
              <Route exact path="/areesPersonals">
                <AreesPersonals />
              </Route>

              <Route exact path="/tasquetes">
                <Tasquetes />
              </Route>
              
              <Route exact path="/dnd">
                <DnD data={data_dnd} />
              </Route>

              <Route exact path="/temps">
                <Dashboard />
              </Route>

              <Route exact path="/files">
                <Files />
              </Route>

              <Route exact path="/dia">
                <Dia />
              </Route>

              <Route exact path="/time-budgeter">
                <TimeBudgeter />
              </Route>

              <Route exact path="/la-gerra">
                <LaGerra />
              </Route>

              <Route exact path="/cicles">
                <Cicles />
              </Route>

              <Route exact path="/foldertm">
                <FolderTM />
              </Route>

              <Route exact path="/config">
                <Perfil u={user} />
              </Route>

            </>
            : <>              
              <Redirect to="/" />
              <EntradaSessio />
            </>
          }
        </div>
      </div>
    </AppContext.Provider>
  </Router>;
};

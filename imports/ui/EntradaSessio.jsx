import { Meteor } from 'meteor/meteor';
import { useHistory, useParams, Link } from "react-router-dom";
import React, {
  useState
} from 'react';

import { LoginForm } from '/imports/ui/LoginForm.jsx';
import { Register } from './Registre.jsx';

const EntradaSessio = () => {
    let history = useHistory();

    const [enterMode, setEnterMode] = useState(null);
    return <>
      <button onClick={() => setEnterMode("login")}>Entra</button>
      <button onClick={() => setEnterMode("register")}>Registra't</button>
      {(() => {
        switch (enterMode) {
          case "login": {
            history.push("/login");
            return <LoginForm />;
          }
          case "register": {
            history.push("/register");
            return <Register />;
          }
          default: return null;
        }
      })()}
    </>;
};

export { EntradaSessio };
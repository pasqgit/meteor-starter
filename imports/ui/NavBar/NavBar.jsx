import React from 'react';

export const NavBar = ({ children }) => <div style={{
     position: `relative`,
     background: `#fffe`,
     top: `0`,
     //overflow: `hidden`,
     boxShadow: `0 0 .3em black`,
     width: `100%`,
     height: `3em`,
     margin: `0`
}}>
     {children}
</div>;
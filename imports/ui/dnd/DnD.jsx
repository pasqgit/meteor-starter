import React, { useState, useEffect, useRef } from 'react';


const DnD = ({data}) => {
    const [list, setList] = useState(data);
    const [dragging, setDragging] = useState(false);
    
    const dragItem = useRef();
    const dragNode = useRef();

    const handleDragEnd = (ev, params) => {
        setDragging(false);
        dragNode.current.removeEventListener('dragend', handleDragEnd);
        dragItem.current = null;
        dragNode.current = null;
    };

    const handleDragStart = (ev, params) => {
        console.log(`Drag starting...`, params);

        dragItem.current = params;
        dragNode.current = ev.target;
        dragNode.current.addEventListener('dragend', handleDragEnd);
        setTimeout(() => {
            setDragging(true);
        }, 0);
        return false;
    };

    const handleDragEnter = (ev, params) => {
        const currentItem = dragItem.current;

        console.log("Entering drag...", params);
        if (ev.target !== dragNode.current) {

         //   console.log("TARGET IS NOT THE SAME");
            setList(oldList => {
                let newList = JSON.parse(JSON.stringify(oldList));
                newList[params.gIdx].items.splice(params.itIdx, 0, newList[currentItem.gIdx].items.splice(currentItem.itIdx, 1)[0]);
                dragItem.current = params;
                return newList;
            });
        }
    };

    const getStyles = (gIdx, itIdx) => {
        const currentItem = dragItem.current;

        if (gIdx === currentItem.gIdx && itIdx === currentItem.itIdx) {
            return "current dnd-item";
        }
        return "dnd-item";
    };


    return <div className="drag-n-drop">
        {
            list?.map((g, gIdx) => {
                return <div 
                    className="dnd-group" 
                    key={g.title}
                    onDragEnter={g.items.length == 0 && dragging ? ev => handleDragEnter(ev, {gIdx, itIdx: 0}) : null }
                >
                    <div className="dnd-group-title">{g.title}</div>
                        {
                            g.items.map((it, itIdx) => {
                                return <div 
                                    draggable 
                                    onDragStart={ev => {
                                        handleDragStart(ev, {gIdx, itIdx})
                                    }}
                                    onDragEnter={dragging ? ev => {handleDragEnter(ev, {gIdx, itIdx})} : null}
                                    className={dragging ? getStyles(gIdx, itIdx) : "dnd-item"}
                                    key={it}
                                >
                                    <div>
                                        <p>{it}</p>
                                    </div>
                                </div>;
                            })
                        }
                    </div>
                ;
            })
        }                
    </div>;
};

export { DnD };
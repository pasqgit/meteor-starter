import { Random } from 'meteor/random';
import React, { useState } from 'react';

import { Accounts } from 'meteor/accounts-base';

import { FilesCollection } from '/imports/api/files.js';

import { FileUpload } from '/imports/ui/files/FileUpload.jsx';

import {
    BrowserRouter as Router,
    Route,
    Link,
    useParams,
    useRouteMatch,
    Switch,
    useHistory,
    Redirect
  } from "react-router-dom";

import AvatarFileUpload from '/imports/ui/files/AvatarFileUpload.jsx';

const Register = props => {
    const history = useHistory();

    const [avatarId, setAvatarId] = useState();
    const [avatarLink, setAvatarLink] = useState();

    const [chkAdmin, setChkAdmin] = useState(false);

    //alert(`Random.id: ${Random.id()}`);

    return <div>
                <h2>Regístrate</h2>
                
                <fieldset>
                    <legend>Sobre tu: </legend>
                    {/* Imatge de perfil: <FileUpload uidProvisional={Random.id()} setAvatarId={setAvatarId} /> */}
                    <AvatarFileUpload setAvatarId={setAvatarId} setAvatarLink={setAvatarLink} />
                    <input type="text"
                        id="login-nombre"
                        className="form-control input-lg"
                        placeholder="Nombre"
                    />
                    <input type="text"
                        id="login-apellidos"
                        className="form-control input-lg"
                        placeholder="Apellidos"
                    />
                    <label htmlFor="inDataNaix">Data de naixement: </label><input type="date" id="inDataNaix" name="inDataNaix" />
                    <input type="text"
                        id="login-genere"
                        className="form-control input-lg"
                        placeholder="Genere"
                    />
                    <textarea placeholder="Quin objectiu creus que hauria de perseguir aquesta comunitat?" />
                </fieldset>

                <fieldset>
                    <legend>Contacte: </legend>
                    <input type="email"
                        id="login-email"
                        className="form-control input-lg"
                        placeholder="Email"
                    />
                    <input type="text"
                        id="login-tel"
                        className="form-control input-lg"
                        placeholder="Teléfono"
                    />
                    <input type="text"
                        id="login-provincia"
                        className="form-control input-lg"
                        placeholder="Provincia"
                    />
                    <input type="text"
                        id="login-cp"
                        className="form-control input-lg"
                        placeholder="Código Postal"
                    />
                    <input type="text"
                        id="login-ciudad"
                        className="form-control input-lg"
                        placeholder="Ciudad"
                    />
                </fieldset>
                
                <fieldset>
                    <legend>Necessària: </legend>

                    <label htmlFor="chkAdmin">Admin?</label>
                    <input 
                        type="checkbox" 
                        name="chkAdmin" 
                        // ref={refChkAdmin} 
                        onChange={ ev => {
                            setChkAdmin(!!ev.target.checked);
                        }}
                        checked={chkAdmin}
                    />


                    <input type="text"
                        id="login-username"
                        className="form-control input-lg"
                        placeholder="Nombre de usuario"
                    />
                    <input type="password"
                        id="login-password"
                        className="form-control input-lg"
                        placeholder="Contraseña"
                    />
                    <input type="password"
                        id="login-password2"
                        className="form-control input-lg"
                        placeholder="Repite la contraseña"
                    />
                </fieldset> 
                
                <input type="button"
                    id="register-button"
                    className="btn btn-block submit_btn"
                    value="Registrarte" 
                    onClick={ev => {

                        const rols = chkAdmin ? ["admin", "user"] : ["user"];
                        let id;

                        try {
                            id = Meteor.call('creaUsuariAmbRols', {
                                username: document.querySelector("#login-username").value,
                                password: document.querySelector("#login-password").value,
                                profile: {
                                    avatarId,
                                    avatarLink
                                },
                                opts: {
                                    email: document.querySelector("#login-email").value, 
                                    nombre: document.querySelector("#login-nombre").value,
                                    apellidos: document.querySelector("#login-apellidos").value,
                                    tel: document.querySelector("#login-tel").value,
                                    provincia: document.querySelector("#login-provincia").value,
                                    cp: document.querySelector("#login-cp").value,
                                    ciudad: document.querySelector("#login-ciudad").value,
                                    refs: [],
                                    activado: false,
                                    dataNaix: document.querySelector('#inDataNaix').value,
                                    rols,
                                    avatarId,
                                    avatarLink
                                }
                            }, 
                            'starter', // Àmbit
                            () => {
                                    alert("Usuario creado con éxito. Revisaremos su solicitud y nos comunicaremos con usted lo más rápido posible.");
                                    // try {
                                    //     Meteor.call('avatarUserSet', avatarId, avatarLink);
                                    // } catch (err) {
                                    //     alert("Error assignant l'avatar");
                                    //     console.dir(err);
                                    // }
                                    Meteor.loginWithPassword(document.querySelector("#login-username").value, document.querySelector("#login-password").value);
                                    history.push("/");
                                }
                            );
                        } catch (err) {                                
                            alert("Error creando usuario");
                            console.dir(err);
                        }

                    }}
                />
                <div className="form-group text-center">
                    ¿Ya tienes una cuenta? <a href="#" onClick={ev => {
                        ev.preventDefault();
                        history.push("/login");
                    }}>Identifícate</a>
                </div>
    </div>;
};

export { Register };
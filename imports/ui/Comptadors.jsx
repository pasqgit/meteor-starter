import React from 'react';

const Comptador = ({width, height, complet, color}) => {
    return <div
        style={{
            width,
            height,
            background: `white`,
            border: `1px solid grey`,
            position: `relative`,
            justifyContent: `center`
        }}
    >
        <div
            style={{
                background: color,
                height: `100%`,
                width: `${complet}%`,
                textAlign: `center`,
                display: `flex`,
                alignItems: `center`,
                justifyContent: `center`
            }}
        >
            <span>{complet}%</span>
        </div>
    </div>;
};

export {Comptador};
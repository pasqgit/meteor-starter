import React, { useRef, useState, useContext, useEffect } from "react";
import{ Meteor } from 'meteor/meteor';
import { useTracker } from 'meteor/react-meteor-data';
//import { DatesCollection } from '/imports/api/dates.js';
import { PanellsDreta, PanellDretUsuaris, PanellDretOpcions } from '/imports/ui/Panells/PanellsDreta.jsx';
import { DatesCollection } from '/imports/api/dates.js';
import Radium from 'radium'; 
import { AppContext } from "../../api/Contexts/AppContext";
import { NotesDelDia } from "../Panells/NotesDelDia";
import { DatesContext } from "../../api/Contexts/DatesContext";


export const Dashboard = () => {

     const { mostraData, setMostraData } = useContext(AppContext);

     const user = useTracker(() => Meteor.user());
   
     // Tenim la llista dels dies entre 2 dates. Però la UX no és la millor. L'haurem d'anar revisant.
     //
     //  FIXME: UX
     //    - Un dels problemes és que l'acció desencadenant no està vinculada directament al botó de llistar
     //    - La data no està ben formatada
     //
     //  TODO: Colorejar mesos
     //    - Colors pastís amb hsl amb intervals de 30º
     // 
   
     const
       [decades, setDecades] = useState(),
       [mostraVida50, setMostraVida50] = useState(true)
     ;
   
     const
       [dataStart, setDataStart] = useState(new Date(user?.profile?.dataNaix).setUTCHours(0,0,0,0)),
       [dataEnd, setDataEnd] = useState(`${String(new Date().getFullYear())}-${String(new Date().getMonth() + 1).padStart(2, "0")}-${String(new Date().getDate()).padStart(2, "0")}`),
       [colorOffset, setColorOffset] = useState(0)
     ;

     const [dates, data] = useTracker(() => {
        Meteor.subscribe('datesUsuari', Meteor.userId());
        return [DatesCollection.find().fetch(), DatesCollection.find({date: new Date(new Date(dataEnd)?.setUTCHours(0,0,0,0))}).fetch()];
      });
   
     const
       refStart = useRef(),
       refEnd = useRef()
     ;
   
     useEffect(() => {
      const stTemps = Meteor.setTimeout(() => {
        setDataEnd(`${String(new Date().getFullYear())}-${String(new Date().getMonth() + 1).padStart(2, "0")}-${String(new Date().getDate()).padStart(2, "0")}`);
      }, 60000);
      return () => Meteor.clearTimeout(stTemps);
     })
    
     const VidaReal = () => {
     
          const refTaDataText = useRef();
          
          
      
          // console.log("Dates: ", dates);
      
          const Dia = ({ punterDat, dataStart, dataEnd }) => {
         //   const { dataEnd } = useContext(DatesContext);
            return <div className="divdia"
              style={{
                //background: dates?.some(d => new Date(d.date).getTime() === punterDat.getTime()) ? `black` : (punterDat < new Date(dataStart) || punterDat > new Date(dataEnd)) && punterDat.getDate() !== 1 ? "white" : colorDeMes(new Date(punterDat).getMonth(), colorOffset, 100, 50, 1),
                background: ( // El fons serà NEGRE per a les dates NO VISCUDES excepte els primers de cada mes.
                  dates?.some(d => new Date(d.date).getTime() === punterDat.getTime())
                  ? 
                    // (punterDat < new Date(dataStart)  // Abans de nàixer 
                    //   || punterDat > new Date(dataEnd)  // Després d'avui (o final del periode)
                    // )
                    // ? "black"
                    // : 
                    punterDat.getDate() === new Date(dataStart).getDate() 
                      && punterDat.getMonth() === new Date(dataStart).getMonth() 
                      && `red` 
                      || "white"
                  : colorDeMes(new Date(punterDat).getMonth(), colorOffset, 100, 25, 1)



                  // dates?.some(d => new Date(d.date).getTime() === punterDat.getTime())  // La data conté comentaris
                  // ? colorDeMes(new Date(punterDat).getMonth(), colorOffset, 100, 50, 1)  // Fons del color del mes
                  // : (punterDat < new Date(dataStart)  // Abans de nàixer 
                  //     || punterDat > new Date(dataEnd)  // Després d'avui (o final del periode)
                  //   ) // Qualsevol dels dos cassos
                  //   && punterDat.getDate() !== 1 // Tots els dies menys els primers dies del mes 
                  //     ? "black" // Fons negre
                  //     : punterDat.getDate() === 1 
                  //       && colorDeMes(new Date(punterDat).getMonth(), colorOffset, 100, 50, 1)
                  //       || "white"
                ),
                // transform: mostraData == punterDat ? `scale(1.2)` : `scale(1)`,
                border: ( // Els bordes seran NEGRES per a les dates NO VISCUDES excepte aquells dies ASSENYALATS o amb TEXTS que o bé NO EN TINDRAN o s'indicaran al propi comentari. Els dies VISCUTS tindran un border BLANC. 
                  dates?.some(d => new Date(d.date).getTime() === punterDat.getTime())
                    || punterDat.getDate() === 1 
                        ? `1px solid ${colorDeMes(new Date(punterDat).getMonth(), colorOffset, 100, 50, 1)}`
                        : (punterDat < new Date(dataStart)  // Abans de nàixer 
                            || punterDat > new Date(dataEnd)  // Després d'avui (o final del periode)
                          )
                          ? punterDat.getDate() === new Date(dataStart).getDate() 
                            && punterDat.getMonth() === new Date(dataStart).getMonth() 
                            && `2px solid red` 
                            ||""//"1px solid #0005"
                          : punterDat.getDate() === new Date(dataStart).getDate() 
                            && punterDat.getMonth() === new Date(dataStart).getMonth() 
                            && `2px solid red` 
                            ||"1px solid #fff5"
                
                  // punterDat.getDate() === new Date(dataStart).getDate() 
                  // && punterDat.getMonth() === new Date(dataStart).getMonth() 
                  // && `2px solid red` 
                  // || dates?.some(d => new Date(d.date).getTime() === punterDat.getTime()) 
                  // && "2px solid black"
                ),

                boxShadow: (
                  punterDat.getDate() === new Date(dataEnd)?.getDate() && punterDat.getMonth() === new Date(dataEnd)?.getMonth() && punterDat.getFullYear() === new Date(dataEnd)?.getFullYear()
                  && "0 0 2px 4px #f00a inset, 0 0 0 2px #f00f"
                ),

                outline: (
                  dates?.some(d => new Date(d.date).getTime() === punterDat.getTime())
                  ? `1px solid ${colorDeMes(new Date(punterDat).getMonth(), colorOffset, 100, 50, 1)}`
                  : (punterDat < new Date(dataStart)  // Abans de nàixer 
                      || punterDat > new Date(dataEnd)  // Després d'avui (o final del periode)
                    )
                    ? "1px solid #fffb"
                    : "1px solid black"
                  
                ),

                zIndex: (
                  (punterDat < new Date(dataStart)  // Abans de nàixer 
                    || punterDat > new Date(dataEnd)  // Després d'avui (o final del periode)
                  )
                  ? (dates?.some(d => new Date(d.date).getTime() === punterDat.getTime()) 
                      || punterDat.getDate() === new Date(dataEnd)?.getDate() && punterDat.getMonth() === new Date(dataEnd)?.getMonth() && punterDat.getFullYear() === new Date(dataEnd)?.getFullYear() 
                    ? "100"
                    : "50"
                  )
                  : (dates?.some(d => new Date(d.date).getTime() === punterDat.getTime()) 
                    || (punterDat.getDate() === new Date(dataStart).getDate() 
                      && punterDat.getMonth() === new Date(dataStart).getMonth() 
                    ) || (punterDat.getDate() === 1)
                      || punterDat.getDate() === new Date(dataEnd)?.getDate() && punterDat.getMonth() === new Date(dataEnd)?.getMonth() && punterDat.getFullYear() === new Date(dataEnd)?.getFullYear() 
                    ? "101"
                    : "50"
                  )
                ),
                scale: dates?.some(d => new Date(d.date).getTime() === punterDat.getTime()) && "2",
                opacity: (
                  dates?.some(d => new Date(d.date).getTime() === punterDat.getTime())
                  ? "1"
                  : ".9"
                )
              }}
              title={`${punterDat.toDateString()}\nDies: ${(punterDat.getTime() - new Date(dataStart).getTime()) / (24 * 3600000)}\nAnys: ${((punterDat.getTime() - new Date(dataStart).getTime()) / (24 * 3600000)) / 365}\n${dates.filter(d => new Date(d.date).getTime() === punterDat.getTime()).map(d => d.text+"\n")}`} //Anys: ${((ara.getTime() - new Date(dataStart).getTime()) / (24 * 3600000)) / 365}`}</div>
              data-date={new Date(punterDat.setUTCHours(0,0,0,0)).toJSON()}
              onClick={ev => {
                ev.stopPropagation();
                setMostraData(new Date(ev.target.dataset["date"]).setUTCHours(0,0,0,0));
              }}
            ></div>
          };
      
          const PanellData = () => {
      
            const Data = () => {
              return <div style={{
                position: `absolute`,
                top: `0`,
                left: `0`,
                fontSize: `.8em`,
                padding: `.3em`
              }}>
                {new Date(mostraData)?.toDateString()} {/*.toLocaleFormat('%d-%b-%Y') */}
              </div>;
            };
      
            const Moment = () => {
              return <div style={{
                position: `absolute`,
                top: `0`,
                right: `0`,
                background: `rgba(0,0,0,.7)`,
                fontSize: `.7em`,
                padding: `.7em`
              }}>
                <div>{`Dies: ${(new Date(mostraData).getTime() - new Date(dataStart).getTime()) / (24 * 3600000)}`}</div>
                <div>{`Setmanes: ${((new Date(mostraData).getTime() - new Date(dataStart).getTime()) / (24 * 7 * 3600000)).toFixed(2)}`}</div>
                <div>{`Mesos: ${((new Date(mostraData).getTime() - new Date(dataStart).getTime()) / (24 * 30.5 * 3600000)).toFixed(2)}`}</div>
                <div>{`Anys: ${((new Date(mostraData).getTime() - new Date(dataStart).getTime()) / (24 * 365.25 * 3600000)).toFixed(2)}`}</div>
              </div>;
            };
      
            const Texts = () => {
              const TextsDeLaData = Radium(() => {

                //dates.filter(d => new Date(d.date).getTime() === punterDat.getTime()).map(d => d.text+"\n")

                let result = data.map(d => <div
                  key={`data_${d._id}`}
                  style={{
                    border: `1px solid grey`,
                    padding: `.3em`,
                    margin: `.3em`,
                    position: `relative`,
                    whiteSpace: `pre-wrap`
                  }}
                  title={d.createdAt.toString()}
                >{`${d.text}`}<span 
                  key={`spanClose_${d._id}`}
                  data-spanclose-id={d._id}
                  style={{
                    position: `absolute`,
                    top: `0`,
                    right: `0`,
                    opacity: `0`,
                    // background: `red`
                    ':hover': {
                      opacity: `1`,
                      cursor: `pointer`
                    }
                  }}
                  onClick={ev => {
                    ev.stopPropagation();
      
                    if (confirm("Vas a esborrar el comentari.\nAquesta acció no es pot desfer. Estàs segur?")) {
                      Meteor.call('dates.delete', {_id: ev.target.dataset.spancloseId});
                    }
                  }}
                  >&times;</span>
                </div>); 
                
                return <>{result}</>; //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
              });
      
              return <>
                <TextsDeLaData />
                <div><textarea autoFocus ref={refTaDataText} placeholder="Comenta la data" /></div>
      
                <button onClick={ev => {
                  ev.preventDefault();
                  ev.stopPropagation();
                  Meteor.call('dates.insert', {
                    date: new Date(mostraData),
                    text: refTaDataText.current.value //.replace(/\n\r?/g, '<br />')
                  },
                  (ret) => {
                   // alert("Fet :)");
                    setMostraData(null);
                  });
                }}>Estableix</button>
              </>;
            };
      
            return <>
              <div 
                className="centered" 
                style={{
                  border: `2px red ridge`,
                  background: `darkgrey`,
                  color: `white`,
                  // position: `absolute`,
                  // top: `0`,
                  // left: `0`
                  padding: `2em 1em .7em 1em`,
                  borderRadius: `.5em`,
                  //zIndex: `1000`,
                  minWidth: `35%`,
                  overflow: `hidden`
                }}
                onClick={ev => {
                  ev.stopPropagation();
                }}
              >
                <Data />
                <Moment />
                <Texts />          
              </div>
            </>;
          };
      
          if (dataStart && dataEnd) {
            return <div onClick={ev => {
              setMostraData(null);
            }}>
              <table>
                <tbody>
                  {
                    [
                      (() => {
                        let arr = [];
      
                        for (let j = 0; j <= 9; j += 1) {
                          arr = [...arr, <th className="thany" key={`thany_${j}`}>({j})</th>];
                        }
                        return <tr className="tranys">{arr}</tr>;
                      })()
                      ,
                      (() => {
                        let arri = [];
      
                        let idxDecStart = Math.floor(new Date(dataStart)?.getFullYear() / 10);
                        let idxDecEnd = Math.floor(new Date(dataEnd)?.getFullYear() / 10);
                        let puntAny;
      
                        //for (let nDecades = Math.ceil((new Date(dataEnd)?.getFullYear() - new Date(dataStart)?.getFullYear()) / 10); nDecades >= 0; nDecades -= 1) {
                        for (let puntDec = idxDecStart; puntDec <= idxDecEnd; puntDec += 1) {
                          arri = [...arri, <tr className="trdecada" key={`trdecada_${puntDec}`} data-dec={`trdecada_${puntDec}`} /*title={`dec_${puntDec}`}*/><th className="thdecada">({puntDec % 10})</th>
      
                            {(() => {
                              if (refStart.current && refEnd.current) {
                                let arr = [];
      
                                //forDAny:
                                for (let j = 0; j <= 9; j += 1) {
                                  arr = [...arr, <td className="tdany" key={`tdany_${j}`}>{
      
                                    (() => {
                                      let arrk = [];
                                      //let passatInici = false;
                                      // let any, anyData;
      
                                      // for (let k = 1; k <= 365; k += 1) {
                                      //   arrk = [...arrk, <div className="divdia" key={`${idxDecStart}.${j}.${k}`} title={k}></div>];
                                      // }
      
                                      //   if (!passatInici) {
                                      if (!puntAny) {
                                        puntAny = Math.floor(new Date(dataStart).getFullYear() / 10) * 10;
                                      } else {
                                        puntAny += 1;
                                      }
                                      // } else {
                                      //  puntAny += 1;
                                      // }
                                      let punterDat = new Date(`${puntAny}-01-01`);
                                      punterDat.setUTCHours(0,0,0,0);
      
                                      // iniciEnBlanc:
                                      while (puntAny == punterDat.getFullYear()) {
      
                                        arrk = [...arrk, <Dia key={punterDat} {...{ punterDat, dataStart, dataEnd }} />];
      
                                        punterDat = new Date(punterDat?.getTime() + (24 * 3.6E6));
                                        if (puntAny == punterDat.getFullYear()) {
                                          //   passatInici = true;
                                          //break;
                                          //continue forDAny;
                                        }
                                      }

      
                                      return arrk;
                                    })()
                                  }
                                  </td>]
                                }
                                return arr;
                              }
                            })()}     
      
                          </tr>];
                          //     idxDecStart += 1;
      
                        }
      
                           
                        return <tr>{arri}</tr>;
                      })()
                    ]
                  }
      
                </tbody>
              </table>
              <>
                {mostraData && <div style={{
                  background: `rgba(0,0,0,.5)`,
                  width: `100%`,
                  height: `100%`,
                  position: `fixed`,
                  zIndex: `1000`,
                  top: `0`,
                  left: `0`
                }}
                >
                  {mostraData && <PanellData />}
                </div>}
              </>
            </div>;
          } else { return null; }
        };
   
     function colorDeMes(nMes, hueOffset, s, l, a, monthOffset = 0) {
   
       function hue(n, d, o) {
         return ((((n % 12) * 30) + d)) + (o * 30) % 360;
       }
   
       //return `hsla(${(nMes * (30 + hueOffset) + (30 * monthOffset)) % 360}, ${s}%, ${l}%, ${a})`;
       return `hsla(${hue(nMes+8, hueOffset, monthOffset)}, ${s}%, ${l}%, ${a})`;
     }
   
     const ColorsMesos = ({ hueOffset, setHueOffset, monthOffset, setMonthOffset }) => {
   
       const [delta, setDelta] = useState(0);
       const [offset, setOffset] = useState(0);
   
       const refDelta = useRef();
       const refOffset = useRef();
       const refTxtDelta = useRef();
       const refTxtOffset = useRef();
   
       return <div>
         <input
           type="number"
           defaultValue="0"
           ref={refDelta}
           onChange={ev => setDelta(ev.target.value)}
           value={delta}
         />
         <input
           type="range"
           min="0"
           max="30"
           step="1"
           defaultValue={delta}
           onChange={ev => setDelta(ev?.target?.value)}
           value={delta}
           ref={refTxtDelta}
           title={refDelta?.current?.value}
         />
         <input
           type="number"
           defaultValue="0"
           ref={refOffset}
           onChange={ev => setOffset(ev?.target?.value)}
           value={offset}
         />
         <input
           type="range"
           min="0"
           max="11"
           step="1"
           defaultValue={offset}
           onChange={ev => setOffset(ev?.target?.value)}
           ref={refTxtOffset}
           title={refOffset?.current?.value}
           value={offset}
         />
   
         <div style={{ display: `flex` }}>
           <div style={{ display: `inline-block`, width: `40px`, height: `40px`, background: colorDeMes(0, delta, 100, 50, 1, offset), borderRadius: `50%` }}></div>
           <div style={{ display: `inline-block`, width: `40px`, height: `40px`, background: colorDeMes(1, delta, 100, 50, 1, offset), borderRadius: `50%` }}></div>
           <div style={{ display: `inline-block`, width: `40px`, height: `40px`, background: colorDeMes(2, delta, 100, 50, 1, offset), borderRadius: `50%` }}></div>
           <div style={{ display: `inline-block`, width: `40px`, height: `40px`, background: colorDeMes(3, delta, 100, 50, 1, offset), borderRadius: `50%` }}></div>
           <div style={{ display: `inline-block`, width: `40px`, height: `40px`, background: colorDeMes(4, delta, 100, 50, 1, offset), borderRadius: `50%` }}></div>
           <div style={{ display: `inline-block`, width: `40px`, height: `40px`, background: colorDeMes(5, delta, 100, 50, 1, offset), borderRadius: `50%` }}></div>
           <div style={{ display: `inline-block`, width: `40px`, height: `40px`, background: colorDeMes(6, delta, 100, 50, 1, offset), borderRadius: `50%` }}></div>
           <div style={{ display: `inline-block`, width: `40px`, height: `40px`, background: colorDeMes(7, delta, 100, 50, 1, offset), borderRadius: `50%` }}></div>
           <div style={{ display: `inline-block`, width: `40px`, height: `40px`, background: colorDeMes(8, delta, 100, 50, 1, offset), borderRadius: `50%` }}></div>
           <div style={{ display: `inline-block`, width: `40px`, height: `40px`, background: colorDeMes(9, delta, 100, 50, 1, offset), borderRadius: `50%` }}></div>
           <div style={{ display: `inline-block`, width: `40px`, height: `40px`, background: colorDeMes(10, delta, 100, 50, 1, offset), borderRadius: `50%` }}></div>
           <div style={{ display: `inline-block`, width: `40px`, height: `40px`, background: colorDeMes(11, delta, 100, 50, 1, offset), borderRadius: `50%` }}></div>
         </div>
   
         <button>Estableix els colors</button>
       </div>;
     };
   
     const DiesIAnys = () => {
       const [ara, setAra] = useState(new Date());
   
      //  setTimeout(() => {
      //    setAra(new Date());
      //    //setPD()
      //  }, 50);

       useEffect(() => {
        const stTemps = Meteor.setTimeout(() => {
          setAra(new Date());
          //setPD()
        }, 50);
        return () => Meteor.clearTimeout(stTemps);
      });
   
       return <><div>{`Dies: ${((ara.getTime() - new Date(dataStart).getTime()) / (24 * 3600000)).toFixed(6)}`}</div><div>{`Anys: ${(((ara.getTime() - new Date(dataStart).getTime()) / (24 * 3600000)) / 365).toFixed(8)}`}</div></>;
     }
   
     return <DatesContext.Provider value={{dates, data, dataStart, dataEnd}}>
        <div className="divOuterDash"
          style={{
            display: `flex`,
            position: `relative`
          }}
        >
          <div className="divDash">
            <br />
      
            <PanellsDreta>
              <PanellDretOpcions />
              <PanellDretUsuaris />
              <NotesDelDia />
            </PanellsDreta>
            
            {/* <PanellsDreta>
            </PanellsDreta> */}
      
            <DiesIAnys />
            <VidaReal />
      
            <br />
      
            <input type="date" ref={refStart} title="Inici" defaultValue={dataStart} /*onChange={ev => {setDataStart(ev.target.value);}}*/ />
            <input type="date" ref={refEnd} title="Fi" defaultValue={dataEnd} /*onChange={ev => {setDataEnd(ev.target.value);}}*/ />
      
            <button onClick={ev => {
              ev.preventDefault();
              setDataStart(refStart.current.value);
              setDataEnd(refEnd.current.value);
            }}>Llista els dies</button>
      
            <ColorsMesos />
            <br />
            <br />
      
          </div>
        </div>;
      </DatesContext.Provider>
   };
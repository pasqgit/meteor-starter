import React, { useRef, useState, useContext, useEffect, useMemo } from "react";
import{ Meteor } from 'meteor/meteor';
import { useTracker } from 'meteor/react-meteor-data';
import { AreesPersonalsCollection } from '/imports/api/areesPersonals';
import { TasquetesCollection } from '/imports/api/tasquetes';
import { AreesPersonals } from "../AreesPersonals/AreesPersonals";
import { Tasquetes } from "../Tasquetes/Tasquetes";

import { Entitats } from '/imports/ui/Entitats/Entitats.jsx';
// import Radium from 'radium'; 

// const AreesPersonalsFlexed = Radium(AreesPersonals)


export const DashboardDelDia = () => {
     
     const [fetes, setFetes] = useState([]);
     
     const Fets = () => {
          return <>
               <fieldset>
                    <legend>Fets</legend>
                    <ol>{fetes.map((f,i,a) => <li key={`feta_${i}`}>{f}</li>)}</ol>
               </fieldset>
          </>;
     };
     
     return <>
          <Entitats />

          <div style={{
               background: `#fff7`,
               borderTop: `.3em solid black`,
               // borderRadius: `.3em`
          }}>
               <AreesPersonals fetes={fetes} setFetes={setFetes} />
               <Tasquetes fetes={fetes} setFetes={setFetes} />
               <Fets />
          </div>
     </>;
};
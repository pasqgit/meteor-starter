import React, { useRef, useState, useContext, useEffect } from "react";
import { Meteor } from 'meteor/meteor';
import { useTracker } from 'meteor/react-meteor-data';
import { TasquetesCollection } from '/imports/api/tasquetes';
import Radium from 'radium';
import { AreesPersonalsCollection } from '/imports/api/areesPersonals';
// Aquesta mini aplicació permet la creació de tasques a una col·lecció de la BD.
// Són vinculades a l'usuari que les crea
// Com a mínim tindrà aquests camps:
// { titol, uid, createdAt, dates_feta }
// El camp 'dates_feta' és un arrai dels moments en que la tasca ha sigut realitzada
//

export const Tasquetes = ({ fetes, setFetes }) => {
     const [tasquetaId, setTasquetaId] = useState(null);
     const inTitolTasqueta = useRef();
     const { tasquetes, areesPersonals } = useTracker(() => {
          Meteor.subscribe('tasquetesUsuari', Meteor.userId());
          const tasquetes = TasquetesCollection.find({}).fetch();
          Meteor.subscribe('areesPersonalsUsuari', Meteor.userId());
          const areesPersonals = AreesPersonalsCollection.find({}).fetch();
          return {
               tasquetes,
               areesPersonals
          };
     });
     const [triades, setTriades] = useState([]);

     const refSelAreaPersonal = useRef();
     const refGuardaTasqueta = useRef();

     const Tasqueta = ({ tqt, i, a }) => {
          const [hovered, setHovered] = useState(false);
          const [edita, setEdita] = useState(false);
          const [esborra, setEsborra] = useState(false);
          return <div
               data-tqt_id={tqt._id}
               onMouseEnter={ev => {
                    setHovered(true);
               }}
               onMouseLeave={ev => {
                    setHovered(false);
               }}
          >
               <li key={`tqt_${i}`}
                    style={{
                         textDecoration: triades.includes(tqt.titol) ? `line-through` : `none`,
                         display: triades.includes(tqt.titol) ? `none` : `block`,
                         fontWeight: triades.includes(tqt.titol) ? `bold` : `none`
                    }}
                    data-tasqueta-index={i}
                    onClick={ev => {
                         ev.stopPropagation();

                         confirm(`Vas a triar la tasqueta "${tqt.titol}"?`)
                              && setTriades([tqt.titol, ...triades])
                    }}
               >{tqt.hasOwnProperty("area_personal") ? `(${tqt.area_personal}) ${tqt.titol}` : tqt.titol}</li>
               {
                    hovered && <><button
                         onClick={ev => {
                              ev.preventDefault();
                              setEdita(true);
                              inTitolTasqueta.current.value = tqt.titol;
                              refGuardaTasqueta.current.dataset["tqt_id"] = tqt._id;
                         }}
                    >Edita</button><button
                         onClick={ev => {
                              ev.preventDefault();
                              setEsborra(true);
                              inTitolTasqueta.current.value = tqt.titol;
                              refGuardaTasqueta.current.dataset["tqt_id"] = tqt._id;
                         }}
                    >Esborra</button></>
               }
          </div>;
     };

     return <div style={{ display: `flex` }}>
          <fieldset>
               <legend>Crea una tasqueta nova: </legend>
               <input type="text" ref={inTitolTasqueta} autoFocus />
               <select ref={refSelAreaPersonal} defaultValue="ALTRES">
                    <option value="ALTRES">ALTRES</option>
                    {
                         areesPersonals.map((ap, i, a) => <option key={`opAP_${i}`} value={ap.titol}>{ap.titol}</option>)
                    }
               </select>
               <button
                    ref={refGuardaTasqueta}
                    onClick={ev => {
                         if (tasquetes.some((t, i, a) => t.titol === inTitolTasqueta.current.value)) {
                              Meteor.call('tasquetes.update', { _id: ev.target.dataset["tqt_id"], titol: inTitolTasqueta.current.value, uid: Meteor.userId(), area_personal: refSelAreaPersonal.current.value, dates_feta: [], dates_triada: [] });
                         } else {
                              Meteor.call('tasquetes.insert', { titol: inTitolTasqueta.current.value, uid: Meteor.userId(), dates_feta: [], dates_triada: [] });
                         }
                         inTitolTasqueta.current.value = "";
                         inTitolTasqueta.current.focus();
                    }}
               >Guarda</button>
          </fieldset>
          <fieldset>
               <legend>Tasquetes:</legend>
               <button onClick={ev => {
                    let triada = tasquetes[Math.floor(Math.random() * tasquetes.length)].titol;

                    if (triades.length < tasquetes.length) {

                         if (triada && triades.includes(triada)) {
                              while (triades.includes(triada)) {
                                   triada = tasquetes[Math.floor(Math.random() * tasquetes.length)].titol;
                              }
                         }

                         setTriades([triada, ...triades]);

                    }

               }}>Extreu una tasqueta</button>
               <ul style={{
                    color: `magenta`,
                    textShadow: `0 0 .2em lime`
                    // ,
                    // "&:first-child": {
                    //      fontSize: `2em`
                    // }
               }}>
                    {
                         triades
                              ?.sort((a, b) => a - b)
                              ?.map((tqt, i, a) => <li key={`trd_${i}`}
                                   onClick={ev => {
                                        ev.stopPropagation();

                                        confirm(`Vas a donar la tasqueta "${tqt}" com a feta?`)
                                             && setFetes([tqt, ...fetes])
                                   }}
                              >{tqt}</li>)
                    }
               </ul>
               <ul style={{
                    color: `grey`
               }}>
                    {
                         tasquetes
                              ?.sort((a, b) => a.area_personal - b.area_personal)
                              .map((tqt, i, a) => {
                              //  refGuardaTasqueta.current.dataset["tqt_id"] = tqt._id;
                              return <Tasqueta key={`tasqueta_${i}`} tqt={tqt} i={i} a={a} />;
                         })
                    }
               </ul>
          </fieldset>
     </div>;
};
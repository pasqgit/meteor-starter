import React, { useRef, useState, useContext, useEffect } from "react";
import{ Meteor } from 'meteor/meteor';
import { useTracker } from 'meteor/react-meteor-data';
//import { DatesCollection } from '/imports/api/dates.js';
import { PanellsDreta, PanellDretUsuaris, PanellDretOpcions } from '/imports/ui/Panells/PanellsDreta.jsx';
import { DatesCollection } from '/imports/api/dates.js';
import Radium from 'radium'; 
import { AppContext } from "../../api/Contexts/AppContext";

export const Cicles = () => {

     const user = useTracker(() => Meteor.user());
   
     const
       [titCicle, setTitCicle] = useState(),
       [dataIni, setDataIni] = useState(),
       [dataFi, setDataFi] = useState(),
       [colorCicle, setColorCicle] = useState('hsl(0, 0%, 50%, 1)'),
       [valor, setValor] = useState(0),
       [dies, setDies] = useState(7)
     ;
   
     const [dates, dataIniBD, dataFiBD] = useTracker(() => {
          Meteor.subscribe('datesUsuari', Meteor.userId());

          const 
               dIni = new Date(dataIni),
               dFi = new Date(dataFi)
          ;

          return [DatesCollection.find().fetch(), DatesCollection.find({date: new Date(dIni.setUTCHours(0,0,0,0))}).fetch(), DatesCollection.find({date: new Date(dFi.setUTCHours(0,0,0,0))}).fetch()];
     });

     const
       refTitCicle = useRef(),
       refStart = useRef(),
       refEnd = useRef(),
       refValor = useRef(),
       refDies = useRef()
     ;
   
     // useEffect(() => {
     //  const stTemps = Meteor.setTimeout(() => {
     //    setDataEnd(`${String(new Date().getFullYear())}-${String(new Date().getMonth() + 1).padStart(2, "0")}-${String(new Date().getDate()).padStart(2, "0")}`);
     //  }, 60000);
     //  return () => Meteor.clearTimeout(stTemps);
     // })

     return <>
          <div>
               <div>{`Títol: ${titCicle}`}</div>
               <div>{`Inici: ${dataIni?.toString()} `}</div>
               <div>{`Fi: ${dataFi?.toString()} `}</div>
               <div>{`Color: ${colorCicle} `}</div>
               <div>{`Valor: ${valor} `}</div>
               <div>{`Dies: ${dies} `}</div>
          </div>

          <input type="text" 
               ref={refTitCicle} 
               placeholder="Títol del cicle" 
               onChange={ ev => {
                    setTitCicle(ev.target.value);
               }}
               style={{
                    display: `block`
               }}
          />
          <input type="date" 
               ref={refStart} 
               placeholder="Data d'inici" 
               onChange={ ev => {
                    setDataIni(new Date(ev.target.value));
               }}
               style={{
                    display: `block`
               }}
          />
          <input type="date" 
               ref={refEnd} 
               placeholder="Data de fi" 
               onChange={ ev => {
                    setDataFi(new Date(ev.target.value));
               }}
               style={{
                    display: `block`
               }}
          />
          <input type="number" 
               ref={refValor} 
               placeholder="Valor" 
               onChange={ ev => {
                    setValor(ev.target.value);
               }}
               style={{
                    display: `block`
               }}
          />
          <input type="number" 
               ref={refDies} 
               placeholder="Plaç en dies" 
               placeholder="Valor" 
               onChange={ ev => {
                    setDies(ev.target.value);
               }}
               style={{
                    display: `block`
               }}
          />

          <button onClick={ev => {
               ev.preventDefault();
               ev.stopPropagation();

               confirm("Assignar cicle a les dates d'inici i de fi del cicle.")
               //&& Meteor.call('')
               && console.log(dataIniBD)
          }}>
               Estableix
          </button>
          

     </>;
};
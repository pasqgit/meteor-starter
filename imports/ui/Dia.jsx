import React, { useState, useEffect, useRef } from 'react';
import { useTracker } from 'meteor/react-meteor-data';

import { EntradesText } from './EntradesText.jsx';
import { Comptador } from '/imports/ui/Comptadors.jsx';

import { Llistes } from './Llistes.jsx';

import { HabitsCollection } from '../api/habits.js';
import { Form } from 'react-bootstrap';

import Radium from 'radium';
// import { Button, Modal } from 'react-bootstrap';

import { format, formatRelative, subDays } from 'date-fns';
import { es, ca } from 'date-fns/locale';

import { romanize } from '/imports/funs.js';

import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';




const BarraDia = (props) => {
  const [ara, setAra] = useState(new Date());
  // const iniciDia = ara;
  // iniciDia.setHours(0).setMinutes(0).setSeconds(0).setMilliseconds(0);

  function percentatgeDelDia(t) {
    const ut0 = new Date(t.getTime());
    ut0.setHours(0);
    ut0.setMinutes(0);
    ut0.setSeconds(0);
    ut0.setMilliseconds(0);

    return ((t.valueOf() - ut0.valueOf()) * 100) / (24 * 60 * 60 * 1000);
  }

  //const [pD, setPD] = useState(percentatgeDelDia(ara));

  //useEffect(() => {
  setTimeout(() => {
    setAra(new Date());
    //setPD()
  }, 250);
  //});

  return (
    <>
      <Comptador {...props} complet={percentatgeDelDia(ara).toFixed(3)} />
    </>
  );
};

const BarraDiaInvers = (props) => {
  const [ara, setAra] = useState(new Date());
  // const iniciDia = ara;
  // iniciDia.setHours(0).setMinutes(0).setSeconds(0).setMilliseconds(0);

  function percentatgeDelDia(t) {
    const ut0 = new Date(t.getTime());
    ut0.setHours(0);
    ut0.setMinutes(0);
    ut0.setSeconds(0);
    ut0.setMilliseconds(0);

    return 100 - ((t.valueOf() - ut0.valueOf()) * 100) / (24 * 60 * 60 * 1000);
  }

  //const [pD, setPD] = useState(percentatgeDelDia(ara));

  useEffect(() => {
    const stTemps = Meteor.setTimeout(() => {
      setAra(new Date());
      //setPD()
    }, 250);
    return () => Meteor.clearTimeout(stTemps);
  });

  return (
    <>
      <Comptador {...props} complet={percentatgeDelDia(ara).toFixed(2)} />
    </>
  );
};

const AutoComptador = ({ width, height, color }) => {
  const [complet, setComplet] = useState(0);

  setTimeout(() => {
    setComplet(complet < 100 ? complet + 5 : complet);
  }, 1000);

  return (
    <div>
      <Comptador
        width={width}
        height={height}
        color={color}
        complet={complet}
      />
    </div>
  );
};

const ComptadorClicable = ({ width, height, color }) => {
  const [complet, setComplet] = useState(0);

  return (
    <div
      onClick={(ev) => {
        setComplet(complet < 100 ? complet + 5 : complet);
      }}
    >
      <Comptador
        width={width}
        height={height}
        color={color}
        complet={complet}
      />
    </div>
  );
};

const ZonaHàbits = () => {
    // A la zona d'Hàbits se necessiten: 
    //  >> Una llista de tots els hàbits definits per l'usuari on es puga configurar la selecció d'aquells que apareixen per defecte 
    //

  const [mostraFormNouHabit, setMostraFormNouHabit] = useState(false);


  const ConfigHàbits = () => {
    const { subs_habitsUsuari, habits, user } = useTracker(() => {
    const subs_habitsUsuari = Meteor.subscribe(
        "habitsUsuari",
        Meteor.userId()
    );
    const habits = HabitsCollection.find().fetch();
    const user = Meteor.user();

    return {
        subs_habitsUsuari,
        habits,
        user
    };
    });

    const [mostra, setMostra] = useState(false);

    const HàbitSelector = ({ h }) => {

        return <div 
            style={{
                padding: `5px`,
                border: `1px solid navy`,
                background: `white`,
                fontSize: `.5em`
            }}
            title={h.descrip}
        >{h.titol}</div>;
    };

    return <DragDropContext>
                <fieldset>
                    <legend>Hàbits ACTIUS</legend>
                    <ul >
                    </ul>
                </fieldset>

            <fieldset>
                <legend>Hàbits INACTIUS</legend>
            </fieldset>
        <Droppable droppableId="habitsActius">
            {(provided) => (
                <ul
                    style={{
                        display: `flex`,
                        listStyle: `none`,
                        paddingLeft: `0`,
                        flexWrap: `wrap`,
                        justifyContent: `space-evenly`,
                    }}
                    className="habitsActius" {...provided.droppableProps} ref={provided.innerRef}
                >
                    {habits.map((h, index) => (
                        <Draggable key={`habit_${index}`} draggableId={`habit_${index}`} index={index}>
                            {(provided) => (
                                <li ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}
                                    style={{
                                        display: `inline-block`,
                                        listStyle: `none`
                                    }}
                                    >
                                    <HàbitSelector h={h} />
                                </li>
                            )}
                        </Draggable>
                    ))}
                    {provided.placeholder}
                </ul>
            )}
        </Droppable>
    </DragDropContext>;
  };


  const LlistaHàbits = () => {
    const { subs_habitsUsuari, habits, user } = useTracker(() => {
      const subs_habitsUsuari = Meteor.subscribe(
        "habitsUsuari",
        Meteor.userId()
      );
      const habits = HabitsCollection.find().fetch();
      const user = Meteor.user();

      return {
        subs_habitsUsuari,
        habits,
        user
      };
    });
  
    const Hàbit = ({ h, ambit }) => {
      const [compte, setCompte] = useState(0);
      const [updateHabit, setUpdateHabit] = useState(false);

      useEffect(() => {
        const sto = Meteor.setTimeout(() => {
          updateHabit &&
            Meteor.call("habits.update", {
              ...h,
              reps: [
                ...h.reps,
                {
                  quantitat: compte,
                  createdAt: new Date(),
                },
              ],
            });
        }, 10000);
        return () => Meteor.clearTimeout(sto);
      });

      const SegmentReps = Radium(({ rep }) => {
        //const [repsObj, setRepsObj] = useState({...rep, text: ""});
        const [mostraFormEditaSegment, setMostraFormEditaSegment] = useState(
          false
        );
        const FormEditaSegment = () => {
          const refTaText = useRef();
          // const [visible, setVisible] = useState(false);

          return (
            mostraFormEditaSegment && (
              <div
                style={{ border: `solid 1px orange` }}
                onClick={(ev) => {
                  ev.stopPropagation();
                }}
              >
                <button
                  onClick={(ev) => {
                    ev.preventDefault();
                    ev.stopPropagation();
                    if (
                      confirm(
                        "Vas a eliminar el segment de repeticions. Estàs segur?"
                      )
                    ) {
                      Meteor.call("habits.update", {
                        ...h,
                        reps: h.reps.filter(
                          (r, i) => r.createdAt != rep.createdAt
                        ),
                      });
                    }
                  }}
                >
                  Elimina Segment
                </button>
                <textarea ref={refTaText} />
                <button
                  onClick={(ev) => {
                    ev.preventDefault();
                    ev.stopPropagation();
                    Meteor.call("habits.update", {
                      ...h,
                      reps: [
                        ...h.reps.filter(
                          (r, i) => r.createdAt != rep.createdAt
                        ),
                        {
                          ...rep,
                          text: refTaText.current.value,
                          editedAt: new Date(),
                        },
                      ],
                    });
                  }}
                >
                  Estableix
                </button>
                <button
                  onClick={(ev) => {
                    ev.preventDefault();
                    ev.stopPropagation();
                    setMostraFormEditaSegment(false);
                  }}
                >
                  &times;
                </button>
              </div>
            )
          );
        };

        return (
          <>
            <span
              style={{
                border: `2px magenta solid`,
                padding: `0 1px`,
                margin: `1px`,
                background:
                  rep.quantitat == 2
                    ? `#ff0b`
                    : rep.quantitat == 3
                    ? `orange`
                    : rep.quantitat == 4
                    ? `#f00d`
                    : `#fffb`,
                borderRadius: `5px`,
                ":hover": {
                  background: `#ffff`,
                },
              }}
              title={`${rep.createdAt.getHours()}:${String(
                rep.createdAt.getMinutes()
              ).padStart(2, "0")}\n${rep.text || ""}`}
              onContextMenu={(ev) => {
                ev.preventDefault();
                setMostraFormEditaSegment(!mostraFormEditaSegment);
              }}
            >
              {romanize(rep.quantitat)}
            </span>
            <FormEditaSegment />
          </>
        );
      });

      // const blinkAni = Radium.keyframes({
      //     to: {
      //     visibility: 'hidden'
      //     }
      // });

      
        return  <>
            <fieldset
                onClick={(ev) => {
                  setCompte(compte + 1);
                  setUpdateHabit(true);
                }}
            >
                <legend>{h.titol}</legend>
                {h.reps
                ?.filter((rep, irep) => {
                    if (
                    rep.createdAt.getDate() === new Date().getDate() &&
                    rep.createdAt.getFullYear() === new Date().getFullYear() &&
                    rep.createdAt.getMonth() === new Date().getMonth()
                    )
                    return rep;
                })
                .sort((a, b) => a.createdAt > b.createdAt)
                .map((rep, irep) => (
                    <SegmentReps rep={rep} key={irep} />
                ))}
                <br />
                <span
                style={
                    {
                    // animation: `blink-animation 1s steps(5, start)infinite`,
                    // animationName: blinkAni
                    }
                }
                >
                {compte > 0 && compte * 10}
                </span>
                {/* {compte * 10} */}
            </fieldset>
        </>;
         
    };

    return (
      <ul
        style={{
          display: `flex`,
          listStyle: `none`,
          paddingLeft: `0`,
          flexWrap: `wrap`,
          justifyContent: `space-evenly`,
        }}
      >
        {habits.map((h, i) => (
          <li
            key={`habit_${i}`}
            style={{
              display: `inline-block`,
              listStyle: `none`,
            }}
          >
            <Hàbit h={h} ambit="config" />
          </li>
        ))}
      </ul>
    );
  };

  // const ModalCrearHabit = () => {
  //   const [show, setShow] = useState(false);

  //   const handleClose = () => setShow(false);
  //   const handleShow = () => setShow(true);

  //   return (
  //     <>
  //       <Button variant="primary" onClick={handleShow}>
  //         Launch demo modal
  //       </Button>

  //       <Modal show={show} onHide={handleClose}>
  //         <Modal.Header closeButton>
  //           <Modal.Title>Modal heading</Modal.Title>
  //         </Modal.Header>
  //         <Modal.Body>
  //           <div style={{background: `red`}}>
  //             You're reading this text in a modal!
  //           </div>
  //         </Modal.Body>
  //         <Modal.Footer>
  //           <Button variant="secondary" onClick={handleClose}>
  //             Close
  //           </Button>
  //           <Button variant="primary" onClick={handleClose}>
  //             Save Changes
  //           </Button>
  //         </Modal.Footer>
  //       </Modal>
  //     </>
  //   );
  // };

  const FormNouHabit = () => {
    const refTitol = useRef();
    const refDescrip = useRef();

    return (
      <div>
        <input type="text" ref={refTitol} placeholder="Titol de l'hàbit..." />
        <input
          type="text"
          ref={refDescrip}
          placeholder="Descripció opcional..."
        />
        <button
          onClick={(ev) => {
            ev.stopPropagation();
            Meteor.call("habits.insert", {
              titol: refTitol.current.value,
              descrip: refDescrip.current.value,
              reps: [],
            });
            refTitol.current.value = "";
            refDescrip.current.value = "";
            setMostraFormNouHabit(false);
          }}
        >
          Estableix
        </button>
      </div>
    );
  };

  return <>
    <div style={{
        background: `#fff5`,
        borderRadius: `.3em`
    }}>
      {/* <ul style={{
    paddingLeft: `0`
    }}> */}

    

      <ConfigHàbits />
      <LlistaHàbits />
      {/* <ModalCrearHabit /> */}
      <button
        onClick={(ev) => {
          ev.stopPropagation();
          setMostraFormNouHabit(!mostraFormNouHabit);
        }}
      >
        +
      </button>
      {/* </ul> */}
      {mostraFormNouHabit && <FormNouHabit />}
    </div>
  </>;
};

const Dia = () => {
  const [avui, setAvui] = useState(new Date());

  useEffect(() => {
    const toAvui = Meteor.setTimeout(() => {
      setAvui(new Date());
    }, 60000);
    return () => Meteor.clearTimeout(toAvui);
  }, [avui]);

  let dataObj = {
    data: `${avui.getDate()}/${romanize(
      avui.getMonth() + 1
    )}/${avui.getFullYear()}`,
    diaSetmana: format(avui, "eeee", { locale: ca }),
    // ,
    // minut: avui
  };

  let colorDia;

  switch (dataObj.diaSetmana) {
    case "dilluns": {
      colorDia = "#c45041";
      break;
    }
    case "dimarts": {
      colorDia = "#c4833c";
      break;
    }
    case "dimecres": {
      colorDia = "#45ad93";
      break;
    }
    case "dijous": {
      colorDia = "#39acc1";
      break;
    }
    case "divendres": {
      colorDia = "#2396c5";
      break;
    }
    case "dissabte": {
      colorDia = "#a282b0";
      break;
    }
    case "diumenge": {
      colorDia = "#312d1c";
      break;
    }
  }

  // switch (dataObj.minut % 10) {
  //   case 0: {
  //     colorDia = '#c45041';
  //     break;
  //   }
  //   case 1: {
  //     colorDia = '#c4833c';
  //     break;
  //   }
  //   case 2: {
  //     colorDia = '#45ad93';
  //     break;
  //   }
  //   case 3: {
  //     colorDia = '#39acc1';
  //     break;
  //   }
  //   case 4: {
  //     colorDia = '#2396c5';
  //     break;
  //   }
  //   case 5: {
  //     colorDia = '#a282b0';
  //     break;
  //   }
  //   case 6: {
  //     colorDia = '#312d1c';
  //     break;
  //   }
  //   case 7: {
  //     colorDia = '#000000';
  //     break;
  //   }
  //   case 8: {
  //     colorDia = '#ffff00';
  //     break;
  //   }
  //   case 9: {
  //     colorDia = '#aa00aa';
  //     break;
  //   }
  // }

  // Component d'ESTAT ACTUAL:
  //  > Últimes accions realitzades
  //  > Nivell d'acció: ALT, REGULAR, POBRE, INEXISTENT >>> Codificar amb colors

  // Gestió d'Hàbits:
  //  > Creació, selecció, desselecció i preselecció
  //  > Edició

  // Gestió de SegmentReps:
  //  > Ajust de quantitat (edició)
  //  >

  return (
    <fieldset
      style={{
        border: `1px solid navy`,
        borderRadius: `1em`,
        // display: `inline-block`,
        background: colorDia,
        padding: `1em`,
      }}
      // onClick={ev => {
      //   ev.stopPropagation();
      //   setLlistaSeleccionada(null);
      // }}
    >
      <legend
        style={{
          border: `solid 1px navy`,
          display: `inline-block`,
          borderRadius: `.4em`,
          padding: `0 3px`,
          marginBottom: `0`,
          background: `white`,
        }}
      >{`${dataObj.diaSetmana.replace(/\b\w/g, (c) => c.toUpperCase())}, ${
        dataObj.data
      }`}</legend>
      <div
        style={{
          position: `absolute`,
          textAlign: `center`,
          fontSize: `3em`,
          zIndex: `10`,
          margin: `0 auto`,
          width: `100%`,
          top: `.5em`,
          left: "0",
          // color: `white`,
          textShadow: `0 0 .1em black`,
        }}
      >{`${avui.getHours()}:${String(avui.getMinutes()).padStart(
        2,
        "0"
      )}`}</div>
      <BarraDiaInvers width="70%" height="3em" color={`${colorDia}77`} />

      <ZonaHàbits />
    </fieldset>
  );
};

export { Dia };
import React, { useContext, useState } from 'react';
import { useTracker } from 'meteor/react-meteor-data';
import { AppContext } from '../../api/Contexts/AppContext';

export const PanellsDreta = ({children}) => {
     const [isOpen, setIsOpen] = useState(false);
     
     return <div 
          style={{
               position: `fixed`,
               display: `flex`,
               right: `.5em`,
               top: `7em`,
               padding: `.5em .7em`,
               background: `rgba(139,208,221,1)`,
               borderRadius: `.4em`,
               border: `2px ridge grey`,
               margin: `1em`,
               outline: `3px solid yellow`,
               flexDirection: `column`,
               zIndex: `5000`
          }}
     ><div onClick={ev => {
          // ev.preventDefault();
          // ev.stopPropagation();
          setIsOpen(!isOpen);
     }}>
          <span 
               className="fas fa-bars"
               style={{
                    cursor: `pointer`
               }}
          />
     </div>{
          isOpen ? <>
               {children}
          </> : null
     }</div>;
};
 
export const PanellDretOpcions = () => {

     const { mostraData, setMostraData } = useContext(AppContext);

     return <>
          <div style={{
               display: `flex`,
               justifyContent: `space-around`,
               flexDirection: `column`,
               alignContent: `space-around`
          }}>
               <div style={{
                    display: `flex`,
                    justifyContent: `space-around`
               }}>
                    <button>Vista Completa</button>
               </div>
               <div style={{
                    display: `flex`,
                    justifyContent: `space-around`
               }}>
                    <button>-1</button>
                    <button onClick={ev => {
                    ev.stopPropagation();
                    ev.preventDefault();

                    
                    }}>Any Actual</button>
                    <button>+1</button>
               </div>
               <div style={{
                    display: `flex`,
                    justifyContent: `space-around`
               }}>
                    <button onClick={ev => {
                         ev.stopPropagation();
                         ev.preventDefault();
                         let novaData = new Date();

                         // setMostraData(novaData.setUTCHours(0,0,0,0));
                         setMostraData(novaData.setUTCHours(0,0,0,0));
                    }}>Crear nota d'avui</button>
               </div>
          </div>
     </>;
};

export const PanellDretUsuaris = () => {

     const usuaris = useTracker(() => {
          Meteor.subscribe('usuaris');
          return Meteor.users.find().fetch().filter(u => u._id !== Meteor.userId());
     });

     return <>
          <div style={{
               right: `.5em`,
               top: `15em`,
               padding: `.5em .7em`,
               background: `rgba(139,208,221,1)`,
               borderRadius: `.4em`,
               border: `2px ridge grey`,
               margin: `1em`,
               width: `8em`,
               display: `flex`,
               flexWrap: `wrap`,
               justifyContent: `space-around`
          }}>{
          usuaris.map(u => <div style={{
               display: `inline-block`,
               borderRadius: `50%`,
               width: `1em`,
               height: `1em`,
               border: `1px solid #fffa`,
               background: (() => {
               var letters = '0123456789ABCDEF';
               var color = '#';
               for (var i = 0; i < 6; i++) {
               color += letters[Math.floor(Math.random() * 16)];
               }
               return color;
               })(),
               margin: `.2em`
          }} title={u.username}
          key={u._id}></div>)
          }</div>
     </>;
};
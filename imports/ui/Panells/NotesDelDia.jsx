import React, { useRef, useState, useContext, useEffect } from "react";
import{ Meteor } from 'meteor/meteor';
import { useTracker } from 'meteor/react-meteor-data';
//import { DatesCollection } from '/imports/api/dates.js';
import { PanellsDreta, PanellDretUsuaris, PanellDretOpcions } from '/imports/ui/Panells/PanellsDreta.jsx';
import { DatesCollection } from '/imports/api/dates.js';
import Radium from 'radium'; 
import { AppContext } from "../../api/Contexts/AppContext";
import { DatesContext } from "../../api/Contexts/DatesContext";

export const NotesDelDia = () => {
     const { mostraData, setMostraData } = useContext(AppContext);
     const { dates, data, dataStart, dataEnd } = useContext(DatesContext);

     

     console.log("mD: ", mostraData);
     console.log("D: ", data);
     console.log("dE: ", dataEnd);
     
     return <>
          <div style={{
               right: `.5em`,
               top: `15em`,
               padding: `.5em .7em`,
               background: `rgba(139,208,221,1)`,
               borderRadius: `.4em`,
               border: `2px ridge grey`,
               margin: `1em`,
               width: `8em`,
               display: `flex`,
               flexWrap: `wrap`,
               justifyContent: `space-around`,
               
               display: `flex`,
               justifyContent: `space-around`,
               flexDirection: `column`,
               alignContent: `space-around`
          }}>
               { data.map(d => <div>{d.text}</div>)
          }</div>
     </>;
};
import React, { useState, useEffect, useRef } from 'react';
import { useTracker } from 'meteor/react-meteor-data';
import { LaGerraCollection } from '../api/laGerra.js';


export function getMsSinceMidnight(d) {
     var e = new Date(d);
     return d - e.setHours(0, 0, 0, 0);
   }
   
export const LaGerra = () => {
   
     const {
       subs_laGerra,
       laGerra
     } = useTracker(() => {
       const subs_laGerra = Meteor.subscribe('laGerra', Meteor.userId());
       const laGerra = TimeSessionsCollection.find().fetch();
   
       return {
         subs_laGerra,
         laGerra
       };
     });
   
     const [ara, setAra] = useState(new Date());
     // const [acts, setActs] = useState([]);
     const [act, setAct] = useState(null);
   
     const [dia, setDia] = useState({
       init: null,
       totalSpent: 0,
       acts: []
     });
   
     function msToday(t) {
       const horaZero = new Date().setHours(0, 0, 0, 0);
       return t - horaZero;
     }
   
     // if (subs_laGerra.ready()) {
     let sessio = (ara - laGerra[laGerra?.length - 1]?.acts[laGerra[laGerra?.length - 1]?.acts?.length - 1] > msToday(ara)) ? {
       init: new Date().setHours(0, 0, 0, 0),
       acts: [],
       user: Meteor.userId()
     } : laGerra[laGerra?.length - 1];
     // } else {
     // const [sessio, setSessio] = useState({
     //   init: null,
     //   totalSpent: 0,
     //   stop: null,
     //   acts: [],
     //   user: Meteor.userId()
     // });
     // }
   
     const [configActs, setConfigActs] = useState([
       {
         nomAct: "Dormir",
         horesNominals: 8,
         msRemain: 8 * 60 * 60 * 1000,
         msDue: null,
         init: null,
         maxDue: null,
         doneMsSum: 0,
         color: `black`
       },
       {
         nomAct: "Prod",
         horesNominals: 8,
         msRemain: 8 * 60 * 60 * 1000,
         msDue: null,
         init: null,
         maxDue: null,
         doneMsSum: 0,
         color: `blue`
       },
       {
         nomAct: "Neces",
         horesNominals: 2,
         msRemain: 2 * 60 * 60 * 1000,
         msDue: null,
         init: null,
         maxDue: null,
         doneMsSum: 0,
         color: `red`
       },
       {
         nomAct: "Altres",
         horesNominals: 6,
         msRemain: 6 * 60 * 60 * 1000,
         msDue: null,
         init: null,
         maxDue: null,
         doneMsSum: 0,
         color: `magenta`
       }
     ]);
   
     const refPunter = useRef();
   
     useEffect(() => {
       const stTemps = Meteor.setTimeout(() => {
         setAra(new Date());
       }, 1000);
       return () => Meteor.clearTimeout(stTemps);
     });
   
     useEffect(() => {
       const stTemps2 = Meteor.setTimeout(() => {
         const horaZero = new Date().setHours(0, 0, 0, 0);
   
         if (sessio.init && !dia.init) {
           setDia({ ...dia, init: new Date() });
         }
         //setAra(new Date());
         if (ara.getHours() == 0 && ara.getMinutes() == 0) {
           setDia({
             ...dia,
             date: horaZero,
             used: (act && acts.reduce((acc, a, i) => acc + (ara - a.start))),
             budget,
             acts: sessio.acts.map((sa, ind) => sa.start < ara && sa.start.setHours(0, 0, 0, 0))
           })
         }
         //setPD()
       }, 59000);
       return () => Meteor.clearTimeout(stTemps2);
     });
   
   
     function afegeixAct(act) {
       const instant = new Date();
       //let sessio;
       //let nouActs = [...acts];
   
       setAct(act);
   
       //nouActs[nouActs.length]?.end = instant;
   
       //setActs([...nouActs, act])
       if (!dia.init) {
         setDia({
           ...dia,
           init: instant
         })
       }
   
       sessio = ((sessioAnt) => {
         let nouSessActs = [...sessioAnt.acts.slice(0, sessioAnt.acts.length - 1)];
   
         if (sessioAnt.acts.length) {
           nouSessActs = [
             ...nouSessActs,
             {
               ...sessioAnt.acts[sessioAnt.acts.length - 1],
               stop: instant
             }
           ];
         }
   
         return {
           ...sessioAnt,
           init: sessioAnt.init ? sessioAnt.init : instant,
           acts: [
             ...nouSessActs,
             act
           ]
         };
       })(sessio || {
         init: null,
         totalSpent: 0,
         end: null,
         acts: [],
         user: Meteor.userId()
       });
   
       Meteor.call('laGerra.upsert', sessio);
   
       // setActs([...acts, act]);
       // acts[acts.length].end = instant;
     }
   
     function msSessioInit(sessio) {
          const 
               init = sessio?.init,
               hZeroData = new Date(init).setHours(0,0,0,0)
          ;

          return init - hZeroData;
     }
   
     const Segments = ({sessio}) => {
       const refSegments = useRef();
   
       return <><div
         style={{
           position: `relative`,
           //left: `calc(${getMsSinceMidnight(new Date()) - getMsSinceMidnight(sessio?.acts[0]?.start) / (24*60*60*1000) * 100}% - 1px)px)`
           left: `calc(${getMsSinceMidnight(new Date()) / (24 * 60 * 60 * 1000) * 100}% - 1px)`,
           whiteSpace: `nowrap`
         }}
       >
         {sessio?.acts?.map((activ, idx) => <><div
           ref={refSegments}
           key={`act_${idx}`}
           style={{
             background: activ.color,
             display: `inline-block`,
             minWidth: `1px`,
             width: `${!activ.stop ? ((ara - activ.start) * refPunter.current.clientWidth / (24 * 60 * 60 * 1000)) : ((activ.stop - activ.start) * refPunter.current.clientWidth / (24 * 60 * 60 * 1000))}px`,
             height: `2em`,
             left: `-${((ara - sessio?.acts[0].start) * refPunter.current.clientWidth / (24 * 60 * 60 * 1000)) || "auto"}px`,
             position: `relative`,
             whiteSpace: `nowrap`
           }}
           title={`${activ.nomAct} fa ${new Date() - activ.start} ms ${activ?.stop}`}
         ></div>
         </>)}
          <div style={{
               position: `absolute`,
               background: `cyan`, 
               width: `1px`, 
               display: `inline-block`, 
               height: `2em`,
               lineHeight: `2em`,
               left: `calc(${((new Date(sessio?.init) - new Date(sessio?.init).setHours(0,0,0,0)) / (24 * 60 * 60 * 1000)) * 100}% - ${getMsSinceMidnight(ara) / (24 * 60 * 60 * 1000) * 100}%)`
               }}
          >init</div>
          <div style={{
               position: `absolute`,
               background: `lime`, 
               width: `1px`, 
               display: `inline-block`, 
               height: `2em`,
               lineHeight: `2em`,
               //left: `calc(${((new Date(sessio?.init) - new Date(sessio?.init).setHours(0,0,0,0)) / (24 * 60 * 60 * 1000)) * 100}% - ${getMsSinceMidnight(ara) / (24 * 60 * 60 * 1000) * 100}%)`
               //left: `calc(${(((new Date(new Date(sessio?.init).getTime() + (24*3600000))) - sessio?.init) / (24*3600000)) * 100}%)` // FLIPES AMB LA TONTÀ! QUE NO ESTAVA CLAR QUE HAVIA DE SER 100%??????
               left: `calc(${((new Date(sessio?.init) - new Date(sessio?.init).setHours(0,0,0,0)) / (24 * 60 * 60 * 1000)) * 100}% - ${getMsSinceMidnight(ara) / (24 * 60 * 60 * 1000) * 100}% + 100%)`
               }}
          >24h</div>
          <div style={{
               position: `absolute`,
               background: `lime`, 
               width: `1px`, 
               display: `inline-block`, 
               height: `2em`,
               lineHeight: `2em`,
               //left: `calc(${((new Date(sessio?.init) - new Date(sessio?.init).setHours(0,0,0,0)) / (24 * 60 * 60 * 1000)) * 100}% - ${getMsSinceMidnight(ara) / (24 * 60 * 60 * 1000) * 100}%)`
               //left: `calc(${(((new Date(new Date(sessio?.init).getTime() + (24*3600000))) - sessio?.init) / (24*3600000)) * 100}%)` // FLIPES AMB LA TONTÀ! QUE NO ESTAVA CLAR QUE HAVIA DE SER 100%??????
               left: `calc(${((new Date(sessio?.init) - new Date(sessio?.init).setHours(0,0,0,0)) / (24 * 60 * 60 * 1000)) * 100}% - ${getMsSinceMidnight(ara) / (24 * 60 * 60 * 1000) * 100}% + 200%)`
               }}
          >48h</div>
          <div style={{
               position: `absolute`,
               background: `lime`, 
               width: `1px`, 
               display: `inline-block`, 
               height: `2em`,
               lineHeight: `2em`,
               //left: `calc(${((new Date(sessio?.init) - new Date(sessio?.init).setHours(0,0,0,0)) / (24 * 60 * 60 * 1000)) * 100}% - ${getMsSinceMidnight(ara) / (24 * 60 * 60 * 1000) * 100}%)`
               //left: `calc(${(((new Date(new Date(sessio?.init).getTime() + (24*3600000))) - sessio?.init) / (24*3600000)) * 100}%)` // FLIPES AMB LA TONTÀ! QUE NO ESTAVA CLAR QUE HAVIA DE SER 100%??????
               left: `calc(${((new Date(sessio?.init) - new Date(sessio?.init).setHours(0,0,0,0)) / (24 * 60 * 60 * 1000)) * 100}% - ${getMsSinceMidnight(ara) / (24 * 60 * 60 * 1000) * 100}% + 300%)`
               }}
          >72h</div>
       </div>
       </>;
     };
   
     function msToTime(ms, clock) {
       const
         h = Math.floor(ms / (60 * 60 * 1000)).toString().padStart(2, '0'),
         m = Math.floor((ms / 1000) % 3600 / 60).toString().padStart(2, '0'),
         s = Math.floor((ms / 1000) % 60).toString().padStart(2, '0');
   
       if (clock) {
         return `${h}:${m}`;
       } else {
         return `${h}h${m}'${s}"`;
       }
     }
   
     const Estat = () => {
   
       function msAct(nom) {
         return `${sessio?.acts?.reduce((acc, a, i) => {
           if (a.nomAct == nom) {
             if (a.hasOwnProperty("stop")) {
               return acc + (a.stop - a.start);
             } else {
               return acc + (ara - a.start);
             }
           } else {
             return acc;
           }
         }, 0)}`
       }
   
       return <div
         style={{
           border: `3px ridge hsl(0,100%,25%)`,
           margin: `2em auto 0`,
           width: `20em`,
           minHeight: `6em`
         }}
       >
         <div style={{ color: act?.color }}>Activitat actual: {act?.nomAct}</div>
         <hr />
         <div style={{display: `flex`}}>
           <div style={{
               color: `${configActs.find((v, i) => v.nomAct == "Dormir")?.color}`
             }}>{`Dormir: ${msToTime(msAct("Dormir"))} (${msToTime(configActs.find((v, i) => v.nomAct == "Dormir")?.horesNominals * 3600000 - msAct("Dormir"))})`}</div>
           <div style={{width: `10em`, height: `1em`, border: `1px solid black`, display: `flex`}}>
             <div style={{
               width: `${100 - ((msAct("Dormir") / (configActs.find((v, i) => v.nomAct == "Dormir")?.horesNominals * 3600000)) * 100)}%`,
               background: `${configActs.find((v, i) => v.nomAct == "Dormir")?.color}`
             }}></div>
             <div></div>
           </div> 
         </div>
         <div style={{display: `flex`}}>
           <div style={{
               color: `${configActs.find((v, i) => v.nomAct == "Prod")?.color}`
             }}>{`Prod: ${msToTime(msAct("Prod"))} (${msToTime(configActs.find((v, i) => v.nomAct == "Prod")?.horesNominals * 3600000 - msAct("Prod"))})`}</div>
           <div style={{width: `10em`, height: `1em`, border: `1px solid black`, display: `flex`}}>
             <div style={{
               width: `${100 - ((msAct("Prod") / (configActs.find((v, i) => v.nomAct == "Prod")?.horesNominals * 3600000)) * 100)}%`,
               background: `${configActs.find((v, i) => v.nomAct == "Prod")?.color}`
             }}></div>
             <div></div>
           </div> 
         </div>
         <div style={{display: `flex`}}>
           <div style={{
               color: `${configActs.find((v, i) => v.nomAct == "Neces")?.color}`
             }}>{`Neces: ${msToTime(msAct("Neces"))} (${msToTime(configActs.find((v, i) => v.nomAct == "Neces")?.horesNominals * 3600000 - msAct("Neces"))})`}</div>
           <div style={{width: `10em`, height: `1em`, border: `1px solid black`, display: `flex`}}>
             <div style={{
               width: `${100 - ((msAct("Neces") / (configActs.find((v, i) => v.nomAct == "Neces")?.horesNominals * 3600000)) * 100)}%`,
               background: `${configActs.find((v, i) => v.nomAct == "Neces")?.color}`
             }}></div>
             <div></div>
           </div> 
         </div>
         <div style={{display: `flex`}}>
           <div style={{
               color: `${configActs.find((v, i) => v.nomAct == "Altres")?.color}`
             }}>{`Altres: ${msToTime(msAct("Altres"))} (${msToTime(configActs.find((v, i) => v.nomAct == "Altres")?.horesNominals * 3600000 - msAct("Altres"))})`}</div>
           <div style={{width: `10em`, height: `1em`, border: `1px solid black`, display: `flex`}}>
             <div style={{
               width: `${100 - ((msAct("Altres") / (configActs.find((v, i) => v.nomAct == "Altres")?.horesNominals * 3600000)) * 100)}%`,
               background: `${configActs.find((v, i) => v.nomAct == "Altres")?.color}`
             }}></div>
             <div></div>
           </div> 
         </div>
         <hr />
         <div>Inici de sessió: {sessio?.init?.toString()}</div>
         <div>Sessió: {`${msToTime(ara - sessio?.acts[0]?.start)} (${msToTime(24 * 3600000 - (ara - sessio?.acts[0]?.start))})`}</div>
       </div>;
     };
   
     const Tren = () => {
       return <div
         style={{
           border: `3px ridge hsl(0,100%,25%)`,
           margin: `2em auto 0`,
           width: `20em`,
           minHeight: `6em`
         }}
       >
         {sessio?.acts?.map((a, i) => <div key={`div_${i}`} style={{ display: `inline-block`, margin: `0 1em`, color: a.color }}>
           {a.nomAct}
         </div>)}
       </div>;
     };

     const SessionsAnteriors = () => {
          return laGerra.sort((a, b) => b.init - a.init).map((ts, i) => {
               return <div style={{
                         background: `#bbb`,
                         width: `90%`,
                         height: `2em`,
                         border: `1px solid black`,
                         margin: `2em auto`,
                         position: `relative`,
                         overflow: `hidden`,
                         display: `block`
                    }}>
                    <Segments sessio={ts} />
               </div>;
          });
     };
   
   
     return <>
       <div
         style={{
           display: `inline-block`,
           position: `absolute`,
           textAlign: `center`,
           fontSize: `3em`,
           zIndex: `10`,
           margin: `0 auto`,
           //width: `100%`,
           // margin-left: auto;
           // margin-right: auto;
           left: `0`,
           right: `0`,
           top: `.5em`,
           // left: "0",
           // color: `white`,
           textShadow: `0 0 .1em black`,
           background: `hsla(0, 100%, 100%, .87)`,
           width: `max-content`,
           padding: `.1em .5em`,
           borderRadius: `.5em`
         }}
       >{(() => {
         let dema = new Date(ara);
         dema.setDate(dema.getDate() + 1);
         let msPerDema = -1 * (ara - new Date(dema.setHours(0, 0, 0, 0)));
   
         return `${ara.getHours()}:${String(ara.getMinutes()).padStart(2, "0")} (${msToTime(msPerDema, true)})`;
       })()}
       </div>
       <div style={{
         display: `flex`
       }}>
         <Estat />
         <Tren />
       </div>
       <div
         ref={refPunter}
         style={{
           background: `#bbb`,
           width: `90%`,
           height: `2em`,
           border: `1px solid black`,
           margin: `2em auto`,
           position: `relative`,
           overflow: `hidden`
         }}
       >
         {/* <div 
           style={{
             width: `1px`,
             height: `1em`,
             left: `calc(${getMsSinceMidnight(new Date()) / (24*60*60*1000) * 100}% - 1px)`,
             background: `black`,
             top: `-1em`,
             position: `relative`
           }}
         >
         </div> */}
         <Segments sessio={sessio} />
       </div>
       
       <div style={{
         display:`flex`,
         justifyContent: `space-around`
       }}>
         <button
           style={{
             background: "blue",
             flexGrow: `1`,
             padding: `2em`
           }}
           onClick={e => {
             afegeixAct({
               nomAct: "Prod",
               start: new Date(),
               overDue: false,
               color: "blue"
             });
           }}
         >Prod</button>
         <button
           style={{
             background: "red",
             flexGrow: `1`,
             padding: `2em`          
           }}
           onClick={e => {
             afegeixAct({
               nomAct: "Neces",
               start: new Date(),
               overDue: false,
               color: "red"
             });
           }}
         >Neces</button>
         <button
           style={{
             background: "magenta",
             flexGrow: `1`,
             padding: `2em`          
           }}
           onClick={e => {
             afegeixAct({
               nomAct: "Altres",
               start: new Date(),
               overDue: false,
               color: "magenta"
             });
           }}
         >Altres</button>
         <button
           style={{
             background: "#ddd",
             flexGrow: `1`,
             padding: `2em`          
           }}
           onClick={e => {
             afegeixAct({
               nomAct: "Dormir",
               start: new Date(),
               overDue: false,
               color: "#000"
             })
           }}
         >Dormir</button>
       </div>
       <div style={{
         display:`flex`
       }}>
         <button
           style={{
             padding: `2em`,
             flexGrow: `1`
           }}
           onClick={e => {
             const instant = new Date();
   
             if (confirm("Acabar la sessió actual?") && confirm("Vas a tancar la sessió actual i posaràs tots els valors A ZERO!!")) {
               setAct({
                 nomAct: null,
                 start: instant
               });
   
               Meteor.call('laGerra.upsert', {...sessio, end: instant});
               Meteor.call('laGerra.upsert', {
                 init: instant,
                 acts: [],
                 user: Meteor.userId()
               });
             }
           }}
         >REINICIAR LA SESSIÓ</button>

       </div>
     <SessionsAnteriors />    
     </>;
   };
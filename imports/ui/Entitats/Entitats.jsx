import React, { useRef, useState, useContext, useEffect } from "react";
import { Meteor } from 'meteor/meteor';

import { Mongo } from 'meteor/mongo';
import { useHistory, useParams, Link } from "react-router-dom";
import { useTracker } from 'meteor/react-meteor-data';
import { EntitatsCollection } from '/imports/api/entitats';

import { DndProvider, useDrag, useDrop } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';

import { ItemTypes } from '/imports/ui/DnD2/ItemTypes.js';

import { TextareaAutosize } from '@mui/base';

import LlistaArxiusEnt from '/imports/ui/files/LlistaArxiusEnt.jsx';
import FileUploadEnt from "../files/FileUploadEnt";

import CheckboxTree from 'react-checkbox-tree';

// import { Modal } from 'react-ui';

import * as ReactDOM from 'react-dom';


// import FontAwesomeIcon from '@fortawesome/react-fontawesome';
// import { AreesPersonalsCollection } from '/imports/api/areesPersonals';
// Aquesta mini aplicació permet la creació de tasques a una col·lecció de la BD.
// Són vinculades a l'usuari que les crea
// Com a mínim tindrà aquests camps:
// { titol, uid, createdAt, dates_feta }
// El camp 'dates_feta' és un arrai dels moments en que la tasca ha sigut realitzada
//

// Les entitats han de poder relacionar-se entre elles. Relacions han de poder definir-se i seleccionar-se.

// > Entitat superior
// > Llista d'etiquetes
// > 

// Una entitat pot ser: 
// > una persona, 
// > una planta, 
// > un objecte qualsevol, 
// > una fotografia,  
// > un article descarregat d'internet,  
// > un llibre,
// > un concepte,
// > un dia,
// > una cançó,
// > un disc,
// > un grup,
// > un membre d'un grup,
// > un instrument,
// > un camp conceptual
// > ...

// Pràcticament, una entitat pot ser qualsevol cosa. 
// El repte és codificar-les de manera que puguen representar-se adequadament.
// Hauran de tindre un nombre de camps variables i hauran de poder relacionar-se entre si (d'un a un, un a molts o molts a un)
// 

const Modal = props => {
     return ReactDOM.createPortal(
          <div style={{
               position: `absolute`,
               width: `100%`,
               height: `100%`,
               zIndex: `1000`,
               background: `#aaaa`
          }}>
               <div>
                    {props.children}
               </div>
          </div>,
     document.body);
};


const Ent = ({entitats}) => {
     const [optText, setOptText] = useState(false);
     const [creaSubEnt, setCreaSubEnt] = useState(false);
     const [optArxiu, setOptArxiu] = useState(false);
     const { id } = useParams();
     
     
     const e = entitats?.filter(e => e._id === id )[0];

     const eSubs = entitats?.filter(e => e.superiors.includes(id));

     const CreaSubEnt = () => {
          const inTitolSubEnt = useRef();

          return <>{
               creaSubEnt && <div style={{
                    border: `1px solid black`,
                    padding: `.5em`,
                    margin: `.5em`
               }}>
                    <input type="text" ref={inTitolSubEnt} 
                         onKeyPress={ev => {
                              if (ev.key == "Enter") {
                                   Meteor.call('entitats.insert', {superiors: [id], titol: inTitolSubEnt.current.value});
                              }
                         }}
                         autoFocus
                    />
                    <button onClick={ev => {
                         Meteor.call('entitats.insert', {superiors: [id], titol: inTitolSubEnt.current.value})
                    }}>Crea</button>
               </div>
          }</>;
     };

     const CreaText = () => {
          const taText = useRef();
          const [isDark, setIsDark] = useState(true);
          const [isZen, setIsZen] = useState(false);

          document.addEventListener('fullscreenchange', () => {
               setIsZen(false);
          }, false);
          
          return <>{
               optText && <div id="divText">
                    {/* <textarea ref={taText}
                         autoFocus
                         style={{
                              width: `min(100%, 1600px)`,
                              margin: `0 auto`,
                              display: `block`,
                              background: `${isDark ? "#000" : "#fff"}`,
                              color: `${isDark ? "#fff" : "#000"}`,
                              height: `${isZen ? "710px" : "auto"}`,
                              border: `none`
                         }}
                    ></textarea> */}
                    <TextareaAutosize
                         ref={taText}
                         autoFocus
                         aria-label="empty textarea"
                         // placeholder="Empty"
                         style={{ 
                              width: `min(100%, 1600px)`,
                              margin: `0 auto`,
                              display: `block`,
                              background: isDark ? "#000" : "#fff",
                              color: isDark ? "#fff" : "#000",
                              //height: `${isZen ? "710px" : ""}`,
                              border: `none`,
                         // fontSize: `${isZen ? "1vw" : "auto"}`
                              cursor: isZen ? "none" : "auto"
                         }}
                    />
                    <button 
                         style={{
                              background: `${isZen ? "lime" : ""}`
                         }}
                         onClick={ev => {
                              ev.preventDefault();
                              const dT = taText.current; // document.querySelector("#divText");
                              
                           //   document.fullScreenElement = isZen;
                              
                              if (!isZen) {
                                   dT.scrollIntoView();

                                   // window.scrollBy(0,-20);
                                   dT.requestFullscreen({ navigationUI: "show" })
                                        .then(function() {
                                             dT.focus();
                                        })
                                        .catch(function(error) {
                                        
                                        });
                              } else {
                                   window.scrollTo(0, 0);
                              }

                              setIsZen(!isZen);
                              
                         }}
                    ><img style={{
                         maxHeight: `1em`
                    }} src="/zen.svg"/></button>
                    <button 
                         style={{
                              background: `${isDark ? "lime" : ""}`
                         }}
                         onClick={ev => {
                              ev.preventDefault();
                              setIsDark(!isDark);
                         }}
                    ><span className="fas fa-adjust" /></button>
                    <button onClick={ev => {
                         ev.preventDefault();
                         Meteor.call('entitats.afegirText', id, taText.current.value);
                    }}>Guardar</button>
               </div>
          }</>;
     };

     const CreaArxiu = () => {
          
          return <div style={{
               border: `solid 1px pink`
          }}>
               {
                    optArxiu 
                         ? <div>
                              <FileUploadEnt entId={id} />
                         </div>
                         : null
               }
          </div>;
     };
     
     const OpcionsPredeterminades = () => {
        
          return <>
               <button 
                    onClick={ev => {
                         setOptText(!optText);
                    }}
                    style={{
                         background: optText ? `lime` : `auto`
                    }}
               >Text</button> {/* Afegim un text a l'Entitat. Simple i directe. Els Texts es guardaran a un arrai al propi objecte de l'Entitat. */}
               <button 
                    onClick={ev => {
                         ev.preventDefault();
                         setOptArxiu(!optArxiu);
                    }}
                    style={{
                         background: optArxiu ? `lime` : `auto`
                    }}
               >Arxiu</button>
               <button onClick={ev => {

               }}>Data</button> {/* Afegir una data a l'Entitat. Sospite que les dates podran tindre natures i significats diferents. */}

               <button 
                    onClick={ev => {
                         setCreaSubEnt(!creaSubEnt);
                    }}
                    style={{
                         background: creaSubEnt ? `lime` : `auto`
                    }}
               >SubCat</button>

          </>;             
     };

     const EntBreadcrumbs = ({entId}) => {
          const e = entitats?.filter(e => e._id === entId )[0];

          if (e?.hasOwnProperty("superiors") && e?.superiors.length > 0) {
               // 
               const eSupId = e?.superiors[0];
               const eSup = entitats?.filter(eSup => eSup._id === eSupId )[0];

               return <>
                    <EntBreadcrumbs entId={eSupId} />
                    <Link to={`/ent/${eSupId}`}>
                         <div style={{
                              display: `inline-block`,
                              padding: `.1em .2em`,
                              border: `2px solid black`,
                              borderRadius: `.3em`,
                              background: `#fffa`,
                              margin: `.1em`
                         }}>{eSup?.titol}
                         </div>
                    </Link>
               </>;
          } else {
               // Mostra l'arrel
               return <>
                    <Link to="/entitats">
                         <div style={{
                              display: `inline-block`,
                              padding: `.1em .2em`,
                              border: `2px solid black`,
                              borderRadius: `.3em`,
                              background: `#fffa`,
                              margin: `.1em`
                         }}><span className="fas fa-carrot"></span>
                         </div>
                    </Link>
               </>;
          }
     };

     return <>
          <div style={{
                    // border: `1px solid black`,
                    padding: `.5em`
               }}
          >
               <h1 style={{
                    border: `1px solid black`,
                    padding: `.5em`
               }}>
                    
                    <EntBreadcrumbs entId={id} />
                    <span style={{
                         margin: `.1em`
                    }}>{e?.titol}</span>
               </h1>
               
               {/* <Link to={`/ent/${e?._id}`}> */}
                    <Entitats superior={id} />
               {/* </Link> */}
               
               <div>
                    <LlistaArxiusEnt entId={id} />
               </div>

               <div style={{
                    textAlign: `right`,
                    margin: `.3em`
               }}>
                    <OpcionsPredeterminades />
                    <>{
                         (optText || creaSubEnt || optArxiu) && <div style={{
                              border: `1px solid black`,
                              padding: `.5em`,
                              margin: `.5em`
                         }}>
                              <CreaSubEnt />
                              <CreaArxiu />
                              <CreaText />
                         </div>
                    }</>
               </div>
               <>{ 
                    e?.texts?.length && <div
                         className="ent_texts"
                         style={{
                              border: `1px solid black`,
                              padding: `.5em`
                         }}
                    >
                         {e?.texts
                              // ?.filter(t => (
                              //      t.createdAt.getDate() === new Date().getDate() 
                              //      && t.createdAt.getDay() === new Date().getDay()
                              //      && t.createdAt.getFullYear() === new Date().getFullYear()
                              // ))
                              ?.reverse()
                              ?.map((t,i) => <div 
                                   key={`ent_${i}`}
                                   style={{
                                        border: `1px solid black`,
                                        padding: `.5em`,
                                        margin: `.2em`,
                                        whiteSpace: `pre-wrap`
                                   }}
                                   title={t?.createdAt?.toLocaleString()}
                              >{t.text}</div>)
                         }
                    </div> || null
               }</>
          </div>
     </>;
};

const Entitats = ({superior}) => {
     const [entitatId, setEntitatId] = useState(null);
     const inTitolEntitat = useRef();
     const { 
          entitats
          //, areesPersonals 
     } = useTracker(() => {
          Meteor.subscribe('entitatsUsuari', Meteor.userId());
          const entitats = EntitatsCollection.find({}).fetch();
          // Meteor.subscribe('areesPersonalsUsuari', Meteor.userId());
          // const areesPersonals = AreesPersonalsCollection.find({}).fetch();
          return {
               entitats
               // ,
               // areesPersonals
          };
     });
     const [triades, setTriades] = useState([]);
    // const refSelAreaPersonal = useRef();
     const refGuardaEntitat = useRef();

     const Entitat = ({ entitat, i, a }) => {
          let history = useHistory();
          const [hovered, setHovered] = useState(false);
          const [edita, setEdita] = useState(false);
          const [esborra, setEsborra] = useState(false);
          const spnTitol = useRef();
          // const [dragging, setDragging] = useState(false);
          const [estil, setEstil] = useState({});
          //const refLi = useRef();
          const [draggedOver, setDraggedOver] = useState(false);
          // const [draggingId, setDraggingId] = useState(null);

          const [modalTrashOpen, setModalTrashOpen] = useState(false);

          const [{ isDragging }, drag] = useDrag(() => {
             //  setDraggingId(entitat._id);;

               return {
                    type: ItemTypes.ENTITAT,
                    collect: (monitor) => {
                         // console.log(monitor);
                         // console.log("SID: ", monitor.getSourceId());
                         // console.log("ITEM: ", monitor.getItem()?.id);
                         // setDraggingId();
                         return {
                              isDragging: !!monitor.isDragging()
                         }
                    },
                    item: {
                         id: entitat._id,
                         titol: entitat.titol
                    }
               };
          });

          const [, drop] = useDrop(() => ({
               accept: ItemTypes.ENTITAT,
               drop: (item, monitor) => {
                    if (confirm(`Moure l'entitat ${item.titol} a ${entitat.titol}?`)) {
                         Meteor.call('entitat.novaSub', entitat, item.id);
                         Meteor.call('entitat.novaSuperior', {_id: item.id}, entitat._id);
                    //      alert(`
                    //      Entitat ori: ${item.id}
                    //      Entitat dest: ${entitat._id}`);//
                    }
                    //return null;
               }
          }));

          const [obrePrivacitat, setObrePrivacitat] = useState(false);

          function esborraAmbDescendents(e) {
               if (e?.descendents?.length == 0) {
                    Meteor.call('entitats.remove', e._id);
               } else {
                    e?.descendents?.forEach(d => {
                         const novaEnt = {...e, descendents: e.descendents.filter( ef => ef =! d)};

                         console.log(`novaEnt: ${novaEnt}`);

                         Meteor.call('entitats.update', novaEnt, () => {esborraAmbDescendents(d);});
                    })
               }
          }
          
          return <div 
               ref={drop}
               entid={entitat._id}
               style={draggedOver 
                    ? {
                         padding: `2em`,
                         background: `magenta`
                    }
                    : {} 
               }
               onDragOver={ev => {
                    setDraggedOver(true);
               }}
               onDragLeave={ev => {
                    setDraggedOver(false);
               }}
          >
               <li
                    // className="liEntitat" 
                    ref={drag}
                    key={`entitat_${i}`}
                    style={{
                         textDecoration: triades.includes(entitat.titol) ? `line-through` : `none`,
                        // display: triades.includes(entitat.titol) ? `none` : `block`,
                        // fontWeight: triades.includes(entitat.titol) ? `bold` : `none`
                    //     display: `flex`,
                        background: `${hovered ? "red" : "black"}`,
                        border: `1px solid black`,
                        padding: `.5em`,
                        borderRadius: `.4em`,
                        margin: `1px`,
                        textAlign: `center`,
                        listStyle: `none`,
                    //     alignContent: `center`,
                    //     alignItems: `center`
                         alignSelf: `center`,
                         color: `white`,
                         cursor: `pointer`
                    }}
                    data-entitat-index={i}
                    title={entitat.text}
                    data-entitat_id={entitat._id}
                    onMouseEnter={ev => {
                         setHovered(true);
                    }}
                    onMouseLeave={ev => {
                         setHovered(false);
                    }}

                    onClick={ev => {
                         ev.preventDefault();
                         ev.stopPropagation();

                         if (!edita) {
                              history.push(`/ent/${entitat._id}`);
                         }
                    }}
               >
                    {/* <Link to={`/ent/${entitat._id}`}> */}
                         {/* {entitat.hasOwnProperty("area_personal") 
                              ? `(${entitat.area_personal}) ${entitat.titol}`  */}
                              {/* :  */}
                         
                         <span 
                              ref={spnTitol} 
                              contentEditable={edita} 
                              focus={edita || undefined}
                              onBlur={ev => {
                                   setEdita(false);

                                //   alert(spnTitol.current.textContent);
                                   Meteor.call('entitats.update', {...entitat, titol: spnTitol.current.textContent});
                              }}
                         >{ `${entitat.titol}` }</span>

                         {
                              entitat.privacy?.includes(Meteor.userId()) && <span className="fas fa-lock" style={{marginLeft: `.4em`}} />
                         }
                         {
                              hovered && <><span style={{
                                   position: `absolute`,
                                   cursor: `pointer`, // No està funcionant
                                   transform: `translate(-3em, -1.2em)`
                              }}>
                                   <button
                                        onClick={ev => {
                                             ev.preventDefault();
                                             ev.stopPropagation();
                                             
                                            // setObrePrivacitat(!obrePrivacitat);
                                             if (confirm(`
                                                  Tria SÍ per fer aquesta Entitat PRIVADA.
                                                  Tria NO per fer-la PÚBLICA`)) {
                                                  Meteor.call('ent.fesPrivada', entitat._id);
                                             } else {
                                                  Meteor.call('ent.fesPublica', entitat._id);
                                             }
                                        }}
                                        
                                   ><span className="fas fa-globe-europe" /></button> {/* users / user-friends / user / globe-europe / globe-africa / globe-asia / globe-americas */}
                                   <button
                                        onClick={ev => {
                                             ev.preventDefault();
                                             ev.stopPropagation();
                                             
                                             setEdita(true, () => {
                                                  spnTitol.current.click();
                                             });

                                             
                                             
                                             // inTitolEntitat.current.value = entitat.titol;
                                             // refGuardaEntitat.current.dataset["entitat_id"] = entitat._id;
                                        }}
                                        
                                   ><span className="fas fa-pencil-alt" /></button>
                                   <button
                                        onClick={ev => {
                                             ev.preventDefault();
                                             ev.stopPropagation();

                                             if (confirm(`Vas a esborrar l'entitat ${entitat.titol}. Procedir?`)) {
                                                  if (entitat?.descendents?.length > 0) {
                                                       if (confirm(`L'entitat conté subentitats. Si continues s'esborraran totes de manera definitiva.`)) {

                                                            esborraAmbDescendents(entitat);
                                                       }
                                                  } else {
                                                       Meteor.call('entitats.remove', entitat._id);
                                                  }
                                             }
                                          //  setModalTrashOpen(true);
                                        }}
                                   ><span className="fas fa-trash-alt" /></button>
                              </span>

                              {/* <div>{modalTrashOpen && (
                                   <Modal>
                                        
                                   </Modal>
                                   )} </div> */}
                            </>
                         }
                    {/* </Link> */}
               </li>
          </div>;
     };

     const refTaText = useRef();
     const refSelSuperior = useRef();
     const refSelType = useRef();

     const [volCrearEnt, setVolCrearEnt] = useState(false);

     const CreaEnt = () => {
          const [creaDescripcio, setCreaDescripcio] = useState(false);
          const [creaSuperior, setCreaSuperior] = useState(false);
          const [creaCompartir, setCreaCompartir] = useState(false);

          const [checked, setChecked] = useState([]);
          const [expanded, setExpanded] = useState([]);

          // const nodes = [{
          //      value: 'mars',
          //      label: 'Mars',
          //      children: [
          //          { value: 'phobos', label: 'Phobos' },
          //          { value: 'deimos', label: 'Deimos' },
          //      ],
          //  }];

          // function entitatsSuperiors(entId) {
          //      return entitats.filter(e => e.superiors.includes(entId));
          // }

          function eId2e(eId) {
               return entitats.find(e => e._id === eId);
          }

          function formaTreeNodes(e) {

               if (!e) {
                    return entitats
                         .filter(e => !e.superiors.length)
                         .map(e => formaTreeNodes(e))
                    ;
               } else {
                    if (!e.descendents.length) {
                         return {
                              value: e._id,
                              label: e.titol
                         };
                    } else {
                         return {
                              value: e._id,
                              label: e.titol,
                              children: e.descendents
                                   .map(eId => {
                                        //console.log("eId: ", eId);
                                        return formaTreeNodes(eId2e(eId));
                                   })
                         };
                    }
               }
          }

          const nodes = formaTreeNodes();

          console.log(nodes);

          return  <fieldset>
               <legend>Crea una entitat nova: </legend>
               <input type="text" ref={inTitolEntitat} autoFocus placeholder="Títol" />
               <br />
               <button onClick={ev => {
                    ev.preventDefault();

                    setCreaDescripcio(!creaDescripcio);
               }}>Descripció</button>
               <button onClick={ev => {
                    ev.preventDefault();

                    setCreaSuperior(!creaSuperior);
               }}>Superior</button>
               <button onClick={ev => {
                    ev.preventDefault();

                    setCreaCompartir(!creaCompartir);
               }}><span className="fas fa-share-alt" /></button>

               <br />
               {    creaDescripcio && <>
                         <textarea ref={refTaText} placeholder="Text" />
                         <br />
                    </>
               }
               {    creaSuperior && <div style={{
                         background: `#dddd`,
                         padding: `.7em`,
                         margin: `.3em`,
                         borderRadius: `.3em`,
                         border: `#33c 1px solid`
                    }}>
                         <CheckboxTree
                              icons={{
                                   check: <span className="rct-icon rct-icon-check" />,
                                   uncheck: <span className="rct-icon rct-icon-uncheck" />,
                                   halfCheck: <span className="rct-icon rct-icon-half-check" />,
                                   expandClose: <span className="rct-icon rct-icon-expand-close" />,
                                   expandOpen: <span className="rct-icon rct-icon-expand-open" />,
                                   expandAll: <span className="rct-icon rct-icon-expand-all" />,
                                   collapseAll: <span className="rct-icon rct-icon-collapse-all" />,
                                   parentClose: <span className="rct-icon rct-icon-parent-close" />,
                                   parentOpen: <span className="rct-icon rct-icon-parent-open" />,
                                   leaf: <span className="rct-icon rct-icon-leaf fas fa-dice-d6" />
                              }}
                              nodes={nodes}
                              checked={checked}
                              expanded={expanded}
                              onCheck={checked => {
                                   if (checked.length <= 1) {
                                        setChecked( checked );
                                   } else {
                                        alert("De moment no pots assignar més d'una entitat superior");
                                   }
                              }}
                              onExpand={expanded => setExpanded( expanded )}
                              //checkModel="leaf"
                              noCascade
                         />    
                    </div>
               }
               
               <button
                    ref={refGuardaEntitat}
                    onClick={ev => {
                         if (entitats.some(e => e.titol === inTitolEntitat.current.value)) {
                              Meteor.call('entitats.update', { _id: ev.target.dataset["tqt_id"], titol: inTitolEntitat.current.value, uid: Meteor.userId(), area_personal: refSelAreaPersonal.current.value, dates_feta: [], dates_triada: [] });
                         } else {

                              //let superiors = checked; // refSelSuperior?.current?.value && refSelSuperior?.current?.value !== "--CAP--" ? [ refSelSuperior?.current?.value ] : [];

                              // console.log("superiors: ", superiors);

                              Meteor.call('entitats.insert', { 
                                   titol: inTitolEntitat?.current?.value,
                                   texts: refTaText?.current?.value && [{
                                        text: refTaText?.current?.value, //document.querySelector('#taText').value,
                                        createdAt: new Date(),
                                        createdBy: Meteor.userId()
                                   }] || [],
                                   uid: Meteor.userId(), 
                                   dates_feta: [], 
                                   dates_triada: [],
                                   superiors: checked,
                                   descendents: [],
                                 //  type: refSelType.current.value !== "--CAP--" ? [ refSelType.current.value ] : []
                              },
                              (err, novaEntId) => {
                                   if (checked.length) {
                                        checked.forEach(supId => {
     
                                             let eSup = eId2e(supId);

                                             // console.log("eSup: ", eSup);
                                             // console.log("novaEntId: ", novaEntId);

                                             let eSupMod = {...eSup, descendents: [...eSup.descendents, novaEntId]};
     
                                             Meteor.call('entitats.update', eSupMod);
                                        });
                                   }
                              });
                         }
                         inTitolEntitat.current.value = "";
                         inTitolEntitat.current.focus();
                         //document.querySelector('#taText').value = "";
                    }}
               >Guarda</button>
          </fieldset>
     };

     return <DndProvider backend={HTML5Backend}>
          <div>
          {/* <fieldset> */}
               <ol
                    style={{
                         padding: `0`,
                         display: `flex`,
                         // flexDirection: `column`
                         alignItems: `stretch`,
                         color: `white`,
                         flexWrap: `wrap`,
                         justifyContent: `space-evenly`
                    }}
               >{
                    entitats
                         .filter(e => {
                              if (superior && superior !== []) {
                                   return e.superiors.includes(superior);
                              } else {
                                   return e.superiors?.length == 0;
                              }
                         })
                         .sort((a, b) => a.titol.toLowerCase() > b.titol.toLowerCase())
                         .map((ent,i,a) => {
                              return <Entitat key={`ent_${i}`} entitat={ent} />
                         })
               }</ol>
               {
                    !superior && <>
                         <button onClick={ev => {
                              ev.preventDefault();

                              setVolCrearEnt(!volCrearEnt);
                         }}>+</button>
                         { volCrearEnt && <CreaEnt /> }
                    </>
               }
          {/* </fieldset> */}
          </div>
     </DndProvider>;
};

export { Entitats, Ent };